.cpp------------------------------------------------------------------------------------
// Fill out your copyright notice in the Description page of Project Settings.

#include "VolumeController.h"
#include "ConstructorHelpers.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonReader.h"
#include "Serialization/JsonWriter.h"
#include "Serialization/JsonSerializer.h"
#include "Templates/SharedPointer.h"
#include "Misc/FileHelper.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AVolumeController::AVolumeController()
{
 	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	// Give both boxes a mesh
	if (MeshAsset.Object != nullptr)
	{
		ActorMesh = MeshAsset.Object;
	}

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BoxOne");
	if (ActorMesh != nullptr)
	{
		MeshComponent->SetStaticMesh(ActorMesh);
		MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	}
	MeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0,0,0)));

	LoadFromJSON();
}

// Called when the game starts or when spawned
void AVolumeController::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVolumeController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVolumeController::SaveToJSON()
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	//FVector MeshTransform = *MeshComponent->GetRelativeTransform->RelativeScale3D;
	JsonObject->SetStringField("TestArray", "Test");
	//JsonObject->SetArrayField(TEXT("TestArray"), );

	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

	//TSharedRef<TJsonWriter<TCHAR, TPrettyJsonPrintPolicy< TCHAR >>> JsonWriter = TJsonWriterFactory<TCHAR, TPrettyJsonPrintPolicy< TCHAR >>::Create(&OutputString);
	//FJsonSerializer::Serialize(JsonObject.ToSharedRef(), JsonWriter);

	// Games/Content/****.json
	FString FullPath = FPaths::GameContentDir();
	FString Path = TEXT("TestJson.json");
	FullPath += Path;

	FFileHelper::SaveStringToFile(OutputString, *FullPath);
}

void AVolumeController::LoadFromJSON()
{
	FString Path = TEXT("TestJson.json");
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(JsonFullPath(Path));
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	if (FJsonSerializer::Deserialize(Reader, JsonObject) && JsonObject.IsValid())
	{
		TArray<FString> ArrayJson;
		JsonObject->TryGetStringArrayField(TEXT("TestArray"), ArrayJson);
		FString JsonString = JsonObject->GetStringField(TEXT("TestArray"));
		NameToJson = JsonString;
		
		/*
		FVector Vector = FVector{
			FCString::Atof(*ArrayJson[0]),
			FCString::Atof(*ArrayJson[1]),
			FCString::Atof(*ArrayJson[2])};
		MeshComponent->SetRelativeScale3D(Vector);*/
		//NameToJson = *ArrayJson[0];
	}
}

FString AVolumeController::JsonFullPath(FString Path)
{
	// Games/Content/****.json
	FString FullPath = FPaths::GameContentDir();
	FullPath += Path;
	FString JsonStr;
	FFileHelper::LoadFileToString(JsonStr, *FullPath);

	return JsonStr;
}
.h--------------------------------------------------------------------------------------
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/CoreUObject/Public/UObject/SoftObjectPtr.h"
#include "VolumeController.generated.h"

UCLASS()
class SANDBOX_API AVolumeController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVolumeController();

private:
	UPROPERTY()
	UStaticMesh* ActorMesh;

	UPROPERTY()
	UStaticMeshComponent* MeshComponent;

	UPROPERTY()
	FVector DimensionStorage;

	UPROPERTY(EditAnywhere)
	FString NameToJson;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SaveToJSON();
	UFUNCTION()
	void LoadFromJSON();
	UFUNCTION()
	FString JsonFullPath(FString Path);

};
----------------------
build.cs = "Json","JsonUtilities" 