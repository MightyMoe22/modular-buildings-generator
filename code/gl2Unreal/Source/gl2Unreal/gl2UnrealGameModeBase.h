// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "gl2UnrealGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GL2UNREAL_API Agl2UnrealGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
