// Copyright Maurice Langer und Jakob Seitz 2020

#include "TestModuleSetup.h"
#include "Runtime/CoreUObject/Public/UObject/SoftObjectPtr.h"
#include "Misc/FileHelper.h"
#include "ConstructorHelpers.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Misc/FileHelper.h"
#include "Containers/Array.h"
#include "JSONManager/JSONManager.h"
#include "Modules/ModularBuildingVolumeData.h"
#include "Engine/StaticMesh.h"

// Sets default values
ATestModuleSetup::ATestModuleSetup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	ModuleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("ModuleComponent");
	ModuleMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SetMeshToModuleComponent(nullptr, FVector(0, 0, 0));

	LoadModuleContainer();
	SetupReferenceVolume();
}

void ATestModuleSetup::SetupReferenceVolume()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		ReferenceVolumeMesh = MeshAsset.Object;
	}
	ReferenceVolumeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	ReferenceVolumeMeshComponent->SetStaticMesh(ReferenceVolumeMesh);
	// Create hierarchy by setting the component as chide from RootComponent + keep childs position relative to parent
	ReferenceVolumeMeshComponent->SetupAttachment(RootComponent);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling;
	TSharedPtr<FJsonObject> JsonObject = NULL;
	if(JSONManager::ReadJsonFile(Path, JsonObject))
	{
		BoxScaling = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject).VolumeScale;
	} else
	{
		BoxScaling = FVector(1, 1, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f"), BoxScaling.X, BoxScaling.Y, BoxScaling.Z);
	ReferenceVolumeMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}

void ATestModuleSetup::LoadModuleContainer()
{
	ModuleContainer = UModuleContainer::GetInstance();
}

void ATestModuleSetup::LoadModule()
{
	UModuleDataObject* ModuleToLoad;

	if(NameOfModuleToLoad != "")
	{
		if(ModuleContainer->AllModuleDataObjects.Contains(NameOfModuleToLoad))
		{
			ModuleToLoad = *ModuleContainer->AllModuleDataObjects.Find(NameOfModuleToLoad);

			auto MeshAsset = LoadObject<UStaticMesh>(NULL, *ModuleToLoad->ModuleData.StaticMeshPath, NULL, LOAD_None, NULL);

			if(MeshAsset != nullptr)
			{
				ModuleMesh = MeshAsset;
				SetMeshToModuleComponent(ModuleMesh, ModuleToLoad->ModuleData.Position);
			}
		}
	}
}

void ATestModuleSetup::SetMeshToModuleComponent(UStaticMesh* Mesh, FVector Position)
{
	ModuleMeshComponent->SetStaticMesh(Mesh);
	ModuleMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), Position));
}

FString ATestModuleSetup::JsonFullPath(FString Path)
{
	// Games/Content/****.json
	//FString FullPath = FPaths::GameContentDir();
	FString FullPath = FPaths::ProjectPluginsDir();
	FullPath += Path;
	FString JsonStr;
	FFileHelper::LoadFileToString(JsonStr, *FullPath);

	return JsonStr;
}
// Called every frame
void ATestModuleSetup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
