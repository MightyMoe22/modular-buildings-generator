// Copyright Maurice Langer und Jakob Seitz 2020

#include "SetupBuildingBockVolume.h"
#include "ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Misc/FileHelper.h"
#include "Containers/Array.h"

// Sets default values
ASetupBuildingBockVolume::ASetupBuildingBockVolume()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	DummyModuleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("ModuleComponent");
	DummyModuleMeshComponent->SetupAttachment(RootComponent);

	SetupReferenceVolume();
}

void ASetupBuildingBockVolume::SetupReferenceVolume()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		ReferenceVolumeMesh = MeshAsset.Object;
	}
	ReferenceVolumeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	ReferenceVolumeMeshComponent->SetStaticMesh(ReferenceVolumeMesh);
	// Create hierarchy by setting the component as child from RootComponent + keep childs position relative to parent
	ReferenceVolumeMeshComponent->SetupAttachment(RootComponent);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(JsonFullPath(Path));
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	FVector BoxScaling;

	if(FJsonSerializer::Deserialize(Reader, JsonObject) && JsonObject.IsValid())
	{
		float Scale_X = JsonObject->GetNumberField(TEXT("Scale_X"));
		float Scale_Y = JsonObject->GetNumberField(TEXT("Scale_Y"));
		float Scale_Z = JsonObject->GetNumberField(TEXT("Scale_Z"));

		BoxScaling = FVector(Scale_X, Scale_Y, Scale_Z);
	} else
	{
		BoxScaling = FVector(1, 1, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f"), BoxScaling.X, BoxScaling.Y, BoxScaling.Z);
	ReferenceVolumeMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}

void ASetupBuildingBockVolume::LoadModuleMesh()
{
	if(DummyModuleMesh != nullptr)
	{
		SetMeshToModuleComponent(DummyModuleMesh);
	}
}

void ASetupBuildingBockVolume::SetMeshToModuleComponent(UStaticMesh* Mesh)
{
	DummyModuleMeshComponent->SetStaticMesh(Mesh);
	DummyModuleMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0)));
}

void ASetupBuildingBockVolume::SaveBuildingBlockDimension()
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	// Fill JsonObject with ModuleData
	JsonObject->SetNumberField("Scale_X", ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().X);
	JsonObject->SetNumberField("Scale_Y", ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().Y);
	JsonObject->SetNumberField("Scale_Z", ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().Z);

	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

	// Games/Content/****.json
	//FString FullPath = FPaths::GameContentDir();
	FString FullPath = FPaths::ProjectPluginsDir();
	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FullPath += Path;

	FFileHelper::SaveStringToFile(OutputString, *FullPath);
}

FString ASetupBuildingBockVolume::JsonFullPath(FString Path)
{
	// Games/Content/****.json
	//FString FullPath = FPaths::GameContentDir();
	FString FullPath = FPaths::ProjectPluginsDir();
	FullPath += Path;
	FString JsonStr;
	FFileHelper::LoadFileToString(JsonStr, *FullPath);

	return JsonStr;
}

// Called when the game starts or when spawned
void ASetupBuildingBockVolume::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASetupBuildingBockVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASetupBuildingBockVolume::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
	// in the future this will fail to compile and we can update it.  
	if(PropertyName == GET_MEMBER_NAME_CHECKED(ASetupBuildingBockVolume, DummyModuleMesh))
	{
		LoadModuleMesh();
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
