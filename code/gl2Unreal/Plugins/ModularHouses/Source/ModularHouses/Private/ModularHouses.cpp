// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ModularHouses.h"
#include "Generator/GroundGenerator.h"
#include "ModularBuildingSettingStyle.h"
#include "ModularBuildingSettingCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Text/STextBlock.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"


#include "Widgets/Layout/SBox.h"
#include "Widgets/Layout/SGridPanel.h"
#include "Widgets/Layout/SScrollBox.h"
#include "Widgets/Layout/SHeader.h"

#include "Widgets/Input/SSpinBox.h"
#include "Widgets/Input/SCheckBox.h"
#include "Widgets/Input/SSlider.h"
#include "Widgets/Input/SButton.h"

static const FName ModularBuildingSettingTabName("Building");

//TSharedPtr<SMenuAnchor> StacklessAnchor1;

#define LOCTEXT_NAMESPACE "FModularHousesModule"

FModularHousesModule::FModularHousesModule()
{
	//Bind setTileSize auf FOnFloatValueChanged floatDel
	floatValueChanged.BindRaw(this, &FModularHousesModule::setTileSize);
	checkStateChanged.BindRaw(this, &FModularHousesModule::setStepByStep);
	buttonClicked.BindRaw(this, &FModularHousesModule::finishFloorClicked);
	button1Clicked.BindRaw(this, &FModularHousesModule::openModuleCreator);
	button2Clicked.BindRaw(this, &FModularHousesModule::openHouseGenerator);
	GenerateRandomGroundButtonClicked.BindRaw(this, &FModularHousesModule::generateRandomCrossGround);
}

void FModularHousesModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	FModularBuildingSettingStyle::Initialize();
	FModularBuildingSettingStyle::ReloadTextures();

	FModularBuildingSettingCommands::Register();

	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FModularBuildingSettingCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FModularHousesModule::PluginButtonClicked),
		FCanExecuteAction());

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender());
		MenuExtender->AddMenuExtension("WindowLayout", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateRaw(this, &FModularHousesModule::AddMenuExtension));

		LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		TSharedPtr<FExtender> ToolbarExtender = MakeShareable(new FExtender);
		ToolbarExtender->AddToolBarExtension("Settings", EExtensionHook::After, PluginCommands, FToolBarExtensionDelegate::CreateRaw(this, &FModularHousesModule::AddToolbarExtension));

		LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender);
	}


	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(ModularBuildingSettingTabName, FOnSpawnTab::CreateRaw(this, &FModularHousesModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("FModularBuildingsSettingsStandaloneWindowTabTitle", "Modular Buildings"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);



}

void FModularHousesModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FModularBuildingSettingStyle::Shutdown();

	FModularBuildingSettingCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(ModularBuildingSettingTabName);
}

//Event-Funktionen
void FModularHousesModule::setTileSize(float NewTileSize)
{
	TileSize = NewTileSize;
	UE_LOG(LogTemp, Warning, TEXT("TileSize = %f"), TileSize);
}

void FModularHousesModule::setStepByStep(ECheckBoxState checkBoxState)
{
	StepByStep = (bool) checkBoxState;
	UE_LOG(LogTemp, Warning, TEXT("(bool) StepByStep = %i"), StepByStep);
}

FReply FModularHousesModule::openModuleCreator()
{
	moduleCreatorBlueprint = LoadObject<UWidgetBlueprint>(NULL, TEXT("/ModularHouses/Tools/ModuleCreator.ModuleCreator"), NULL, LOAD_None, NULL);
	//moduleCreatorBlueprint = UWidgetBlueprint::
	UE_LOG(LogTemp, Warning, TEXT("ModuleCreator"));
	UEditorWidgetFunctionLibary::StartWidget(moduleCreatorBlueprint);
	return FReply::Handled();
}

FReply FModularHousesModule::openHouseGenerator()
{
	houseGeneratorBlueprint = LoadObject<UWidgetBlueprint>(NULL, TEXT("/ModularHouses/Tools/HouseVolumeMenu.HouseVolumeMenu"), NULL, LOAD_None, NULL);
	UE_LOG(LogTemp, Warning, TEXT("HouseGenerator"));
	UEditorWidgetFunctionLibary::StartWidget(houseGeneratorBlueprint);
	return FReply::Handled();
}

FReply FModularHousesModule::finishFloorClicked()
{
	activeFloor++;
	UE_LOG(LogTemp, Warning, TEXT("activeFloor = %i"), activeFloor);
	return FReply::Handled();
}

FReply FModularHousesModule::generateRandomCrossGround()
{
	/*UGroundGenerator *generator = new UGroundGenerator({ 30,20 });
	generator->GenerateRandomCrossGround({ 5,5 });
	delete generator;*/
	return FReply::Handled();
}

#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )

TSharedRef<SDockTab> FModularHousesModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{

	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			SNew(SScrollBox)
			+ SScrollBox::Slot()[
				SNew(SBorder)
					.Padding(5)
					.Content()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
							.Padding(5.0f)
							[
								SNew(SButton)
								.OnClicked(button1Clicked)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("ModuleCreator")))
										.Justification(ETextJustify::Center)
									]
							]
						+ SVerticalBox::Slot()
							.Padding(5.0f)
							[
								SNew(SButton)
								.OnClicked(button2Clicked)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("HouseGenerator")))
										.Justification(ETextJustify::Center)
									]
							]
					]
			]
	];
	/*return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			SNew(SBox)
			//.HAlign(HAlign_Center)
			//.VAlign(VAlign_Center)
			[
				SNew(SBorder)
				.Padding(5)
				.Content()
				[
					SNew(SScrollBox)

						+ SScrollBox::Slot()
						.Padding(5.0f)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot()
							.AutoHeight()
							[
								SNew(SHeader)
								.Content()
								[
									SNew(STextBlock)
									.Text(LOCTEXT("HeaderContentLabel", "Modular Building Settings"))
								]
							]
							+ SVerticalBox::Slot()
							.AutoHeight()
							[
								SNew(SGridPanel)

								+ SGridPanel::Slot(0,0)
									.Padding(5.0f, 5.0f)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("StepByStep")))
									]

								+ SGridPanel::Slot(1, 0)
									.Padding(5.0f, 5.0f)
									[
										SNew(SCheckBox)
										//.OnCheckStateChanged_Lambda([](ECheckBoxState value)->void{ UE_LOG(LogTemp, Warning, TEXT("hjsdhjfgklfkl")); })
										//.OnCheckStateChanged(this, &FModularHousesModule::checkChange)
										.OnCheckStateChanged(checkStateChanged)
										.IsChecked((ECheckBoxState) StepByStep)
								
									]

								+ SGridPanel::Slot(0, 1)
									.Padding(5.0f, 5.0f)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("grid-scale-factor")))
									]


								+ SGridPanel::Slot(1, 1)
									.Padding(5.0f, 5.0f)
									[
										SNew(SBox)
										.WidthOverride(50.0f)
									[
										SNew(SSpinBox<float>)
										.MinValue(0.0f)
									.MaxValue(20.0f)
									.MinSliderValue(TAttribute< TOptional<float> >(0.1f))
									.MaxSliderValue(TAttribute< TOptional<float> >(10.0f))
									.Value(TileSize)
									//.Delta(1.0f)
									//.Value(this, [this]()->void{returnValue = });
									.OnValueChanged(floatValueChanged)
									]

									]
								+ SGridPanel::Slot(0, 2)
									.Padding(5.0f, 5.0f)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("floor-panel")))
									]


								+ SGridPanel::Slot(1, 2)
									.Padding(5.0f, 5.0f)
									[
												SNew(SVerticalBox)
												+ SVerticalBox::Slot()
											.AutoHeight()
											[
												SNew(SButton)
												.Text(LOCTEXT("press if you want to edit the next floor", "finish floor"))
											//.OnClicked(this, &FModularHousesModule::FinishFloorButtonClicked)
											//.OnClicked_Lambda([]()->FReply { GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Button Clicked!")); return FReply::Handled(); })
											//.OnClicked(this, &FModularHousesModule::finishFloorClicked)
											.OnClicked(buttonClicked)
											]

											+ SVerticalBox::Slot()
												.AutoHeight()
												[
													SNew(STextBlock)
													.Text(FText::Format(LOCTEXT("Floor", "Floor  {0}/{1}"), activeFloor, floors))
												]

									]								
								+ SGridPanel::Slot(0, 3)
									.Padding(5.0f, 5.0f)
									[
										SNew(STextBlock)
										.Text(FText::FromString(TEXT("Grundriss")))
									]


								+ SGridPanel::Slot(1, 3)
									.Padding(5.0f, 5.0f)
									[
										SNew(SButton)
										.Text(LOCTEXT("Generate Random Ground", "Generate Random Ground"))
										.OnClicked(GenerateRandomGroundButtonClicked)
									]
							]

						]
				]
			]
		];*/

	
	
}


void FModularHousesModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->InvokeTab(ModularBuildingSettingTabName);
}

void FModularHousesModule::AddMenuExtension(FMenuBuilder& Builder)
{
	Builder.AddMenuEntry(FModularBuildingSettingCommands::Get().OpenPluginWindow);
}

void FModularHousesModule::AddToolbarExtension(FToolBarBuilder& Builder)
{
	Builder.AddToolBarButton(FModularBuildingSettingCommands::Get().OpenPluginWindow);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FModularHousesModule, ModularHouses)