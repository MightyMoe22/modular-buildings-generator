// Copyright Maurice Langer und Jakob Seitz 2020

#include "SetupModuleVolume.h"
#include "ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Misc/FileHelper.h"
#include "Containers/Array.h"

// Sets default values
ASetupModuleVolume::ASetupModuleVolume()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	ModuleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("ModuleComponent");
	ModuleMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SetMeshToModuleComponent(nullptr);

	SetupReferenceVolume();

	FString FullPath1 = FPaths::ProjectPluginsDir();
	UE_LOG(LogTemp, Warning, TEXT("%s"), *FullPath1);
}

// Called when the game starts or when spawned
void ASetupModuleVolume::BeginPlay()
{
	Super::BeginPlay();
}

void ASetupModuleVolume::SetupReferenceVolume()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if (MeshAsset.Object != nullptr)
	{
		ReferenceVolumeMesh = MeshAsset.Object;
	}
	ReferenceVolumeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	ReferenceVolumeMeshComponent->SetStaticMesh(ReferenceVolumeMesh);
	// Create hierarchy by setting the component as child from RootComponent + keep childs position relative to parent 
	ReferenceVolumeMeshComponent->SetupAttachment(RootComponent);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(JsonFullPath(Path));
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	FVector BoxScaling;

	if (FJsonSerializer::Deserialize(Reader, JsonObject) && JsonObject.IsValid())
	{
		float Scale_X = JsonObject->GetNumberField(TEXT("Scale_X"));
		float Scale_Y = JsonObject->GetNumberField(TEXT("Scale_Y"));
		float Scale_Z = JsonObject->GetNumberField(TEXT("Scale_Z"));

		BoxScaling = FVector(Scale_X, Scale_Y, Scale_Z);
	}
	else
	{
		BoxScaling = FVector(1, 1, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f"), BoxScaling.X, BoxScaling.Y, BoxScaling.Z);
	ReferenceVolumeMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}

void ASetupModuleVolume::LoadModuleMesh()
{
	if (ModuleMesh != nullptr)
	{
		SetMeshToModuleComponent(ModuleMesh);
	}
}

void ASetupModuleVolume::SetMeshToModuleComponent(UStaticMesh* Mesh)
{
	ModuleMeshComponent->SetStaticMesh(Mesh);
	ModuleMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0)));
}

void ASetupModuleVolume::RemoveModuleMesh()
{
	ModuleMesh = nullptr;
	SetMeshToModuleComponent(nullptr);
}

void ASetupModuleVolume::SaveModule()
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	FString ModuleStaticMeshPath = ModuleMesh->GetPathName();
	FTransform Filler;


	if (ModuleStaticMeshPath != "")
	{
		// Fill JsonObject with ModuleData
		JsonObject->SetStringField("ModuleStaticMeshPath", ModuleStaticMeshPath);
		JsonObject->SetNumberField("ModuleRelativePosition_X", ModuleMeshComponent->GetRelativeTransform().GetLocation().X);
		JsonObject->SetNumberField("ModuleRelativePosition_Y", ModuleMeshComponent->GetRelativeTransform().GetLocation().Y);
		JsonObject->SetNumberField("ModuleRelativePosition_Z", ModuleMeshComponent->GetRelativeTransform().GetLocation().Z);
		JsonObject->SetNumberField("Rot_X", ModuleMeshComponent->GetRelativeTransform().GetRotation().Euler().X);
		JsonObject->SetNumberField("Rot_Y", ModuleMeshComponent->GetRelativeTransform().GetRotation().Euler().Y);
		JsonObject->SetNumberField("Rot_Z", ModuleMeshComponent->GetRelativeTransform().GetRotation().Euler().Z);

		FString OutputString;
		TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
		FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

		// Games/Content/****.json
		//FString FullPath = FPaths::GameContentDir();
		FString FullPath = FPaths::ProjectPluginsDir();
		FString Path = TEXT("/ModularHouses/Content/JSON/ModuleData.json");
		FullPath += Path;

		FFileHelper::SaveStringToFile(OutputString, *FullPath);
	}

}

// Called every frame
void ASetupModuleVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASetupModuleVolume::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
	// in the future this will fail to compile and we can update it.  
	if (PropertyName == GET_MEMBER_NAME_CHECKED(ASetupModuleVolume, ModuleMesh))
	{
		LoadModuleMesh();
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}

FString ASetupModuleVolume::JsonFullPath(FString Path)
{
	// Games/Content/****.json
	//FString FullPath = FPaths::GameContentDir();
	FString FullPath = FPaths::ProjectPluginsDir();
	FullPath += Path;
	FString JsonStr;
	FFileHelper::LoadFileToString(JsonStr, *FullPath);

	return JsonStr;
}