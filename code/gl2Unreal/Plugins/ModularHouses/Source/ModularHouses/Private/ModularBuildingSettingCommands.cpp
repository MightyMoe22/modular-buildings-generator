// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ModularBuildingSettingCommands.h"

#define LOCTEXT_NAMESPACE "FModularHousesModule"

void FModularBuildingSettingCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "Modular Buildings Gen", "Opens Modular Building Menu", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE