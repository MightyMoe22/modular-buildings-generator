// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "./UI/EditorWidgetFunctionLibary.h"

class FToolBarBuilder;
class FMenuBuilder;

DECLARE_DELEGATE_OneParam(FFloatDelegate,float);
class FModularHousesModule : public IModuleInterface
{
public:

	UPROPERTY(BlueprintCallable CallInEditor, Category = "Buttons")
	UWidgetBlueprint* moduleCreatorBlueprint = NewObject<UWidgetBlueprint>();

	UPROPERTY(BlueprintCallable CallInEditor, Category = "Buttons")
	UWidgetBlueprint* houseGeneratorBlueprint = NewObject<UWidgetBlueprint>();


	FModularHousesModule();
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/** TileSize setter */
	void setTileSize(float NewTileSize);
	void setStepByStep(ECheckBoxState checkBoxState);

	float TileSize = 1.0;
	bool StepByStep = true;
	int activeFloor = 0;
	int floors = 5;

	/** This function will be bound to Command. */
	void PluginButtonClicked();

private:

	FOnClicked buttonClicked;
	FOnClicked button1Clicked;
	FOnClicked button2Clicked;
	FOnClicked GenerateRandomGroundButtonClicked;
	FOnFloatValueChanged floatValueChanged;
	FOnCheckStateChanged checkStateChanged;
	FReply openModuleCreator();
	FReply openHouseGenerator();
	FReply finishFloorClicked();
	FReply generateRandomCrossGround();

private:

	void checkChange() {
		UE_LOG(LogTemp, Warning, TEXT("helllos"));
	}

	void AddToolbarExtension(FToolBarBuilder& Builder);
	void AddMenuExtension(FMenuBuilder& Builder);


	TSharedRef<class SDockTab> OnSpawnPluginTab(const class FSpawnTabArgs& SpawnTabArgs);


private:
	TSharedPtr<class FUICommandList> PluginCommands;
};
