// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SetupBuildingBockVolume.generated.h"

UCLASS()
class MODULARHOUSES_API ASetupBuildingBockVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASetupBuildingBockVolume();

	UPROPERTY()
	UStaticMesh* ReferenceVolumeMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* ReferenceVolumeMeshComponent;

	// Mesh that represents the module that need to be setup
	UPROPERTY(EditAnywhere, Category = "Building Block Setup")
	UStaticMesh* DummyModuleMesh;

	UPROPERTY()
	UStaticMeshComponent* DummyModuleMeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void SetupReferenceVolume();

	UFUNCTION()
	void LoadModuleMesh();

	UFUNCTION()
	void SetMeshToModuleComponent(UStaticMesh* Mesh);

	UFUNCTION()
	FString	JsonFullPath(FString Path);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Building Block Setup")
	void SaveBuildingBlockDimension();
};
