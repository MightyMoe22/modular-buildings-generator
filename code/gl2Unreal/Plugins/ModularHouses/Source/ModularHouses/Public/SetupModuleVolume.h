// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SetupModuleVolume.generated.h"

UCLASS()
class MODULARHOUSES_API ASetupModuleVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASetupModuleVolume();
	// Mesh that represent the actor itself
	UPROPERTY()
	UStaticMesh* ReferenceVolumeMesh;

	UPROPERTY(SimpleDisplay)
	UStaticMeshComponent* ReferenceVolumeMeshComponent;

	// Mesh that represents the module that need to be setup
	UPROPERTY(EditAnywhere, Category = "Module Setup")
	UStaticMesh* ModuleMesh;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* ModuleMeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetupReferenceVolume();

	UFUNCTION()
	void LoadModuleMesh();

	UFUNCTION()
	void SetMeshToModuleComponent(UStaticMesh* Mesh);

	UFUNCTION()
	FString	JsonFullPath(FString Path);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	UFUNCTION(CallInEditor, Category = "Module Setup")
	void RemoveModuleMesh();

	UFUNCTION(CallInEditor, Category = "Module Setup")
	void SaveModule();
};
