// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "ModularBuildingSettingStyle.h"

class FModularBuildingSettingCommands : public TCommands<FModularBuildingSettingCommands>
{
public:

	FModularBuildingSettingCommands()
		: TCommands<FModularBuildingSettingCommands>(TEXT("ModularHouses"), 
													 NSLOCTEXT("Contexts", "ModularHouses", "ModularHouses Plugin"), 
													 NAME_None, FModularBuildingSettingStyle::GetStyleSetName())
	{}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};