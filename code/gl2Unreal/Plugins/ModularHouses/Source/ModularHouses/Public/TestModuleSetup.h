// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Modules/ModuleContainer.h"
#include "TestModuleSetup.generated.h"

UCLASS()
class MODULARHOUSES_API ATestModuleSetup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestModuleSetup();

	UPROPERTY()
	UStaticMesh* ReferenceVolumeMesh;

	UPROPERTY(SimpleDisplay)
	UStaticMeshComponent* ReferenceVolumeMeshComponent;

	// Mesh that represents the module that need to be setup
	UPROPERTY()
	UStaticMesh* ModuleMesh;

	UPROPERTY(VisibleAnywhere, Category = "Test Module Setup")
	UStaticMeshComponent* ModuleMeshComponent;

	UPROPERTY(EditAnywhere, Category = "Test Module Setup")
	FString NameOfModuleToLoad;

private:

	UPROPERTY(VisibleAnywhere, Category = "Test Module Setup")
	UModuleContainer* ModuleContainer;

protected:

	UFUNCTION()
	void SetupReferenceVolume();

	UFUNCTION()
	FString	JsonFullPath(FString Path);

	UFUNCTION()
	void SetMeshToModuleComponent(UStaticMesh* Mesh, FVector Position);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(CallInEditor, Category = "Test Module Setup")
	void LoadModule();

private:

	UFUNCTION()
	void LoadModuleContainer();

};
