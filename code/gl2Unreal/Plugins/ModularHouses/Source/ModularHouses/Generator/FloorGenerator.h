// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Generator/GroundGeneratorInterface.h"
#include "FloorManager.h"
#include "Generator/Stairs/StairGenerator.h"
#include "FloorGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UFloorGenerator : public UObject
{

	GENERATED_BODY()


public:
	UFloorManager* floorManager;
	TScriptInterface<IGroundGeneratorInterface> generator;
	
	void GenerateFloors(FIntVector newSize);

private:
	UGroundGrid* deepCopy(UGroundGrid* oldGroundGrid);
	void generateStairs();
};
