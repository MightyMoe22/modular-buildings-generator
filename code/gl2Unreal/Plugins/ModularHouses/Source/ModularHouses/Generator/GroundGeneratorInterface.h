// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Grid/GroundGrid.h"
#include "GroundGeneratorData.h"
#include "GroundGeneratorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGroundGeneratorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IGroundGeneratorInterface
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UGroundGeneratorData* groundGeneratorData;

	UFUNCTION(BlueprintNativeEvent)
	UGroundGrid* generateGroundGridForActiveFloor();
};
