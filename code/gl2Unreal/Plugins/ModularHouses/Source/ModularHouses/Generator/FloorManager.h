// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Grid/GroundGrid.h"
#include "FloorManager.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UFloorManager : public UObject
{
	GENERATED_BODY()

	TArray<UGroundGrid*> floors = TArray<UGroundGrid*>();

public:
	FIntVector Size = FIntVector();
	//TScriptInterface<IGroundGeneratorInterface> generator;

	void initialize(FIntVector newSize);
	void addFloor(UGroundGrid* grid);
	UGroundGrid* getFloor(int i);
	std::list<FGridTileStruct>* getGridTileAtPosition(FIntVector position);
	void addGridTileAtPosition(FGridTileStruct gridTile, FIntVector position);
};
