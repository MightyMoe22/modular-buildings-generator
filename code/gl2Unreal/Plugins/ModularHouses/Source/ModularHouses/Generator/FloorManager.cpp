// Copyright Maurice Langer und Jakob Seitz 2020


#include "FloorManager.h"

void UFloorManager::initialize(FIntVector newSize)
{
	floors.Empty();
	Size = newSize;
}

void UFloorManager::addFloor(UGroundGrid* grid)
{
	if (grid->Size != FIntPoint(Size.X, Size.Y))
		UE_LOG(LogTemp, Error, TEXT("Invalid Size. Size should be (%i, %i) but is (%i, %i)"), Size.X, Size.Y, grid->Size.X, grid->Size.Y);

	if (floors.Num() - 1 >= Size.Z)
		UE_LOG(LogTemp, Error, TEXT("Invalid index. Too many floors in floor-manager. Floors: %i, Height: %i"), floors.Num(), Size.Z+1);

	floors.Add(grid);
}

UGroundGrid* UFloorManager::getFloor(int i)
{
	if (floors.IsValidIndex(i))
		return floors[i];
	UE_LOG(LogTemp, Error, TEXT("Invalid Index. FloorManager doesnt know the floor %i"), i);
	return nullptr;
}

std::list<FGridTileStruct>* UFloorManager::getGridTileAtPosition(FIntVector position)
{
	return getFloor(position.Z)->getGridTilesAtPosition({ position.X, position.Y });
}

void UFloorManager::addGridTileAtPosition(FGridTileStruct gridTile, FIntVector position)
{
	getFloor(position.Z)->addGridTileAtPosition(gridTile, { position.X, position.Y });
}
