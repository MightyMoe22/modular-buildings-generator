// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Shapes/Rectangle/RectangleMerger/RectangleMerger.h"
#include "GroundPlan.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UGroundPlan : public UObject
{
	GENERATED_BODY()

public:
	URectangleMerger* mergedGroundRectangles;
	URectangleMerger* mergedRoofRectangles;
	URectangleMerger* roomRectangles;

	UGroundPlan();

	void createRoomRectangles();
};
