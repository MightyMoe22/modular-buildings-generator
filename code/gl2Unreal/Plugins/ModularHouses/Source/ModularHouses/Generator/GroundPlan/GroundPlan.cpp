// Copyright Maurice Langer und Jakob Seitz 2020


#include "GroundPlan.h"

UGroundPlan::UGroundPlan()
{
	mergedGroundRectangles = NewObject<URectangleMerger>(this, URectangleMerger::StaticClass(), FName("groundRectangles"));
	mergedRoofRectangles = NewObject<URectangleMerger>(this, URectangleMerger::StaticClass(), FName("roofRectangles"));
	roomRectangles = NewObject<URectangleMerger>(this, URectangleMerger::StaticClass(), FName("roomRectangles"));
}

void UGroundPlan::createRoomRectangles()
{
	roomRectangles->clearRectangles();
	for (auto rectangle : mergedGroundRectangles->rectangles)
		roomRectangles->addRectangle(rectangle);
	for (auto rectangle : mergedRoofRectangles->rectangles)
		roomRectangles->addRectangle(rectangle);
	roomRectangles = roomRectangles->getscaledURectangleMerger(1);
}
