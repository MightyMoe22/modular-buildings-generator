// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GroundGeneratorData.generated.h"

UCLASS(Blueprintable, ClassGroup = (House), meta = (BlueprintSpawnableComponent))
class MODULARHOUSES_API UGroundGeneratorData : public UObject
{
	GENERATED_BODY()
public:
	//general
	UPROPERTY(BlueprintReadWrite)
	FIntVector Size = {20, 15, 10};
	UPROPERTY(BlueprintReadWrite)
	int activeFloor;

	//room
	UPROPERTY(BlueprintReadWrite)
	int minRoomSize = 5;
	UPROPERTY(BlueprintReadWrite)
	int maxRoomSize;
	
	//roof
	UPROPERTY(BlueprintReadWrite)
	int minRoofHeight = 3;
	UPROPERTY(BlueprintReadWrite)
	int maxRoofHeight = 5;

	UPROPERTY(BlueprintReadWrite)
	int minRoofStart = 1;
	UPROPERTY(BlueprintReadWrite)
	int maxRoofStart = 5;
	
	UPROPERTY(BlueprintReadWrite)
	int minRoofDistance = 2;
	UPROPERTY(BlueprintReadWrite)
	int maxRoofDistance = 3;

	UPROPERTY(BlueprintReadWrite)
	int minRoofNumber = 1;
	UPROPERTY(BlueprintReadWrite)
	int maxRoofNumber = 3;

	//roofGenerators
	UPROPERTY(BlueprintReadWrite)
	bool generateGableRoofs = true;
	UPROPERTY(BlueprintReadWrite)
	bool generateFlatRoofs = false;

	UPROPERTY(BlueprintReadWrite)
	int minEntryDoors = 2;
	UPROPERTY(BlueprintReadWrite)
	int maxEntryDoors = 5;

	UPROPERTY(BlueprintReadWrite)
	int minStairs = 1;
	UPROPERTY(BlueprintReadWrite)
	int maxStairs = 4;

	//Randomness
	FRandomStream stream = FRandomStream();

	UFUNCTION(BlueprintCallable)
	void randomizeStream();

	UFUNCTION(BlueprintCallable)
	void setRandomStream(int seed);

	UFUNCTION(BlueprintCallable)
	FRandomStream getRandomStream();
};
