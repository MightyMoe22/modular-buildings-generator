// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Grid/GroundGrid.h"
#include "SubGeneratorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USubGeneratorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API ISubGeneratorInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//UFUNCTION(BlueprintNativeEvent)   Problem: Keine einheitlichen Parameter
	//void initialize();

	//UFUNCTION(BlueprintNativeEvent)	Problem: Keine einheitlichen Parameter
	//void generate();

	//UFUNCTION(BlueprintNativeEvent)	Problem: RoomGenerator wird nicht geupdated sondern generiert jedesmal komplett neu
	//void update();

	UFUNCTION(BlueprintNativeEvent)
	void draw(UGroundGrid* groundGrid);
};
