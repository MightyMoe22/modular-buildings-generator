// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofConnectionOut.h"

void URoofConnectionOut::initRoofConnection(int length, FIntPoint ObenLinks, EModuleDirection direction)
{
	gridTiles.Add(FGridTileStruct(EModuleCategory::R_Filler, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Outside));

	if (direction == EModuleDirection::North || direction == EModuleDirection::South)
		initComponent({ length, 1 }, ObenLinks);
	else
		initComponent({ 1, length }, ObenLinks);
}

TArray<FGridTileStruct> URoofConnectionOut::getGridTilesAtPosition(FIntPoint position)
{
	for (auto rectangle : cuttingHouseRectangles) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position) && !rectangle->Execute_isPositionBorderOfShape(rectangle.GetObject(), position)) return {};
	}
	for (auto rectangle : cuttingRoofs) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position)) return {};
	}
	return gridTiles;
}

void URoofConnectionOut::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	//Case Edge
	if (isPositionCornerOfRectangle(position)) {
		auto newGridTile = FGridTileStruct(EModuleCategory::R_E_Filler, gridTile.moduleDirection, gridTile.moduleEnvironment, gridTile.moduleFacing);

		switch (gridTile.moduleDirection)
		{
		case EModuleDirection::South:
			if (position == BottomRight) newGridTile.flippedAtDirection = EModuleDirection::East;
			break;
		case EModuleDirection::West:
			if (position == BottomLeft) newGridTile.flippedAtDirection = EModuleDirection::South;
			break;
		case EModuleDirection::North:
			if (position == TopLeft) newGridTile.flippedAtDirection = EModuleDirection::West;
			break;
		case EModuleDirection::East:
			if (position == TopRight) newGridTile.flippedAtDirection = EModuleDirection::North;
			break;
		default:
			break;
		}
		
		grid->addGridTileAtPosition(newGridTile, position);
		return;
	}
	//else
	Super::addGridTileAtPosition(gridTile, grid, position);
}
