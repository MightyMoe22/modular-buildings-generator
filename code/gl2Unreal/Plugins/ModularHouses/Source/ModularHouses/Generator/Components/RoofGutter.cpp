// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofGutter.h"

void URoofGutter::initGutter(int length, FIntPoint ObenLinks, EModuleDirection direction)
{
	gridTiles.Add(FGridTileStruct(EModuleCategory::R_Gutter, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Inside));
	gridTiles.Add(FGridTileStruct(EModuleCategory::R_Gutter, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Outside));

	if (direction == EModuleDirection::North || direction == EModuleDirection::South)
		initComponent({ length, 1 }, ObenLinks);
	else
		initComponent({ 1, length }, ObenLinks);
}

void URoofGutter::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	//Case Edge
	if (isPositionCornerOfRectangle(position)) {
		auto newGridTile = FGridTileStruct(EModuleCategory::R_E_Gutter, gridTile.moduleDirection, gridTile.moduleEnvironment, gridTile.moduleFacing);
		
		switch (gridTile.moduleDirection)
		{
		case EModuleDirection::North:
			if (position == BottomRight)
				newGridTile.flippedAtDirection = EModuleDirection::East;
			break;
		case EModuleDirection::East:
			if (position == BottomLeft)
				newGridTile.flippedAtDirection = EModuleDirection::South;
			break;
		case EModuleDirection::South:
			if (position == TopLeft)
				newGridTile.flippedAtDirection = EModuleDirection::West;
			break;
		case EModuleDirection::West:
			if (position == TopRight)
				newGridTile.flippedAtDirection = EModuleDirection::North;
			break;
		default:
			break;
		}
		
		grid->addGridTileAtPosition(newGridTile, position);
		return;
	}
	//else
	Super::addGridTileAtPosition(gridTile, grid, position);
}
