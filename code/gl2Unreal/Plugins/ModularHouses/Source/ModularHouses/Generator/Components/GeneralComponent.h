// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ComponentInterface.h"
#include "../Shapes/Rectangle/Rectangle.h"
#include "../FGridTileStruct.h"
#include "GeneralComponent.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UGeneralComponent : public URectangle, public IComponentInterface
{
	GENERATED_BODY()

public:
	TArray<FGridTileStruct> gridTiles;

	virtual void initComponent(FIntPoint Size, FIntPoint ObenLinks);
	virtual TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position);
	virtual void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	void printComponent();
	virtual void printComponent_Implementation();


	UFUNCTION(BlueprintNativeEvent)
	void printComponentOnGrid(UGroundGrid* grid);
	virtual void printComponentOnGrid_Implementation(UGroundGrid* grid);
};
