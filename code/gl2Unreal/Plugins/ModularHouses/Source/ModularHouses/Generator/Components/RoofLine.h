// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "RoofLine.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofLine : public UCuttableComponent
{
	GENERATED_BODY()
public:
	void initRoof(int length, FIntPoint ObenLinks, EModuleDirection direction);
	void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position) override;
};
