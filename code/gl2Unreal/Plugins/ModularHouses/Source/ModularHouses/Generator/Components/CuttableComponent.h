// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GeneralComponent.h"
#include "CuttableComponent.generated.h"

/**
 * A cuttable component holds a list of cuttingRoofs and a list of cuttingHouseRectangles
 * he implements the getGridTilesAtPosition-function wich only returns a gridTile if position not cut
 * should be overwritten if desired
 */
UCLASS()
class MODULARHOUSES_API UCuttableComponent : public UGeneralComponent
{
	GENERATED_BODY()
public:
	TArray<TScriptInterface<IRectangleInterface>> cuttingRoofs;
	TArray<TScriptInterface<IRectangleInterface>> cuttingHouseRectangles;
	virtual TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;

	bool isPositionCutted(FIntPoint position);
};
