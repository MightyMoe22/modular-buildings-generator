// Copyright Maurice Langer und Jakob Seitz 2020


#include "GeneralComponent.h"

void UGeneralComponent::initComponent(FIntPoint newSize, FIntPoint newObenLinks)
{
	Execute_createRectangle(this, newSize, newObenLinks);
}

TArray<FGridTileStruct> UGeneralComponent::getGridTilesAtPosition(FIntPoint position)
{
	return gridTiles;
}

void UGeneralComponent::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	grid->addGridTileAtPosition(gridTile, position);
}

void UGeneralComponent::printComponent_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT(""));
	UE_LOG(LogTemp, Warning, TEXT("PrintComponent: "));
	UE_LOG(LogTemp, Warning, TEXT("Size: %i %i"), Size.X, Size.Y);
	UE_LOG(LogTemp, Warning, TEXT("ObenLinks: %i %i"), TopLeft.X, TopLeft.Y);

	for (auto gridTile : gridTiles) {
		switch (gridTile.moduleDirection)
		{
		case EModuleDirection::North:
			UE_LOG(LogTemp, Warning, TEXT("Direction: North"));
			break;
		case EModuleDirection::East:
			UE_LOG(LogTemp, Warning, TEXT("Direction: East"));
			break;
		case EModuleDirection::South:
			UE_LOG(LogTemp, Warning, TEXT("Direction: South"));
			break;
		case EModuleDirection::West:
			UE_LOG(LogTemp, Warning, TEXT("Direction: West"));
			break;
		default:
			break;
		}
		switch (gridTile.moduleCategory)
		{
		case EModuleCategory::Roof:
			UE_LOG(LogTemp, Warning, TEXT("Category: Roof"));
			break;
		default:
			UE_LOG(LogTemp, Error, TEXT("Category: ?"));
			break;
		}
	}
}

void UGeneralComponent::printComponentOnGrid_Implementation(UGroundGrid* grid)
{
	for (int x = TopLeft.X; x < TopLeft.X + Size.X; x++)
	{
		for (int y = TopLeft.Y; y < TopLeft.Y + Size.Y; y++)
		{
			FIntPoint position = { x,y };
			for (FGridTileStruct gridTile : getGridTilesAtPosition(position))
				addGridTileAtPosition(gridTile, grid, position);
		}
	}
}
