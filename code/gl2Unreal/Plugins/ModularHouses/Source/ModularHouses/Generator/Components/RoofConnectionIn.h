// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "RoofConnectionIn.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofConnectionIn : public UCuttableComponent
{
	GENERATED_BODY()
public:
	void initRoofConnection(int length, FIntPoint ObenLinks, EModuleDirection direction);
	TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;
};
