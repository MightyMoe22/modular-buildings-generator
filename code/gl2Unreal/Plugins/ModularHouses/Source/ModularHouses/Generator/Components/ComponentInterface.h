// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Grid/GroundGrid.h"
#include "ComponentInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UComponentInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IComponentInterface
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent)
	void printComponent();

	UFUNCTION(BlueprintNativeEvent)
	void printComponentOnGrid(UGroundGrid* grid);

};
