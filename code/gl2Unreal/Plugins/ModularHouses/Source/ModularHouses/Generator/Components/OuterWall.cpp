// Copyright Maurice Langer und Jakob Seitz 2020


#include "OuterWall.h"

TMap<FIntPoint, EModuleDirection> UOuterWall::placedWalls = TMap<FIntPoint, EModuleDirection>();

void UOuterWall::InitWall(int length, FIntPoint ObenLinks, EModuleDirection direction)
{
	TopLeft = ObenLinks;

	gridTiles.Add(FGridTileStruct(EModuleCategory::Window, FDirectionManipulator().rotateDirection(direction, 180), EModuleEnvironment::Outdoor, EModuleFacing::Facing_Inside));
	gridTiles.Add(FGridTileStruct(EModuleCategory::Window, direction, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));

	if (direction == EModuleDirection::North || direction == EModuleDirection::South)
		initComponent({ length, 2 }, ObenLinks);
	else
		initComponent({ 2, length }, ObenLinks);

	if (direction == EModuleDirection::North) {
		outside = [](UOuterWall* self, FIntPoint position) { return position.Y == self->TopLeft.Y; };
		inside = [](UOuterWall* self, FIntPoint position) { return position.Y == self->BottomLeft.Y; };
	}
	if (direction == EModuleDirection::South) {
		outside = [](UOuterWall* self, FIntPoint position) { return position.Y == self->BottomLeft.Y; };
		inside = [](UOuterWall* self, FIntPoint position) { return position.Y == self->TopLeft.Y; };
	}
	if (direction == EModuleDirection::East) {
		outside = [](UOuterWall* self, FIntPoint position) { return position.X == self->TopRight.X; };
		inside = [](UOuterWall* self, FIntPoint position) { return position.X == self->TopLeft.X; };
	}
	if (direction == EModuleDirection::West) {
		outside = [](UOuterWall* self, FIntPoint position) { return position.X == self->TopLeft.X; };
		inside = [](UOuterWall* self, FIntPoint position) { return position.X == self->TopRight.X; };
	}
}

TArray<FGridTileStruct> UOuterWall::getGridTilesAtPosition(FIntPoint position)
{
	if (isPositionCutted(position)) return {};

	if (corners.Contains(position))
		return { corners[position] };

	auto returnGridTiles = TArray<FGridTileStruct>();

	if (gridTiles.Num() != 2)
		UE_LOG(LogTemp, Warning, TEXT("length of gridTiles of outerWall is not 2"));
	if (!isPositionCornerOfRectangle(position)) {
		if (outside(this, position)) {
			returnGridTiles.Add(gridTiles[0]);
			placedWalls.Add(position);
			placedWalls[position] = gridTiles[0].moduleDirection;
		}
		else if (inside(this, position)) {
			returnGridTiles.Add(gridTiles[1]);
			placedWalls.Add(position);
			placedWalls[position] = gridTiles[1].moduleDirection;
		}
	}

	return returnGridTiles;
}

void UOuterWall::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	if (outside(this, position) && !isPositionCornerOfRectangle(position)) {
		auto gridTilesRef = *grid->getGridTilesAtPosition(position);
		for (auto tile : gridTilesRef) {
			if (tile.moduleCategory == EModuleCategory::Roof) {
				auto wall = gridTile;
				wall.moduleCategory = EModuleCategory::Wall;
				return Super::addGridTileAtPosition(wall, grid, position);
			}
		}
	}
	Super::addGridTileAtPosition(gridTile, grid, position);
}

void UOuterWall::addCorner(FIntPoint position, EModuleDirection direction)
{
	auto gridTile = FGridTileStruct(EModuleCategory::Pillar, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Inside);
	corners.Add(position);
	corners[position] = gridTile;
}
