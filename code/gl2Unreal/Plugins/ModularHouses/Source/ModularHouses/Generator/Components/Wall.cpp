// Copyright Maurice Langer und Jakob Seitz 2020


#include "Wall.h"

void UWall::InitWall(int length, FIntPoint ObenLinks, EModuleDirection newDirection)
{
	this->direction = newDirection;
	gridTiles.Add(FGridTileStruct(EModuleCategory::Wall, newDirection, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));
	
	if (newDirection == EModuleDirection::North || newDirection == EModuleDirection::South)
		initComponent({length, 1}, ObenLinks);
	else
		initComponent({1, length}, ObenLinks);
}

void UWall::printComponentOnGrid_Implementation(UGroundGrid* grid)
{
	for (int x = TopLeft.X; x < TopLeft.X + Size.X; x++)
	{
		for (int y = TopLeft.Y; y < TopLeft.Y + Size.Y; y++)
		{
			FIntPoint position = {x, y};
			auto newGridTiles = grid->getGridTilesAtPosition(position);

			//if gridTiles contain a Window place nothing
			for (FGridTileStruct gridTile : *newGridTiles) {
				if ((gridTile.moduleCategory == EModuleCategory::Window 
					|| gridTile.moduleCategory == EModuleCategory::Door)
					&& gridTile.moduleDirection == direction) 
				{
					goto cnt; //continue outer loop
					//Special case were goto should be used. Src: https://docs.microsoft.com/de-de/cpp/cpp/goto-statement-cpp?view=vs-2019
				}
			}

			//else continue as usual
			for (FGridTileStruct gridTile : getGridTilesAtPosition(position))
				addGridTileAtPosition(gridTile, grid, position);

			cnt:;
		}
	}
}