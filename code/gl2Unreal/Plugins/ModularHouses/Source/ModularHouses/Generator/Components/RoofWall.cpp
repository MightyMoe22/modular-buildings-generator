// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofWall.h"

void URoofWall::InitWall(int length, FIntPoint ObenLinks, EModuleDirection newDirection)
{
	this->direction = FDirectionManipulator().rotateDirection(newDirection, 180);
	Super::InitWall(length, ObenLinks, newDirection);
}

void URoofWall::setLeftAndRightPoint(FIntPoint newLeftCorner, FIntPoint newRightCorner)
{
	this->LeftCorner = newLeftCorner;
	this->RightCorner = newRightCorner;
}

TArray<FGridTileStruct> URoofWall::getGridTilesAtPosition(FIntPoint position)
{
	if (position == LeftCorner)
		return { FGridTileStruct(EModuleCategory::R_Wall, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Left) };
	if (position == RightCorner)
		return { FGridTileStruct(EModuleCategory::R_Wall, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Right) };
	return Super::getGridTilesAtPosition(position);
}
