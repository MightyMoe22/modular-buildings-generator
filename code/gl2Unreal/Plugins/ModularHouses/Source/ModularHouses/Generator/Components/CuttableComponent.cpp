// Copyright Maurice Langer und Jakob Seitz 2020


#include "CuttableComponent.h"

TArray<FGridTileStruct> UCuttableComponent::getGridTilesAtPosition(FIntPoint position)
{
	if (isPositionCutted(position)) return {};
	return gridTiles;
}

bool UCuttableComponent::isPositionCutted(FIntPoint position)
{
	for (auto rectangle : cuttingHouseRectangles) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position) && !rectangle->Execute_isPositionBorderOfShape(rectangle.GetObject(), position)) return true;
	}
	for (auto rectangle : cuttingRoofs) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position) && !rectangle->Execute_isPositionBorderOfShape(rectangle.GetObject(), position)) return true;
	}
	return false;
}
