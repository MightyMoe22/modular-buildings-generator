// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofLine.h"

void URoofLine::initRoof(int length, FIntPoint ObenLinks, EModuleDirection direction)
{

	gridTiles.Add(FGridTileStruct(EModuleCategory::Roof, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Outside));
	gridTiles.Add(FGridTileStruct(EModuleCategory::Roof, direction, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));
	gridTiles.Add(FGridTileStruct(EModuleCategory::R_Filler, direction, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));

	if (direction == EModuleDirection::North || direction == EModuleDirection::South)
		initComponent({ length, 1 }, ObenLinks);
	else
		initComponent({ 1, length }, ObenLinks);
}

void URoofLine::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	//Case Edge
	if (isPositionCornerOfRectangle(position)) {
		auto newGridTile = FGridTileStruct(EModuleCategory::R_Edge, gridTile.moduleDirection, gridTile.moduleEnvironment, gridTile.moduleFacing);
		

		switch (gridTile.moduleDirection)
		{
		case EModuleDirection::North:
			if (position == BottomRight) {
				newGridTile.flippedAtDirection = EModuleDirection::East;
			}
			break;
		case EModuleDirection::East:
			if (position == BottomLeft) {
				newGridTile.flippedAtDirection = EModuleDirection::South;
			}
			break;
		case EModuleDirection::South:
			if (position == TopLeft) {
				newGridTile.flippedAtDirection = EModuleDirection::West;
			}
			break;
		case EModuleDirection::West:
			if (position == TopRight) {
				newGridTile.flippedAtDirection = EModuleDirection::North;
			}
			break;
		default:
			break;
		}
		
		grid->addGridTileAtPosition(newGridTile, position);
		return;
	}

	//Case Corner
	if (gridTile.moduleCategory == EModuleCategory::Roof) {
		auto newGridTiles = grid->getGridTilesAtPosition(position);
		auto gridTilesRef = *newGridTiles;
		for (auto listGridTile : gridTilesRef) {
			if (listGridTile.moduleCategory == gridTile.moduleCategory && listGridTile.moduleFacing == gridTile.moduleFacing) {
				FDirectionManipulator directionManipulator = FDirectionManipulator();
				auto direction = directionManipulator.mergeDirections(gridTile.moduleDirection, listGridTile.moduleDirection);
				auto category = EModuleCategory::R_Corner;
				auto newGridTile = FGridTileStruct(category, direction, listGridTile.moduleEnvironment, listGridTile.moduleFacing);
				newGridTiles->remove(listGridTile);
				grid->addGridTileAtPosition(newGridTile, position);
				return;
			}
		}
	}

	//else
	Super::addGridTileAtPosition(gridTile, grid, position);
}
