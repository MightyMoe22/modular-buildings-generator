// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "RoofGutter.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofGutter : public UCuttableComponent
{
	GENERATED_BODY()

	EModuleDirection roofDirection = EModuleDirection::None;
public:
	void initGutter(int length, FIntPoint ObenLinks, EModuleDirection direction);
	void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position) override;
};
