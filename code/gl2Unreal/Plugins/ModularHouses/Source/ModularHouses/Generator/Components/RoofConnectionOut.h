// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "RoofConnectionOut.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofConnectionOut : public UCuttableComponent
{
	GENERATED_BODY()
public:
	void initRoofConnection(int length, FIntPoint ObenLinks, EModuleDirection direction);
	TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;
	void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position) override;
};
