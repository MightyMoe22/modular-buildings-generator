// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "OuterWall.h"
#include "RoofWall.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofWall : public UOuterWall
{
	GENERATED_BODY()

private:
	EModuleDirection direction;
	FIntPoint LeftCorner;
	FIntPoint RightCorner;
public:
	void InitWall(int length, FIntPoint ObenLinks, EModuleDirection direction) override;
	void setLeftAndRightPoint(FIntPoint LeftCorner, FIntPoint RightCorner);
	TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;
};
