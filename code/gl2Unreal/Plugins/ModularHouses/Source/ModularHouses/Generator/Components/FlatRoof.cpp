// Copyright Maurice Langer und Jakob Seitz 2020


#include "FlatRoof.h"

void UFlatRoof::initComponent(FIntPoint newSize, FIntPoint ObenLinks)
{
	gridTiles.Add(FGridTileStruct(EModuleCategory::Flatroof, EModuleDirection::None, EModuleEnvironment::Outdoor, EModuleFacing::None));
	Super::initComponent(newSize, ObenLinks);
}

TArray<FGridTileStruct> UFlatRoof::getGridTilesAtPosition(FIntPoint position)
{
	if (isPositionCutted(position)) return {};
	if (isPositionBorderOfShape(position)) {
		auto direction = FDirectionManipulator::rotateDirection(getDirectionForPosition(position), 180);
		if (isPositionCornerOfRectangle(position))
			return { FGridTileStruct(EModuleCategory::Flatroof_Corner, direction, EModuleEnvironment::Outdoor, EModuleFacing::None) };
		return { FGridTileStruct(EModuleCategory::Flatroof_Edge, direction, EModuleEnvironment::Outdoor, EModuleFacing::None) };
	}
	return Super::getGridTilesAtPosition(position);
}

void UFlatRoof::addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position)
{
	auto newGridTiles = grid->getGridTilesAtPosition(position);
	bool edge = false;
	bool placeWithoutSizeCheck = false;
	for (auto gT : *newGridTiles) {
		if (gT.moduleCategory == EModuleCategory::Flatroof_Edge)
			edge = true;
		if (gT.moduleEnvironment == EModuleEnvironment::Outdoor && gT.moduleCategory != EModuleCategory::Flatroof )
			placeWithoutSizeCheck = true;
		if (gT.moduleEnvironment == EModuleEnvironment::Indoor) {
			return;
		}
	}
	if (edge) {
		if (gridTile.moduleCategory == EModuleCategory::Flatroof_Edge)
			return Super::addGridTileAtPosition(gridTile, grid, position);
		newGridTiles->clear();
	}
	if (newGridTiles->size() == 0 || placeWithoutSizeCheck)
		Super::addGridTileAtPosition(gridTile, grid, position);
}
