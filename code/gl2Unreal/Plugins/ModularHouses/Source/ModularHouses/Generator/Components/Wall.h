// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "algorithm"
#include "GeneralComponent.h"
#include "Wall.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UWall : public UGeneralComponent
{
	GENERATED_BODY()

    EModuleDirection direction;

public:
    void InitWall(int length, FIntPoint ObenLinks, EModuleDirection direction);
    void printComponentOnGrid_Implementation(UGroundGrid* grid) override;
};
