// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "OuterWall.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UOuterWall : public UCuttableComponent
{
	GENERATED_BODY()
    
private:
    TMap<FIntPoint, FGridTileStruct> corners;
    
    protected:
        bool (*inside)(UOuterWall*, FIntPoint) = [](UOuterWall*, FIntPoint position) {return true;};
        bool (*outside)(UOuterWall*, FIntPoint) = [](UOuterWall*, FIntPoint position) { return true;};

    public:
    static TMap<FIntPoint, EModuleDirection> placedWalls;

    virtual void InitWall(int length, FIntPoint ObenLinks, EModuleDirection direction);
	virtual TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;
	void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position) override;

    void addCorner(FIntPoint position, EModuleDirection direction);
};