// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofConnectionIn.h"
//The roof_connection_in is only placed at the Edge of the roof (MC_R_E_Filler)
void URoofConnectionIn::initRoofConnection(int length, FIntPoint ObenLinks, EModuleDirection direction)
{
	gridTiles.Add(FGridTileStruct(EModuleCategory::R_E_Filler, direction, EModuleEnvironment::Outdoor, EModuleFacing::Facing_Inside));

	if (direction == EModuleDirection::North || direction == EModuleDirection::South)
		initComponent({ length, 1 }, ObenLinks);
	else
		initComponent({ 1, length }, ObenLinks);
}

TArray<FGridTileStruct> URoofConnectionIn::getGridTilesAtPosition(FIntPoint position)
{
	if (isPositionCornerOfRectangle(position)) {
		auto newGridTile = Super::getGridTilesAtPosition(position)[0];

		switch (newGridTile.moduleDirection)
		{
		case EModuleDirection::North:
			if (position == BottomRight) newGridTile.flippedAtDirection = EModuleDirection::East;
			break;
		case EModuleDirection::East:
			if (position == BottomLeft) newGridTile.flippedAtDirection = EModuleDirection::South;
			break;
		case EModuleDirection::South:
			if (position == TopLeft) newGridTile.flippedAtDirection = EModuleDirection::West;
			break;
		case EModuleDirection::West:
			if (position == TopRight) newGridTile.flippedAtDirection = EModuleDirection::North;
			break;
		default:
			break;
		}
		return { newGridTile };
	}
	else return {};
}