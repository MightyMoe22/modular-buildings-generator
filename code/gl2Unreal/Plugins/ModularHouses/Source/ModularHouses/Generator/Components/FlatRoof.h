// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CuttableComponent.h"
#include "FlatRoof.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UFlatRoof : public UCuttableComponent
{
	GENERATED_BODY()

public:
	void initComponent(FIntPoint Size, FIntPoint ObenLinks) override;

	TArray<FGridTileStruct> getGridTilesAtPosition(FIntPoint position) override;
	void addGridTileAtPosition(FGridTileStruct gridTile, UGroundGrid* grid, FIntPoint position) override;
};
