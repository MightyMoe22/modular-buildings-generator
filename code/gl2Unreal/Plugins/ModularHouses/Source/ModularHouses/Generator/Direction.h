// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"

UENUM()
enum Direction {
	D_North		UMETA(DisplayName = "North"),
	D_East		UMETA(DisplayName = "East"),
	D_South		UMETA(DisplayName = "South"),
	D_West		UMETA(DisplayName = "West")
};

UENUM(BlueprintType)
enum class EDirection : uint8
{
	North = 0 UMETA(DisplayName = "North"),
	East = 1 UMETA(DisplayName = "East"),
	South = 2 UMETA(DisplayName = "South"),
	West = 3 UMETA(DisplayName = "West"),
	Up = 4 UMETA(DisplayName = "Up"),
	Down = 5 UMETA(DisplayName = "Down"),
	Here = 6 UMETA(DisplayName = "Here")
};