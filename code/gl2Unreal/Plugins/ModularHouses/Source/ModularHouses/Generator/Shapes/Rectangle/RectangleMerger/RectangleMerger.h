// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Rectangle.h"
#include "RectangleMerger.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URectangleMerger : public UObject
{
	GENERATED_BODY()

	int border = 1;
public:
	TArray<TScriptInterface<IRectangleInterface>> rectangles;
	void mergeRectangles(TArray<TScriptInterface<IRectangleInterface>> newRectangles);
	void addRectangle(TScriptInterface<IRectangleInterface> rectangle);
	void clearRectangles();
	void setBorderForAllRectangles(int i);
	
	bool isPositionInARectangle(FIntPoint position);
	bool isPositionBorderOfRectangles(FIntPoint position);
	bool isPositionOuterCornerOfRectangles(FIntPoint position);
	bool isPositionInnerCornerOfRectangles(FIntPoint position);
	TArray<EModuleDirection> getDirectionsForPosition(FIntPoint position);
	TArray<TScriptInterface<IRectangleInterface>> rectangalizeMergedArea();
	URectangleMerger* getscaledURectangleMerger(int scale);

	TArray<TScriptInterface<IRectangleInterface>> getHorizontalCrossedRectangles(TScriptInterface<IRectangleInterface> rectangle);
	TArray<TScriptInterface<IRectangleInterface>> getVerticalCrossedRectangles(TScriptInterface<IRectangleInterface> rectangle);
};
