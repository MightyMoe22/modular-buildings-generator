// Copyright Maurice Langer und Jakob Seitz 2020


#include "RectangleMerger.h"


void URectangleMerger::mergeRectangles(TArray<TScriptInterface<IRectangleInterface>> newRectangles)
{
	rectangles = newRectangles;
}


void URectangleMerger::addRectangle(TScriptInterface<IRectangleInterface> rectangle)
{
	rectangles.Add(rectangle);
}

void URectangleMerger::clearRectangles()
{
	rectangles.Empty();
}

void URectangleMerger::setBorderForAllRectangles (int i)
{
	border = i;
	for (auto rectangle : rectangles) {
		rectangle->Execute_setBorder(rectangle.GetObject(), i);
	}
}

bool URectangleMerger::isPositionInARectangle(FIntPoint position)
{
	bool isInARectangle = false;
	for (auto rectangle : rectangles) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position))
			return true;
	}
	return false;
}

bool URectangleMerger::isPositionBorderOfRectangles(FIntPoint position) //tested with 2 rect
{
	if (!isPositionInARectangle(position)) return false; //Check if in rectangle

	for (auto rectangle : rectangles) {
		if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position) && !rectangle->Execute_isPositionBorderOfShape(rectangle.GetObject(), position))	//If in at least one inner-rectangle return false
			return false;
	}

	return true;
}

bool URectangleMerger::isPositionOuterCornerOfRectangles(FIntPoint position)
{
	//TODO
	if (!isPositionBorderOfRectangles(position))
		return false;
	
	bool edge;

	for (auto rectangle : rectangles) {
		if (rectangle->Execute_isPositionCornerOfRectangle(rectangle.GetObject(), position)) {
			edge = true;
		}
		else if (rectangle->Execute_isPositionInShape(rectangle.GetObject(), position)) {
			return false;
		}
	}
	return edge;
}

bool URectangleMerger::isPositionInnerCornerOfRectangles(FIntPoint position)
{
	//TODO
	return false;
}

TArray<EModuleDirection> URectangleMerger::getDirectionsForPosition(FIntPoint position)
{
	TArray<EModuleDirection> directions;
	EModuleDirection direction = EModuleDirection::None;
	for (auto rectangle : rectangles) {
		EModuleDirection tempDirection = rectangle->Execute_getDirectionForPosition(rectangle.GetObject(), position);
		if (tempDirection != EModuleDirection::None) {
			directions.Add(tempDirection);
		}
	}
	return directions;
}

TArray<TScriptInterface<IRectangleInterface>> URectangleMerger::rectangalizeMergedArea()
{
	TArray<TScriptInterface<IRectangleInterface>> smallRectangles;
	for (auto rectangle : rectangles) {
		
		TArray<TScriptInterface<IRectangleInterface>> overlappingRectangles = getHorizontalCrossedRectangles(rectangle);


		if (getHorizontalCrossedRectangles(rectangle).Num() == 0) {
			smallRectangles.Add(rectangle);
			continue;
		}

		overlappingRectangles.Sort([](TScriptInterface<IRectangleInterface> rectangle1, TScriptInterface<IRectangleInterface> rectangle2) {
			return rectangle1->TopLeft.X < rectangle2->TopLeft.X;
		});

		int x = rectangle->TopLeft.X;
		int y = rectangle->TopLeft.Y;
		FIntPoint topLeft;
		FIntPoint size;
		for (auto overlappingRectangle : overlappingRectangles) {
			URectangle* newRectangle = NewObject<URectangle>(this);
			topLeft = { x,y };
			size = { overlappingRectangle->TopLeft.X - x, rectangle->Size.Y };
			newRectangle->createRectangle(size, topLeft);
			x = overlappingRectangle->BottomRight.X + 1;
			smallRectangles.Add(newRectangle);
		}

		URectangle* newRectangle = NewObject<URectangle>(this);
		topLeft = { x,y };
		size = { rectangle->TopRight.X - x + 1, rectangle->Size.Y };
		newRectangle->createRectangle(size, topLeft);
		smallRectangles.Add(newRectangle);
	}
	return smallRectangles;
}

URectangleMerger* URectangleMerger::getscaledURectangleMerger(int scale)
{
	URectangleMerger* smallerRectangleMerger = NewObject<URectangleMerger>(this);
	for (auto rectangle : rectangles) {
		if (rectangle->Size.X - scale*2 <= 0 || rectangle->Size.Y - scale * 2 <= 0) continue;
		URectangle* newRectangle = NewObject<URectangle>(this);
		FIntPoint newSize = {rectangle->Size.X - scale * 2, rectangle->Size.Y - scale * 2 };
		FIntPoint newTopLeft = {rectangle->TopLeft.X + scale * 1 , rectangle->TopLeft.Y + scale * 1 };
		newRectangle->createRectangle(newSize, newTopLeft);
		smallerRectangleMerger->addRectangle(newRectangle);
	}
	return smallerRectangleMerger;
}

TArray<TScriptInterface<IRectangleInterface>> URectangleMerger::getHorizontalCrossedRectangles(TScriptInterface<IRectangleInterface> rectangle) //tested: looks like it works
{
	TArray<TScriptInterface<IRectangleInterface>> crossedRectangles;
	for (auto overlappingRectangle : rectangles) {
		if (overlappingRectangle == rectangle) continue;
		bool horizontal = rectangle->TopLeft.X < overlappingRectangle->TopLeft.X &&
			rectangle->TopRight.X > overlappingRectangle->TopRight.X;
		if (horizontal)
			crossedRectangles.Add(overlappingRectangle);
	}
	return crossedRectangles;
}

TArray<TScriptInterface<IRectangleInterface>> URectangleMerger::getVerticalCrossedRectangles(TScriptInterface<IRectangleInterface> rectangle)
{
	TArray<TScriptInterface<IRectangleInterface>> crossedRectangles;
	for (auto overlappingRectangle : rectangles) {
		if (overlappingRectangle == rectangle) continue;
		bool vertical = rectangle->TopLeft.Y < overlappingRectangle->TopLeft.Y&&
			rectangle->BottomLeft.Y > overlappingRectangle->BottomLeft.Y;
		if (vertical)
			crossedRectangles.Add(overlappingRectangle);
	}
	return crossedRectangles;
}