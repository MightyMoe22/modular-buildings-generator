// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Shape.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UShape : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IShape
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent)
	bool isPositionInShape(FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	bool isPositionBorderOfShape(FIntPoint position);
};
