// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Shape.h"
#include "RectangleInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class URectangleInterface : public UShape
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IRectangleInterface : public IShape
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	FIntPoint Size;
	FIntPoint TopLeft;
	FIntPoint TopRight;
	FIntPoint BottomLeft;
	FIntPoint BottomRight;
	int border = 1;

	UFUNCTION(BlueprintNativeEvent)
	void createRectangle(FIntPoint newSize, FIntPoint newTopLeft);

	UFUNCTION(BlueprintNativeEvent)
	void setBorder(int newBorder);

	//Functionality
	UFUNCTION(BlueprintNativeEvent)
	bool isPositionCornerOfRectangle(FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	EModuleDirection getDirectionForPosition(FIntPoint position);
};
