// Copyright Maurice Langer und Jakob Seitz 2020


#include "Rectangle.h"

bool URectangle::operator==(const TScriptInterface<IRectangleInterface> otherRect) const
{
	return otherRect->Size == Size && otherRect->TopLeft == TopLeft;
}
bool URectangle::operator!=(const TScriptInterface<IRectangleInterface> otherRect) const
{
	return !operator==(otherRect);
}

void URectangle::createRectangle_Implementation(FIntPoint newSize, FIntPoint newTopLeft)
{
	Size = newSize;
	TopLeft = newTopLeft;
	TopRight = { newTopLeft.X + newSize.X - 1, newTopLeft.Y };
	BottomLeft = { newTopLeft.X, newTopLeft.Y + newSize.Y - 1 };
	BottomRight = { newTopLeft.X + newSize.X - 1, newTopLeft.Y + newSize.Y - 1 };
}

void URectangle::setBorder_Implementation(int newBorder)
{
	border = newBorder;
}

bool URectangle::isPositionCornerOfRectangle_Implementation(FIntPoint position)
{
	return position == TopLeft || position == TopRight || position == BottomLeft || position == BottomRight;
}

EModuleDirection URectangle::getDirectionForPosition_Implementation(FIntPoint position)
{
	if (!Execute_isPositionInShape(this, position)) return EModuleDirection::None;
	else if (position == TopLeft) return EModuleDirection::NorthWest;
	else if (position == TopRight) return EModuleDirection::NorthEast;
	else if (position == BottomLeft) return EModuleDirection::SouthWest;
	else if (position == BottomRight) return EModuleDirection::SouthEast;
	else if (position.Y == TopLeft.Y) return EModuleDirection::North;
	else if (position.X == BottomRight.X) return EModuleDirection::East;
	else if (position.Y == BottomLeft.Y) return EModuleDirection::South;
	else if (position.X == BottomLeft.X) return EModuleDirection::West;
	return EModuleDirection::None;
}

bool URectangle::isPositionInShape_Implementation(FIntPoint position)
{
	return position.X >= TopLeft.X
		&& position.Y >= TopLeft.Y
		&& position.X <= BottomRight.X
		&& position.Y <= BottomRight.Y;
}

bool URectangle::isPositionBorderOfShape_Implementation(FIntPoint position)
{
	if (!Execute_isPositionInShape(this, position)) return false;
	return position.X <= TopLeft.X + border - 1
		|| position.X >= TopRight.X - (border - 1)
		|| position.Y <= TopLeft.Y + border - 1
		|| position.Y >= BottomLeft.Y - (border - 1);
}
