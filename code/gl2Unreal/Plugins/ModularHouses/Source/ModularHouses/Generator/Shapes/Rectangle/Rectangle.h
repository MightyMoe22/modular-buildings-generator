// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ModularHouses\Modules\ModuleData.h"
#include "RectangleInterface.h"
#include "Rectangle.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URectangle : public UObject, public IRectangleInterface
{
	GENERATED_BODY()
public:
	bool operator == (const TScriptInterface<IRectangleInterface> otherRect) const;
	bool operator != (const TScriptInterface<IRectangleInterface> otherRect) const;

	UFUNCTION(BlueprintNativeEvent)
	void createRectangle(FIntPoint Size, FIntPoint TopLeft);
	virtual void createRectangle_Implementation(FIntPoint Size, FIntPoint TopLeft);

	UFUNCTION(BlueprintNativeEvent)
	void setBorder(int border);
	virtual void setBorder_Implementation(int border);

	//Functionality
	UFUNCTION(BlueprintNativeEvent)
	bool isPositionCornerOfRectangle(FIntPoint position);
	virtual bool isPositionCornerOfRectangle_Implementation(FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	EModuleDirection getDirectionForPosition(FIntPoint position);
	virtual EModuleDirection getDirectionForPosition_Implementation(FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	bool isPositionInShape(FIntPoint position);
	virtual bool isPositionInShape_Implementation(FIntPoint position);

	UFUNCTION(BlueprintNativeEvent)
	bool isPositionBorderOfShape(FIntPoint position);
	virtual bool isPositionBorderOfShape_Implementation(FIntPoint position);
};
