﻿// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "list"
#include "FGridTileStruct.h"

//Room
#include "Room/RoomGenerator.h"

//Shape
#include "Ground/GroundGridGeneratorInterface.h"
#include "Ground/RectangleGroundGenerator.h"

//Roof
#include "Roof/RoofMerger.h"

//Grid
#include "Grid/GroundGrid.h"

//Interface
#include "GroundGeneratorInterface.h"
#include "GroundGenerator.generated.h"

UCLASS()
class MODULARHOUSES_API UGroundGenerator : public UObject, public IGroundGeneratorInterface
{
	GENERATED_BODY()

private:
	UGroundGrid *grid;


	URoomGenerator* roomGenerator;
	IGroundGridGeneratorInterface* groundGenerator;
	URoofMerger* roofMerger;
	TArray<IRoofGeneratorInterface*> roofGenerators;

public:
	/**
	Does NOT create a random GroundGrid!
	* Call a specific Generator e.g. RandomCrossGround to create a GroundGrid
	*/
	UGroundGenerator();
	~UGroundGenerator();

	
	//---------- CrossGround ---------- 

	/*
	* Generates a Random CrossGround
	* @param MinMiddleSize Mindestbreite in X und Y - Richtung des entstehenden Kreuzes
	*/


	UGroundGrid* generateGroundGridForActiveFloor();
	virtual UGroundGrid* generateGroundGridForActiveFloor_Implementation();

	//---------- Allgein GroundGenerator ---------- 

	void update();

	void initialize();
	//For Debug purposes
	void printGeneratedGroundToConsole(); //DEBUGGING

private:
};
