#include "GroundGeneratorData.h"


//FRandomStream UGroundGeneratorData::stream = frandomstream();

void UGroundGeneratorData::randomizeStream()
{
	stream.GenerateNewSeed();
	UE_LOG(LogTemp, Warning, TEXT("(GroundGeneratorData) randomized seed: %i"), stream.GetCurrentSeed())
}

void UGroundGeneratorData::setRandomStream(int seed)
{
	stream = FRandomStream(seed);
}

FRandomStream UGroundGeneratorData::getRandomStream()
{
	return stream;
}
