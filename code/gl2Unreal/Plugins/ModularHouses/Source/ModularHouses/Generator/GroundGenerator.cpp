﻿// Copyright Maurice Langer und Jakob Seitz 2020


#include "GroundGenerator.h"

UGroundGrid* UGroundGenerator::generateGroundGridForActiveFloor_Implementation()
{
	if (groundGeneratorData->activeFloor == 1) {
		initialize();
	}
	update();

	//debug
	//printGeneratedGroundToConsole();
	return grid;
}

void UGroundGenerator::initialize()
{
	//create Objects:
	grid = NewObject<UGroundGrid>();
	roofMerger = NewObject<URoofMerger>();
	roomGenerator = NewObject<URoomGenerator>();
	groundGenerator = NewObject<URectangleGroundGenerator>();
	UGroundData* groundData = NewObject<UGroundData>();
	UGroundPlan* groundPlan = NewObject<UGroundPlan>();

	//initialize
	auto size = groundGeneratorData->Size;
	grid->initialize({ size.X, size.Y });
	roomGenerator->initialize(groundGeneratorData);
	roofMerger->initialize(groundGeneratorData, groundPlan);
	groundData->initialize(groundGeneratorData, groundPlan);


	//generation
	groundGenerator->Execute_GenerateRandomGround(Cast<UObject>(groundGenerator), groundData);					//generateGround

	//set dependencies between groundGenerator and roofGenerator
	TArray<URoofData*> roofDataList = groundGenerator->Execute_GetRoofData(Cast<UObject>(groundGenerator));
	for (auto data : roofDataList)
		data->groundPlan = groundPlan;

	roofMerger->generateFromData(roofDataList);																	//generate roofs
}

void UGroundGenerator::update()
{
	//clear grid
	grid->clearGroundGrid();

	//update
	roofMerger->update();	
	groundGenerator->Execute_updateGrid(Cast<UObject>(groundGenerator));
	roofMerger->addGapsBetweenRoofs();
	TArray<URoomWrapper*> rooms = groundGenerator->Execute_getPrimaryRooms(Cast<UObject>(groundGenerator), roomGenerator);
	roomGenerator->generate(rooms);

	//draw
	roofMerger->Execute_draw(roofMerger, grid);
	groundGenerator->Execute_drawGroundGrid(Cast<UObject>(groundGenerator), grid);
	roomGenerator->Execute_draw(roomGenerator, grid);
}

//ONLY DEBUG PURPOSES
void UGroundGenerator::printGeneratedGroundToConsole()
{
	UE_LOG(LogTemp, Warning, TEXT("Type: "));
	int cellLength = 6;
	for (int y = 0; y < groundGeneratorData->Size.Y; y++)
	{
		FString Line;
		for (int x = 0; x < groundGeneratorData->Size.X; x++)
		{
			int start = 0;
			std::list<FGridTileStruct> *thisList = grid->getGridTilesAtPosition({x, y});
			for (FGridTileStruct gridTile : *thisList)  {
				start++;
				if (gridTile.moduleCategory == EModuleCategory::Wall && gridTile.moduleEnvironment == EModuleEnvironment::Outdoor) {
					     if (gridTile.moduleDirection == EModuleDirection::North)		 Line.Append("=");
					else if (gridTile.moduleDirection == EModuleDirection::South) Line.Append("=");
					else if (gridTile.moduleDirection == EModuleDirection::East)	Line.Append("H");
					else if (gridTile.moduleDirection == EModuleDirection::West)	Line.Append("H");
					else															Line.Append("X");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Wall && gridTile.moduleEnvironment == EModuleEnvironment::Indoor) {
						 if(gridTile.moduleDirection == EModuleDirection::North) Line.Append("-");
					else if(gridTile.moduleDirection == EModuleDirection::South) Line.Append("_");
					else if(gridTile.moduleDirection == EModuleDirection::East)	Line.Append("|");
					else if(gridTile.moduleDirection == EModuleDirection::West)	Line.Append("|");
					else															Line.Append("X");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Edge || gridTile.moduleCategory == EModuleCategory::Pillar) {
						 if (gridTile.moduleDirection == EModuleDirection::NorthEast)	Line.Append("7");
					else if (gridTile.moduleDirection == EModuleDirection::SouthEast)	Line.Append("J");
					else if (gridTile.moduleDirection == EModuleDirection::SouthWest)	Line.Append("L");
					else if (gridTile.moduleDirection == EModuleDirection::NorthWest)	Line.Append("T");
					else																	Line.Append("+");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Window) {
						 if (gridTile.moduleDirection == EModuleDirection::North) Line.Append("/");
					else if (gridTile.moduleDirection == EModuleDirection::South) Line.Append("/");
					else if (gridTile.moduleDirection == EModuleDirection::East)	Line.Append("I");
					else if (gridTile.moduleDirection == EModuleDirection::West)	Line.Append("I");
					else															Line.Append("X");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Door) {
					if (gridTile.moduleDirection == EModuleDirection::North) Line.Append("v");
					else if (gridTile.moduleDirection == EModuleDirection::South) Line.Append("^");
					else if (gridTile.moduleDirection == EModuleDirection::East)	 Line.Append("<");
					else if (gridTile.moduleDirection == EModuleDirection::West)	 Line.Append(">");
					else															 Line.Append("X");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Roof) {
					Line.Append("R");
				}
				else if (gridTile.moduleCategory == EModuleCategory::Floor || gridTile.moduleCategory == EModuleCategory::Ceiling || gridTile.moduleCategory == EModuleCategory::None) {
					start--;
				}
				else {
				Line.Append("?");
				}
			}
			for (int i = start; i < cellLength; i++) {
				Line.Append(" ");
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Line);
	}

	UE_LOG(LogTemp, Warning, TEXT(" "));
	UE_LOG(LogTemp, Warning, TEXT("Direction: "));

	for (int y = 0; y < groundGeneratorData->Size.Y; y++)
	{
		FString Line;
		for (int x = 0; x < groundGeneratorData->Size.X; x++)
		{
			int start = 0;
			std::list<FGridTileStruct>* thisList = grid->getGridTilesAtPosition({ x, y });
			for (FGridTileStruct gridTile : *thisList)  {
				if (thisList->size() == 0) break;
				start++;
					 if (gridTile.moduleDirection == EModuleDirection::North) Line.Append("N");
				else if (gridTile.moduleDirection == EModuleDirection::East) Line.Append("E");
				else if (gridTile.moduleDirection == EModuleDirection::South) Line.Append("S");
				else if (gridTile.moduleDirection == EModuleDirection::West) Line.Append("W");
				else if (gridTile.moduleDirection == EModuleDirection::NorthEast) Line.Append("^");
				else if (gridTile.moduleDirection == EModuleDirection::NorthWest) Line.Append("<");
				else if (gridTile.moduleDirection == EModuleDirection::SouthWest) Line.Append("v");
				else if (gridTile.moduleDirection == EModuleDirection::SouthEast) Line.Append(">");
				else if (gridTile.moduleDirection == EModuleDirection::None) start--;
				else Line.Append("X");
			}
			for (int i = start; i < cellLength; i++) {
				Line.Append(" ");
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Line);
	}
}

UGroundGenerator::UGroundGenerator()
{
	groundGeneratorData = NewObject<UGroundGeneratorData>();
}

UGroundGenerator::~UGroundGenerator()
{
}