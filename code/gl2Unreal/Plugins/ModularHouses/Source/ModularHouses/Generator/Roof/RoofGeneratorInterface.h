// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RoofData.h"
#include "../Components/RoofLine.h"
#include "RoofGeneratorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class URoofGeneratorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IRoofGeneratorInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent)
	void InitializeRoof(URoofData* roofData);

	UFUNCTION(BlueprintNativeEvent)
	URoofData* getRoofData();

	UFUNCTION(BlueprintNativeEvent)
	TArray<TScriptInterface<IComponentInterface>> getComponentsForHeight(int height);

	UFUNCTION(BlueprintNativeEvent)
	void addGapsBetweenRoofs();
};
