// Copyright Maurice Langer und Jakob Seitz 2020


#include "GeneralRoofGenerator.h"


void UGeneralRoofGenerator::InitializeRoof_Implementation(URoofData* newRoofData)
{
	this->roofData = newRoofData;
	calculateRoofHeight();
	createRoof();
}

URoofData* UGeneralRoofGenerator::getRoofData_Implementation()
{
	return roofData;
}

TArray<TScriptInterface<IComponentInterface>> UGeneralRoofGenerator::getComponentsForHeight_Implementation(int activeHeight)
{
	if (activeHeight >= 0 && activeHeight < roofData->height) {
		relativeActiveHeight = activeHeight;
		roofData->startedGenerating = true;
		roofData->groundPlan->mergedRoofRectangles->addRectangle(floors[activeHeight]);
		return floors[activeHeight]->RoofComponents;
	}
	return {};
}

void UGeneralRoofGenerator::addGapsBetweenRoofs_Implementation()
{
	auto groundPlan = roofData->groundPlan;
	auto mergedGroundRectangles = groundPlan->mergedGroundRectangles;
	auto mergedRoofRectangles = groundPlan->mergedRoofRectangles;
	if (relativeActiveHeight >= 0 && relativeActiveHeight < roofData->height) {
		for (auto roofLine : floors[relativeActiveHeight]->CuttableComponents) {
			auto crossedGroundRectangles = mergedGroundRectangles->getHorizontalCrossedRectangles(roofLine);
			crossedGroundRectangles.Append(mergedGroundRectangles->getVerticalCrossedRectangles(roofLine));
			auto crossedRoofRectangles = mergedRoofRectangles->getHorizontalCrossedRectangles(roofLine);
			crossedRoofRectangles.Append(mergedRoofRectangles->getVerticalCrossedRectangles(roofLine));
			roofLine->cuttingHouseRectangles = crossedGroundRectangles;
			roofLine->cuttingRoofs = crossedRoofRectangles;
		}
	}
}