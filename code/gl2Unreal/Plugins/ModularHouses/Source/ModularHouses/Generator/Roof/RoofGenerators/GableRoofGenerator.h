// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../GeneralRoofGenerator.h"
#include "GableRoofGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UGableRoofGenerator : public UGeneralRoofGenerator
{
	GENERATED_BODY()
public:
	TArray<TScriptInterface<IComponentInterface>> getComponentsForHeight_Implementation(int height) override;

private:
	void createRoof() override;
	void calculateRoofHeight() override;
};
