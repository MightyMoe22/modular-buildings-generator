// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Components/RoofLine.h"
#include "../Components/RoofConnectionOut.h"
#include "../Components/RoofConnectionIn.h"
#include "../Components/RoofGutter.h"
#include "../Components/RoofWall.h"
#include "../Components/FlatRoof.h"
#include "RoofFloor.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofFloor : public URectangle
{
	GENERATED_BODY()
	
public:
	TArray<TScriptInterface<IComponentInterface>> RoofComponents;
	TArray<UCuttableComponent*> CuttableComponents;
};
