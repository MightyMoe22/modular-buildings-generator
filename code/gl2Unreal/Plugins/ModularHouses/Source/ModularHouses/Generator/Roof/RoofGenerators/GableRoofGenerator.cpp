// Copyright Maurice Langer und Jakob Seitz 2020


#include "GableRoofGenerator.h"


TArray<TScriptInterface<IComponentInterface>> UGableRoofGenerator::getComponentsForHeight_Implementation(int activeHeight) {
	if (activeHeight == 0) {
		this->relativeActiveHeight = activeHeight;
		return floors[activeHeight]->RoofComponents;
	}
	return Super::getComponentsForHeight_Implementation(activeHeight);
}

void UGableRoofGenerator::createRoof()
{
	floors.SetNum(roofData->height);
	for (int i = 0; i < roofData->height; i++)
	{
		 URoofFloor* floor = NewObject<URoofFloor>();
		 URoofLine* leftLine = NewObject<URoofLine>();
		 URoofLine* rightLine = NewObject<URoofLine>();
		 URoofWall* leftWall = NewObject<URoofWall>();
		 URoofWall* rightWall = NewObject<URoofWall>();
		 URoofConnectionOut* leftFillerOut = NewObject<URoofConnectionOut>();
		 URoofConnectionIn* leftFillerIn = NewObject<URoofConnectionIn>();
		 URoofConnectionOut* rightFillerOut = NewObject<URoofConnectionOut>();
		 URoofConnectionIn* rightFillerIn = NewObject<URoofConnectionIn>();
		 URoofGutter* leftRoofGutter = NewObject<URoofGutter>();
		 URoofGutter* rightRoofGutter = NewObject<URoofGutter>();

		 //horizontal roof
		 if (roofData->direction == Direction::D_North || roofData->direction == Direction::D_South) {
			 FIntPoint size = { roofData->Size.X - 2 * i, roofData->Size.Y };
			 FIntPoint topLeft = roofData->TopLeft + FIntPoint(i, 0);
			 floor->createRectangle(size, topLeft);

			 if (i == 0) {
				 leftRoofGutter->initGutter(floor->Size.Y, floor->TopLeft, EModuleDirection::East);
				 rightRoofGutter->initGutter(floor->Size.Y, floor->TopRight, EModuleDirection::West);
			 }
			 else {
				 leftLine->initRoof(floor->Size.Y, floor->TopLeft, EModuleDirection::East);
				 leftFillerOut->initRoofConnection(floor->Size.Y, floor->TopLeft - FIntPoint(1, 0), EModuleDirection::West);
				 rightLine->initRoof(floor->Size.Y, floor->TopRight, EModuleDirection::West);
				 rightFillerOut->initRoofConnection(floor->Size.Y, floor->TopRight + FIntPoint(1, 0), EModuleDirection::East);
				 leftWall->InitWall(floor->Size.X, floor->TopLeft, EModuleDirection::North); //TopWall
				 leftWall->setLeftAndRightPoint(leftWall->TopRight, leftWall->TopLeft);

				 rightWall->InitWall(floor->Size.X, floor->BottomLeft - FIntPoint(0, 1), EModuleDirection::South); //BottomWall
				 rightWall->setLeftAndRightPoint(rightWall->BottomLeft, rightWall->BottomRight);
			 }
			 leftFillerIn->initRoofConnection(floor->Size.Y, floor->TopLeft + FIntPoint(1, 0), EModuleDirection::East);
			 rightFillerIn->initRoofConnection(floor->Size.Y, floor->TopRight - FIntPoint(1, 0), EModuleDirection::West);
		 }

		 //vertical roof
		 else if (roofData->direction == Direction::D_East || roofData->direction == Direction::D_West) {
			 FIntPoint size = { roofData->Size.X, roofData->Size.Y - 2 * i };
			 FIntPoint topLeft = roofData->TopLeft + FIntPoint(0, i);
			 floor->createRectangle(size, topLeft);

			 if (i == 0) {
				 leftRoofGutter->initGutter(floor->Size.X, floor->TopLeft, EModuleDirection::South);
				 rightRoofGutter->initGutter(floor->Size.X, floor->BottomLeft, EModuleDirection::North);
			 }
			 else {
				 leftLine->initRoof(floor->Size.X, floor->TopLeft, EModuleDirection::South);
				 leftFillerOut->initRoofConnection(floor->Size.X, floor->TopLeft - FIntPoint(0, 1), EModuleDirection::North);
				 rightLine->initRoof(floor->Size.X, floor->BottomLeft, EModuleDirection::North);
				 rightFillerOut->initRoofConnection(floor->Size.X, floor->BottomLeft + FIntPoint(0, 1), EModuleDirection::South);
				 leftWall->InitWall(floor->Size.Y, floor->TopLeft, EModuleDirection::West); //LeftWall
				 leftWall->setLeftAndRightPoint(leftWall->TopLeft, leftWall->BottomLeft);
				 rightWall->InitWall(floor->Size.Y, floor->TopRight - FIntPoint(1, 0), EModuleDirection::East); //RightWall
				 rightWall->setLeftAndRightPoint(rightWall->BottomRight, rightWall->TopRight);
			 }
			 leftFillerIn->initRoofConnection(floor->Size.X, floor->TopLeft + FIntPoint(0, 1), EModuleDirection::South);
			 rightFillerIn->initRoofConnection(floor->Size.X, floor->BottomLeft - FIntPoint(0, 1), EModuleDirection::North);
		 }

		 if (i == 0) {
			 floor->RoofComponents.Add(leftRoofGutter);
			 floor->CuttableComponents.Add(leftRoofGutter);
			 floor->RoofComponents.Add(rightRoofGutter);
			 floor->CuttableComponents.Add(rightRoofGutter);
		 }
		 else {
			 if (i < roofData->height-1) {
				 floor->RoofComponents.Add(leftLine);
				 floor->CuttableComponents.Add(leftLine);
				 floor->RoofComponents.Add(rightLine);
				 floor->CuttableComponents.Add(rightLine);
				 floor->RoofComponents.Add(leftWall);
				 floor->RoofComponents.Add(rightWall);
			 }
			 floor->RoofComponents.Add(leftFillerOut);
			 floor->CuttableComponents.Add(leftFillerOut);
			 floor->RoofComponents.Add(rightFillerOut);
			 floor->CuttableComponents.Add(rightFillerOut);
		 }
		 if (i < roofData->height - 2) {
			 floor->RoofComponents.Add(leftFillerIn);
			 floor->CuttableComponents.Add(leftFillerIn);
			 floor->RoofComponents.Add(rightFillerIn);
			 floor->CuttableComponents.Add(rightFillerIn);
		 }
		 floors[i] = floor;
	}
}

void UGableRoofGenerator::calculateRoofHeight()
{
	int heightFactor = (roofData->angle) * 4.0 / PI;
	if (roofData->direction == Direction::D_North || roofData->direction == Direction::D_South) roofData->height = (roofData->Size.X / 2) * heightFactor+1;
	else if (roofData->direction == Direction::D_East || roofData->direction == Direction::D_West) roofData->height = (roofData->Size.Y / 2) * heightFactor+1;
}
