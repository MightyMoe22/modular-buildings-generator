// Copyright Maurice Langer und Jakob Seitz 2020


#include "FlatRoofGenerator.h"


void UFlatRoofGenerator::createRoof()
{
	URoofFloor* floor = NewObject<URoofFloor>(this);

	//horizontal roof
	FIntPoint size = { roofData->Size.X, roofData->Size.Y };
	FIntPoint topLeft = roofData->TopLeft;
	UFlatRoof* flatRoofComponent = NewObject<UFlatRoof>(this);
	flatRoofComponent->initComponent(size, topLeft);
	floor->RoofComponents.Add(flatRoofComponent);
	floor->CuttableComponents.Add(flatRoofComponent);
	floor->createRectangle({ 0,0 }, topLeft);
	floors.Add(floor);
}

void UFlatRoofGenerator::calculateRoofHeight()
{
	roofData->height = 1;
}