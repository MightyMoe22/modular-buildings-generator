// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../GeneralRoofGenerator.h"
#include "FlatRoofGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UFlatRoofGenerator : public UGeneralRoofGenerator
{
	GENERATED_BODY()

private:
	void createRoof() override;
	void calculateRoofHeight() override;
};
