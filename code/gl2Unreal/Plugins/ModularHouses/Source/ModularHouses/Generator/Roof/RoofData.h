// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Direction.h"
#include "../Shapes/Rectangle/Rectangle.h"
#include "../GroundPlan/GroundPlan.h"
#include "RoofData.generated.h"

/**
 * 
 */

UCLASS()
class MODULARHOUSES_API URoofData : public URectangle
{
	GENERATED_BODY()

public:
	bool operator == (const URoofData& otherRoof) const { return roofID == otherRoof.roofID; }
	bool operator != (const URoofData& otherRoof) const { return !operator==(otherRoof); }

	int roofID;

	int startFloor;
	int activeFloor = 0;
	int height;
	UGroundPlan* groundPlan;
	float angle = PI / 4;
	Direction direction;
	TScriptInterface<IRectangleInterface> owningRectangle;
	bool startedGenerating = false;

public:

	void Initialize_RoofData(int roofID, FIntPoint topLeft, FIntPoint Size, Direction direction, TScriptInterface<IRectangleInterface> owningRectangle);

	void printData() {
		UE_LOG(LogTemp, Warning, TEXT("roomID: %i"), roofID);
		UE_LOG(LogTemp, Warning, TEXT("obenLinks: %i %i"), TopLeft.X, TopLeft.Y);
		UE_LOG(LogTemp, Warning, TEXT("ObenRechts: %i %i"), TopRight.X, TopRight.Y);
		UE_LOG(LogTemp, Warning, TEXT("UntenLinks: %i %i"), BottomLeft.X, BottomLeft.Y);
		UE_LOG(LogTemp, Warning, TEXT("UntenRechts: %i %i"), BottomRight.X, BottomRight.Y);
		UE_LOG(LogTemp, Warning, TEXT("Size: %i %i"), Size.X, Size.Y);
	}

};
