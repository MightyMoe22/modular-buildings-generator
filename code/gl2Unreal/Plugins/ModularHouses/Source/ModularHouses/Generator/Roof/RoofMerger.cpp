// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofMerger.h"

void URoofMerger::initialize(UGroundGeneratorData* newGroundGeneratorData, UGroundPlan* newGroundPlan)
{
	this->groundGeneratorData = newGroundGeneratorData;
	this->minRoofStartFloor = groundGeneratorData->minRoofStart;
	this->maxRoofStartFloor = groundGeneratorData->maxRoofStart;
	this->groundPlan = newGroundPlan;
}

void URoofMerger::generateFromData(TArray<URoofData*> roofDataList)
{
	roofGenerators.Empty();
	auto stream = groundGeneratorData->getRandomStream();
	for (URoofData* roofData : roofDataList) {
		IRoofGeneratorInterface* roofGenerator;
		int generate; // 0 = gableRoof, 1 = flatRoof

		if (groundGeneratorData->generateGableRoofs && groundGeneratorData->generateFlatRoofs)
			generate = stream.RandRange(0, 1);
		else if (groundGeneratorData->generateFlatRoofs)
			generate = 1;
		else generate = 0;

		if (generate == 1)
			roofGenerator = NewObject<UFlatRoofGenerator>();
		else
			roofGenerator = NewObject<UGableRoofGenerator>();
		roofGenerator->Execute_InitializeRoof(Cast<UObject>(roofGenerator), roofData);

		roofGenerators.Add(roofGenerator);

		int height = roofData->height;
		roofData->startFloor = stream.RandRange(minRoofStartFloor+1, maxRoofStartFloor+1);
	}

	roofDataList.Empty();
}

void URoofMerger::update()
{
	int floor = groundGeneratorData->activeFloor;
	groundPlan->mergedRoofRectangles->clearRectangles();
	components.Empty();
	for (IRoofGeneratorInterface* roofGenerator : roofGenerators) {
		auto roofData = roofGenerator->Execute_getRoofData(Cast<UObject>(roofGenerator));
		int height = roofData->height;
		int startFloor = roofData->startFloor;
		if (floor >= startFloor) {
			TArray<TScriptInterface<IComponentInterface>> localComponents = roofGenerator->Execute_getComponentsForHeight(Cast<UObject>(roofGenerator), roofData->activeFloor);
			for (auto component : localComponents)
				components.Add(component);
			roofData->activeFloor++;
		}
	}
}

void URoofMerger::addGapsBetweenRoofs()
{
	for (auto roofGenerator : roofGenerators) {
		roofGenerator->Execute_addGapsBetweenRoofs(Cast<UObject>(roofGenerator));
	}
}

void URoofMerger::draw_Implementation(UGroundGrid* groundGrid)
{
	for (TScriptInterface<IComponentInterface> component : components)
		component->Execute_printComponentOnGrid(component.GetObject(), groundGrid);
}
