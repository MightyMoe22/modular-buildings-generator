// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Shapes/Rectangle/RectangleMerger/RectangleMerger.h"
#include "RoofData.h"
#include "RoofGeneratorInterface.h"
#include "RoofGenerators/GableRoofGenerator.h"
#include "RoofGenerators/FlatRoofGenerator.h"
#include "../GroundGeneratorData.h"
#include "Generator/SubGenerator/SubGeneratorInterface.h"
#include "RoofMerger.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoofMerger : public UObject, public ISubGeneratorInterface
{
    GENERATED_BODY()
private:
    UGroundPlan* groundPlan;
    TArray<TScriptInterface<IComponentInterface>> components;
public:
    int minRoofStartFloor;
    int maxRoofStartFloor;
    URectangleMerger* rectangleMerger;
    TArray<IRoofGeneratorInterface*> roofGenerators;
    UGroundGeneratorData* groundGeneratorData;
    
    void initialize(UGroundGeneratorData* groundGeneratorData, UGroundPlan* groundPlan); //initialize
    void generateFromData(TArray<URoofData*> roofData); //generate
    void update();    //update
    void addGapsBetweenRoofs();

    //UFUNCTION(BlueprintNativeEvent)
    //void initialize();
    //virtual void initialize_Implementation() {};

    UFUNCTION(BlueprintNativeEvent)
    void generate();
    virtual void generate_Implementation() {}; //TODO

    UFUNCTION(BlueprintNativeEvent)
    void draw(UGroundGrid* groundGrid);
    virtual void draw_Implementation(UGroundGrid* groundGrid);
};
