// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RoofGeneratorInterface.h"
#include "RoofFloor.h"
#include "GeneralRoofGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UGeneralRoofGenerator : public UObject, public IRoofGeneratorInterface
{
	GENERATED_BODY()
protected:
	URoofData* roofData;
	int relativeActiveHeight = -1;
	TArray<URoofFloor*> floors;

public:

	UFUNCTION(BlueprintNativeEvent)
		void InitializeRoof(URoofData* roofData);
	virtual void InitializeRoof_Implementation(URoofData* roofData);


	UFUNCTION(BlueprintNativeEvent)
		URoofData* getRoofData();
	virtual URoofData* getRoofData_Implementation();


	UFUNCTION(BlueprintNativeEvent)
		TArray<TScriptInterface<IComponentInterface>> getComponentsForHeight(int height);
	virtual TArray<TScriptInterface<IComponentInterface>> getComponentsForHeight_Implementation(int height);

	UFUNCTION(BlueprintNativeEvent)
		void addGapsBetweenRoofs();
	virtual void addGapsBetweenRoofs_Implementation();

private:
	virtual void createRoof() {};
	virtual void calculateRoofHeight() {};
};
