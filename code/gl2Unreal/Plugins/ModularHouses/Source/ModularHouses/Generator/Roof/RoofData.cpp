// Copyright Maurice Langer und Jakob Seitz 2020


#include "RoofData.h"

void URoofData::Initialize_RoofData(int newRoofID, FIntPoint topLeft, FIntPoint newSize, Direction newDirection, TScriptInterface<IRectangleInterface> newOwningRectangle)
{
	this->owningRectangle = newOwningRectangle;
	this->direction = newDirection;
	this->roofID = newRoofID;
	Execute_createRectangle(this, newSize, topLeft);
}
