#include "FGridTileStruct.h"

bool FGridTileStruct::operator==(const FGridTileStruct& otherGridTile) const
{
	return moduleDirection == otherGridTile.moduleDirection
		&& moduleFacing == otherGridTile.moduleFacing
		&& moduleEnvironment == otherGridTile.moduleEnvironment
		&& moduleCategory == otherGridTile.moduleCategory;
}

bool FGridTileStruct::operator!=(const FGridTileStruct& otherGridTile) const
{
	return !operator==(otherGridTile);
}

FGridTileStruct::FGridTileStruct(EModuleCategory category, EModuleDirection direction, EModuleEnvironment environment, EModuleFacing facing)
{
	this->moduleCategory = category;
	this->moduleDirection = direction;
	this->moduleEnvironment = environment;
	this->moduleFacing = facing;
}
