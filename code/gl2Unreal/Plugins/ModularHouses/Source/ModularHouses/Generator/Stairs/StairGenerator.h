// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Generator/FloorManager.h"
#include "Generator/GroundGeneratorData.h"
#include "StairGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UStairGenerator : public UObject
{
	GENERATED_BODY()

	UGroundGeneratorData* generatorData;
	UFloorManager* floorManager;
public:
	void draw(UFloorManager* newFloorManager, UGroundGeneratorData* newGroundGeneratorData);
	
private:
	TArray<FIntPoint> findPositionsForStairs(int floor);
	bool isStairPlaceable(FIntVector position);
	void drawStairAtPosition(FIntVector position);
};
