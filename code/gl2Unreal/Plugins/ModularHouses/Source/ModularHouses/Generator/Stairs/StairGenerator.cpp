// Copyright Maurice Langer und Jakob Seitz 2020


#include "StairGenerator.h"

void UStairGenerator::draw(UFloorManager* newFloorManager, UGroundGeneratorData* newGroundGeneratorData)
{
	generatorData = newGroundGeneratorData;
	floorManager = newFloorManager;

	auto stream = generatorData->getRandomStream();
	for (int z = 0; z < floorManager->Size.Z; z++) {
		auto goodPositions = findPositionsForStairs(z);
		int numberOfStairs = stream.FRandRange(generatorData->minStairs, generatorData->maxStairs);

		for (int i = 0; i < numberOfStairs; i++)
		{
			if (goodPositions.Num() == 0) return;
			int randIndex = stream.FRandRange(0, goodPositions.Num() - 1);
			FIntPoint randPos = goodPositions[randIndex];
			drawStairAtPosition({ randPos.X, randPos.Y, z });
			goodPositions.Remove(randPos);
		}
	}
}

TArray<FIntPoint> UStairGenerator::findPositionsForStairs(int floor)
{
	auto positions = TArray<FIntPoint>();

	int X = floorManager->Size.X;
	int Y = floorManager->Size.Y;
	for (int x = 0; x < X; x++)
		for (int y = 0; y < Y; y++)
			if (isStairPlaceable({ x,y,floor }))
				positions.Add({ x,y });

	return positions;
}


//Rules for Stairs:
//^^^^^^^^^^^^^^^^^
//
//On Stairs :
//-Only exactly one Wall / Window(left)
//- floor(= > no Stairs in top of Stairs)
//
//over Stairs :
//Only exactly one Wall / Window(in same direction)
//(no shelf)
//
//No other stairs in neighbor-Tiles (could barrikade floor)



bool UStairGenerator::isStairPlaceable(FIntVector position)
{
	if (position.Z == floorManager->Size.Z) return false;
	auto gridTiles = floorManager->getGridTileAtPosition(position);
	auto gridTilesAbove = floorManager->getGridTileAtPosition(position + FIntVector(0,0,1)); //TODO

	bool containsFloor = false;
	bool containsExactlyOneWall = false;
	EModuleDirection wallDirection = EModuleDirection::None;
	for (auto gridTile : *gridTiles) {
		if (gridTile.moduleCategory == EModuleCategory::Floor)
			containsFloor = true;
		if (gridTile.moduleCategory == EModuleCategory::Wall || gridTile.moduleCategory == EModuleCategory::Window) {
			if (containsExactlyOneWall == false) {
				containsExactlyOneWall = true;
				wallDirection = gridTile.moduleDirection;
			}
			else
				return false;
		}
		if (gridTile.moduleCategory == EModuleCategory::Door) return false;
	}

	bool aboveContainsExactlyOneWall = false;
	for (auto gridTile : *gridTilesAbove) {
		if (gridTile.moduleCategory == EModuleCategory::Wall || gridTile.moduleCategory == EModuleCategory::Window) {
			if (aboveContainsExactlyOneWall == false)
				aboveContainsExactlyOneWall = true;
			else
				return false;
			if (gridTile.moduleDirection != wallDirection)
				return false;
		}
		if (gridTile.moduleCategory == EModuleCategory::Door) return false;
	}
	return containsFloor && containsExactlyOneWall && aboveContainsExactlyOneWall ;
}

void UStairGenerator::drawStairAtPosition(FIntVector position)
{
	FGridTileStruct stairs;
	FGridTileStruct wall;
	FGridTileStruct ceiling;

	auto gridTiles = floorManager->getGridTileAtPosition(position);
	auto gridTilesAbove = floorManager->getGridTileAtPosition(position + FIntVector(0,0,1));
	for (auto gridTile : *gridTiles) {
		if (gridTile.moduleCategory == EModuleCategory::Wall || gridTile.moduleCategory == EModuleCategory::Window) {
			stairs = gridTile;
			stairs.moduleCategory = EModuleCategory::Stairs;
			wall = gridTile;
		}
		if (gridTile.moduleCategory == EModuleCategory::Ceiling)
			ceiling = gridTile;
	}
	gridTiles->remove(wall);
	gridTiles->remove(ceiling);
	floorManager->addGridTileAtPosition(stairs, position);

	FGridTileStruct floor;
	for (auto gridTile : *gridTilesAbove) {
		if (gridTile.moduleCategory == EModuleCategory::Floor) {
			floor = gridTile;
		}
	}
	gridTilesAbove->remove(floor);
}
