// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "ModularHouses\Modules\ModuleData.h"
#include "FGridTileStruct.generated.h"

USTRUCT()
struct MODULARHOUSES_API FGridTileStruct
{
	GENERATED_BODY()

	bool operator == (const FGridTileStruct& otherGridTile) const;
	bool operator != (const FGridTileStruct& otherGridTile) const;

	FGridTileStruct() {}
	FGridTileStruct(EModuleCategory category, EModuleDirection direction, EModuleEnvironment environment, EModuleFacing facing);

	EModuleDirection moduleDirection = EModuleDirection::None;
	EModuleFacing moduleFacing = EModuleFacing::None;
	EModuleEnvironment moduleEnvironment = EModuleEnvironment::None;
	EModuleCategory moduleCategory = EModuleCategory::None;

	EModuleDirection flippedAtDirection = EModuleDirection::None;
};

FORCEINLINE uint32 GetTypeHash(const FGridTileStruct& gts)
{
	return FCrc::MemCrc_DEPRECATED(&gts, sizeof(FGridTileStruct));
}
