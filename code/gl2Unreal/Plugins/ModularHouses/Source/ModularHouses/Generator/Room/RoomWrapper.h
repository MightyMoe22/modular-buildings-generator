// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Room.h"
#include "RoomWrapper.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URoomWrapper : public UObject
{
	GENERATED_BODY()
public:
    FRoom room;
};
