// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "RoomGenerator.h"


void URoomGenerator::initialize(UGroundGeneratorData* newGroundGeneratorData)
{
	this->groundGeneratorData = newGroundGeneratorData;

	//if it should be possible to change roomSize while step-by-step generation move the following code in the generate-function
	int minRoomSize = newGroundGeneratorData->minRoomSize;
	MinRoomSize = { minRoomSize, minRoomSize };
	MinRoomArea = minRoomSize ^ 2;
}

void URoomGenerator::generate(TArray<URoomWrapper*> RoomsTodoQueue) {
	auto activeFloor = groundGeneratorData->activeFloor;

	FinalRooms.clear();

	int badRooms = 0;

	bool SeperateX;
	auto stream = groundGeneratorData->getRandomStream();
	while (RoomsTodoQueue.IsValidIndex(0))
	{
		FRoom room = RoomsTodoQueue.Pop()->room;
		//RoomsTodoQueue->remove(room);

		if (!room.isSeperable(MinRoomSize, MinRoomArea)) {
			if (!isValidRoom(&room)) badRooms++;
			FinalRooms.push_front(room);
		}
		else
		{
			if (room.isXSeperable(MinRoomSize, MinRoomArea) && room.isYSeperable(MinRoomSize, MinRoomArea)) {
				SeperateX = room.Size.X > room.Size.Y;
			}
			else if (room.isXSeperable(MinRoomSize, MinRoomArea))
				SeperateX = true;
			else SeperateX = false;

			if (SeperateX) {
				int leftSeperateBorder = FMath::Max(MinRoomSize.X, MinRoomArea / room.Size.Y);
				int rightSeperateBorder = room.Size.X - FMath::Max(MinRoomSize.X, MinRoomArea / room.Size.Y);

				int seperatePoint = room.Origin.X + (int)stream.RandRange(leftSeperateBorder, rightSeperateBorder);
				FRoom newLeftRoom = generateRoom({ room.Origin.X, room.Origin.Y }, { seperatePoint - room.Origin.X + 1, room.Size.Y });
				FRoom newRightRoom = generateRoom({ (seperatePoint + 1), room.Origin.Y }, { room.Origin.X + room.Size.X - (seperatePoint + 1), room.Size.Y });

				URoomWrapper* LeftRoomWrapper = NewObject<URoomWrapper>(this);
				LeftRoomWrapper->room = newLeftRoom;

				URoomWrapper* RightRoomWrapper = NewObject<URoomWrapper>(this);
				RightRoomWrapper->room = newRightRoom;
				RoomsTodoQueue.Push(LeftRoomWrapper);
				RoomsTodoQueue.Push(RightRoomWrapper);

			}
			else {
				int seperatePoint = room.Origin.Y + (int)stream.RandRange(MinRoomSize.Y, room.Size.Y - MinRoomSize.Y);
				FRoom newTopRoom = generateRoom({ room.Origin.X, room.Origin.Y }, { room.Size.X, seperatePoint - room.Origin.Y + 1 });
				FRoom newBottomRoom = generateRoom({ room.Origin.X, (seperatePoint + 1) }, { room.Size.X, room.Origin.Y + room.Size.Y - (seperatePoint + 1) });

				URoomWrapper* TopRoomWrapper = NewObject<URoomWrapper>(this);
				TopRoomWrapper->room = newTopRoom;

				URoomWrapper* BottomRoomWrapper = NewObject<URoomWrapper>(this);
				BottomRoomWrapper->room = newBottomRoom;

				RoomsTodoQueue.Push(TopRoomWrapper);
				RoomsTodoQueue.Push(BottomRoomWrapper);
			}
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("BadRooms: %i"), badRooms);
	//DEBUG:
}


FRoom URoomGenerator::generateRoom(FIntPoint Origin, FIntPoint Size)
{
	FRoom room = FRoom();
	room.InitializeRoom(Origin, Size, getUniqueRoomID());
	return room;
}


void URoomGenerator::fixAdjacencyLists(FRoom* roomToFix)
{
	for (std::list<FRoom> adjacencyList : { roomToFix->adjacencyListTop, roomToFix->adjacencyListBottom, roomToFix->adjacencyListLeft, roomToFix->adjacencyListRight }) {
		adjacencyList.clear();
	}
	for (FRoom testRoom : FinalRooms) {
		if (*roomToFix == testRoom) continue;

		//Oberhalb
		bool XNichtOberhalb = testRoom.UntenRechts.X < roomToFix->ObenLinks.X || testRoom.UntenLinks.X > roomToFix->ObenRechts.X;
		bool YOberhalb = roomToFix->ObenLinks.Y-1 == testRoom.UntenLinks.Y;
		bool Oberhalb = !XNichtOberhalb && YOberhalb;
		if (Oberhalb) {
			roomToFix->adjacencyListTop.push_front(testRoom);
			testRoom.adjacencyListBottom.remove(*roomToFix);
			testRoom.adjacencyListBottom.push_front(*roomToFix);
			/*DEBUG:
			UE_LOG(LogTemp, Warning, TEXT(" "));
			testRoom.printRoom();
			UE_LOG(LogTemp, Warning, TEXT("is in top of "));
			roomToFix->printRoom();*/
		}

		//Unterhalb
		bool XNichtUnterhalb = XNichtOberhalb;
		bool YUnterhalb = roomToFix->UntenLinks.Y+1 == testRoom.ObenLinks.Y;
		bool Unterhalb = !XNichtUnterhalb && YUnterhalb;
		if (Unterhalb) {
			roomToFix->adjacencyListBottom.push_front(testRoom);
			testRoom.adjacencyListTop.remove(*roomToFix);
			testRoom.adjacencyListTop.push_front(*roomToFix);
			/*DEBUG:
			UE_LOG(LogTemp, Warning, TEXT(" "));
			testRoom.printRoom();
			UE_LOG(LogTemp, Warning, TEXT("is below "));
			roomToFix->printRoom();*/
		}

		//Links
		bool XLinks = roomToFix->ObenLinks.X-1 == testRoom.ObenRechts.X;
		bool YNichtLinks = testRoom.UntenRechts.Y < roomToFix->ObenLinks.Y || testRoom.ObenRechts.Y > roomToFix->UntenLinks.Y;
		bool Links = XLinks && !YNichtLinks;
		if (Links) {
			roomToFix->adjacencyListLeft.push_front(testRoom);
			testRoom.adjacencyListRight.remove(*roomToFix);
			testRoom.adjacencyListRight.push_front(*roomToFix);
			/*DEBUG:
			UE_LOG(LogTemp, Warning, TEXT(" "));
			testRoom.printRoom();
			UE_LOG(LogTemp, Warning, TEXT("is left of "));
			roomToFix->printRoom();*/
		}

		//Rechts
		bool XRechts = roomToFix->ObenRechts.X+1 == testRoom.ObenLinks.X;
		bool YNichtRechts = YNichtLinks;
		bool Rechts = XRechts && !YNichtRechts;
		if (Rechts) {
			roomToFix->adjacencyListRight.push_front(testRoom);
			testRoom.adjacencyListLeft.remove(*roomToFix);
			testRoom.adjacencyListLeft.push_front(*roomToFix);
			/*DEBUG:
			UE_LOG(LogTemp, Warning, TEXT(" "));
			testRoom.printRoom();
			UE_LOG(LogTemp, Warning, TEXT("is right of "));
			roomToFix->printRoom();*/
		}
	}
}

bool URoomGenerator::isValidRoom(FRoom* room)
{
	return room->isValid(MinRoomSize, MinRoomArea);
}

void URoomGenerator::GenerateDoorsBetweenRooms(UGroundGrid* grid)
{
	int l;
	int r;
	int m;
	bool isSpaceForADoor;
	for (FRoom room : FinalRooms) {
		fixAdjacencyLists(&room);
		int spaceForDoor = 0;

		//Oben
		for (FRoom adjRoom : room.adjacencyListTop) {
			l = FMath::Max(room.ObenLinks.X, adjRoom.ObenLinks.X);
			r = FMath::Min(room.ObenRechts.X, adjRoom.ObenRechts.X);
			m = (l + r) / 2;
			isSpaceForADoor = r - l >= spaceForDoor;
			if (isSpaceForADoor) {
				int x = m;
				int y = room.ObenLinks.Y;
				replaceAllWallsWithDoors({ x, y }, grid, EModuleDirection::North);
			}
		}

		//Links
		for (FRoom adjRoom : room.adjacencyListLeft) {
			l = FMath::Max(room.ObenLinks.Y, adjRoom.ObenLinks.Y);
			r = FMath::Min(room.UntenLinks.Y, adjRoom.UntenLinks.Y);
			m = (l + r) / 2;
			isSpaceForADoor = r - l >= spaceForDoor;
			if (isSpaceForADoor) {
				int x = room.ObenLinks.X;
				int y = m;
				replaceAllWallsWithDoors({ x, y }, grid, EModuleDirection::West);
			}
		}

		//Rechts
		for (FRoom adjRoom : room.adjacencyListRight) {
			l = FMath::Max(room.ObenLinks.Y, adjRoom.ObenLinks.Y);
			r = FMath::Min(room.UntenLinks.Y, adjRoom.UntenLinks.Y);
			m = (l + r) / 2;
			isSpaceForADoor = r - l >= spaceForDoor;
			if (isSpaceForADoor) {
				int x = room.ObenRechts.X;
				int y = m;
				replaceAllWallsWithDoors({ x, y }, grid, EModuleDirection::East);
			}
		}

		//Unten
		for (FRoom adjRoom : room.adjacencyListBottom) {
			l = FMath::Max(room.ObenLinks.X, adjRoom.ObenLinks.X);
			r = FMath::Min(room.ObenRechts.X, adjRoom.ObenRechts.X);
			m = (l + r) / 2;
			isSpaceForADoor = r - l >= spaceForDoor;
			if (isSpaceForADoor) {
				int x = m;
				int y = room.UntenLinks.Y;
				replaceAllWallsWithDoors({ x, y }, grid, EModuleDirection::South);
			}
		}
	}
}

void URoomGenerator::draw_Implementation(UGroundGrid* groundGrid)
{
	for (auto room : FinalRooms) {
		room.printRoomOnGroundGrid(groundGrid);
	}
	GenerateDoorsBetweenRooms(groundGrid);
}

void URoomGenerator::replaceAllWallsWithDoors(FIntPoint position, UGroundGrid* grid, EModuleDirection direction)
{
	std::list<FGridTileStruct> removeList;
	for (FGridTileStruct gridTile : *grid->getGridTilesAtPosition(position))
	{
		if (gridTile.moduleCategory == EModuleCategory::Wall && gridTile.moduleDirection == direction) {
			grid->addGridTileAtPosition((FGridTileStruct(EModuleCategory::Door, gridTile.moduleDirection, gridTile.moduleEnvironment, gridTile.moduleFacing)), position);
			removeList.push_front(gridTile);
		}
	}
	for (FGridTileStruct gridTile : removeList) {
		grid->getGridTilesAtPosition(position)->remove(gridTile);
	}
	removeList.clear();
}


int URoomGenerator::getUniqueRoomID()
{
	return RoomIDCounter++;
}
