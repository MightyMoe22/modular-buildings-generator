// Copyright Maurice Langer und Jakob Seitz 2020


#include "Room.h"

void FRoom::InitializeRoom(FIntPoint newOrigin, FIntPoint newSize, int newRoomID)
{
	this->Origin = newOrigin;
	this->roomID = newRoomID;
	this->Size = newSize;
	Area = Size.X * Size.Y;

	UWall* topWall = NewObject<UWall>();
	topWall->InitWall(Size.X, Origin, EModuleDirection::North);
	UWall* bottomWall = NewObject<UWall>();
	bottomWall->InitWall(Size.X, Origin + FIntPoint(0, Size.Y - 1), EModuleDirection::South);
	UWall* leftWall = NewObject<UWall>();
	leftWall->InitWall(Size.Y, Origin, EModuleDirection::West);
	UWall* rightWall = NewObject<UWall>();
	rightWall->InitWall(Size.Y, Origin + FIntPoint(Size.X - 1, 0), EModuleDirection::East);
	walls = { topWall, leftWall, bottomWall, rightWall };
	
	

	generateAllPoints();
	if (Origin.X < 0 || Origin.Y < 0 || Size.X < 0 || Size.Y < 0) {
		UE_LOG(LogTemp, Error, TEXT("To small input in Room-Constructor!"));
		printRoom();
	}

}

void FRoom::generateAllPoints()
{
	ObenLinks = Origin;
	ObenRechts = { Origin.X + Size.X - 1, Origin.Y };
	UntenLinks = { Origin.X, Origin.Y + Size.Y - 1 };
	UntenRechts = { Origin.X + Size.X - 1, Origin.Y + Size.Y - 1 };
}

bool FRoom::isValid(FIntPoint MinSize, int MinArea)
{
	return Size.X >= MinSize.X && Size.Y >= MinSize.Y && isVolumeValid(MinArea);
}

inline bool FRoom::isVolumeValid(int MinArea) {
	return Area >= MinArea;
}

inline bool FRoom::isSeperable(FIntPoint MinSize, int MinArea) {
	if (!isValid(MinSize, MinArea)) return false; //Ist bereits nicht Valid
	if (Area < MinArea * 2) return false; //Volume reicht nicht aus
	if (Size.X < MinSize.X * 2 && Size.Y < MinSize.Y * 2) return false; //Breite / H�he reicht nicht aus
	return true;
}

inline bool FRoom::isXSeperable(FIntPoint MinSize, int MinArea) {
	return (Size.X >= MinSize.X * 2);
}

inline bool FRoom::isYSeperable(FIntPoint MinSize, int MinArea) {
	return (Size.Y >= MinSize.Y * 2);
}

void FRoom::printRoomOnGroundGrid(UGroundGrid* grid)
{
	for (auto wall : walls) {
		wall->Execute_printComponentOnGrid(wall.GetObject(), grid);
	}
}

inline void FRoom::printRoom() {
	UE_LOG(LogTemp, Warning, TEXT("Origin (%i, %i)   | Size (%i, %i)"), Origin.X, Origin.Y, Size.X, Size.Y);
}
