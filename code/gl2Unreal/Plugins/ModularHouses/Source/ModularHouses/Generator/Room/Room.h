// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "list"
#include "../Components/Wall.h"
#include "Grid/GroundGrid.h"

struct MODULARHOUSES_API FRoom
{


	bool operator == (const FRoom& otherRoom) const { return roomID == otherRoom.roomID; }
	bool operator != (const FRoom& otherRoom) const { return !operator==(otherRoom); }


	FRoom() {}
	void InitializeRoom(FIntPoint Origin, FIntPoint Size, int roomID);
	
	TArray<TScriptInterface<IComponentInterface>> walls;

	int roomID;
	FIntPoint Origin;
	FIntPoint Size;
	int Area;
	std::list<FRoom> adjacencyListTop;
	std::list<FRoom> adjacencyListLeft;
	std::list<FRoom> adjacencyListRight;
	std::list<FRoom> adjacencyListBottom;
	FIntPoint ObenLinks;
	FIntPoint ObenRechts;
	FIntPoint UntenLinks;
	FIntPoint UntenRechts;

	void generateAllPoints();

	bool isValid(FIntPoint MinSize, int MinArea);
	bool isVolumeValid(int MinArea);

	bool isSeperable(FIntPoint MinSize, int MinArea);
	bool isXSeperable(FIntPoint MinSize, int MinArea);
	bool isYSeperable(FIntPoint MinSize, int MinArea);

	void printRoomOnGroundGrid(UGroundGrid* grid);

	void printRoom();
};
