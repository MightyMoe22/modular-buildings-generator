// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "list"
#include "Room.h"
#include "RoomWrapper.h"
#include "../Grid/GroundGrid.h"
#include "Generator/GroundGeneratorData.h"
#include "Generator/SubGenerator/SubGeneratorInterface.h"
#include "RoomGenerator.generated.h"

UCLASS()
class MODULARHOUSES_API URoomGenerator : public UObject, public ISubGeneratorInterface
{
	GENERATED_BODY()
private:
	//Room-Variables
	int RoomIDCounter = 0;

public:
	FIntPoint MinRoomSize = { 4,4 };
	int MinRoomArea = 50; 
	UGroundGeneratorData* groundGeneratorData;

public:
	std::list<FRoom> FinalRooms;
	//---------- RoomGenerator ---------- 

	

	void initialize(UGroundGeneratorData* groundGeneratorData);

	void generate(TArray<URoomWrapper*> RoomsTodoQueue);  //generate

	/*
	returns a room with Unique ID
	*/
	FRoom generateRoom(FIntPoint Origin, FIntPoint Size);
	void fixAdjacencyLists(FRoom* roomToFix);
	
	bool isValidRoom(FRoom* room);

	UFUNCTION(BlueprintNativeEvent)
	void draw(UGroundGrid* groundGrid);
	virtual void draw_Implementation(UGroundGrid* groundGrid);

private:
	void GenerateDoorsBetweenRooms(UGroundGrid* grid);
	void replaceAllWallsWithDoors(FIntPoint position, UGroundGrid* grid, EModuleDirection direction);
	/*Zuf�llige Raum-ID*/
	int getUniqueRoomID();


};
