// Copyright Maurice Langer und Jakob Seitz 2020


#include "FloorGenerator.h"

void UFloorGenerator::GenerateFloors(FIntVector newSize)
{
	floorManager->initialize(newSize);
	if (generator->groundGeneratorData->Size != newSize)
		UE_LOG(LogTemp, Error, TEXT("GroundGenerator has invalid Size. Size should be (%i, %i) but is (%i, %i)"), newSize.X, newSize.Y, generator->groundGeneratorData->Size.X, generator->groundGeneratorData->Size.Y);

	for (int i = 1; i <= newSize.Z; i++)
	{
		generator->groundGeneratorData->activeFloor = i;
		auto floor = generator->Execute_generateGroundGridForActiveFloor(generator.GetObject());
		floorManager->addFloor(deepCopy(floor));
	}

	generateStairs();
}

UGroundGrid* UFloorGenerator::deepCopy(UGroundGrid* oldGroundGrid) {
	auto Size = generator->groundGeneratorData->Size;
	UGroundGrid* newGroundGrid = NewObject<UGroundGrid>();
	newGroundGrid->initialize(oldGroundGrid->Size);
	//deep copy
	for (int x = 0; x < Size.X; x++)
		for (int y = 0; y < Size.Y; y++)
		{
			FIntPoint position = { x,y };
			for (auto gridTile : *oldGroundGrid->getGridTilesAtPosition(position)) {
				newGroundGrid->addGridTileAtPosition(gridTile, position);
			}
		}
	return newGroundGrid;
}

void UFloorGenerator::generateStairs()
{
	NewObject<UStairGenerator>()->draw(floorManager, generator->groundGeneratorData);
}
