// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Generator/GroundGeneratorData.h"
#include "../GroundPlan/GroundPlan.h"
#include "GroundData.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API UGroundData : public UObject
{
	GENERATED_BODY()

public:
	FIntVector Size;
	FIntPoint minMiddleSize;
	FIntPoint maxMiddleSize;
	FIntPoint minDistance;
	FIntPoint maxDistance;
	UGroundPlan* groundPlan;
	UGroundGeneratorData* groundGeneratorData;

	void initialize(UGroundGeneratorData* groundGeneratorData, UGroundPlan* groundPlan);
};
