// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Grid/GroundGrid.h"
#include "../FGridTileStruct.h"
#include "../Room/RoomWrapper.h"
#include "../Room/RoomGenerator.h"
#include "../Roof/RoofData.h"
#include "GroundData.h"
#include "GroundGridGeneratorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGroundGridGeneratorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MODULARHOUSES_API IGroundGridGeneratorInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.

private:


public:	

	/*
	* Generates a Random CrossGround
	* @param MinMiddleSize Mindestbreite in X und Y - Richtung des entstehenden Kreuzes
	*/
	UFUNCTION(BlueprintNativeEvent)
	void GenerateRandomGround(UGroundData* groundData); //generate

	UFUNCTION(BlueprintNativeEvent)
	TArray<URoofData*> GetRoofData();

	/*UFUNCTION(BlueprintNativeEvent)
	bool isGridTileBorder(FIntPoint &location, EModuleDirection &direction);*/

	UFUNCTION(BlueprintNativeEvent)
	void drawGroundGrid(UGroundGrid* GroundGrid); //draw

	UFUNCTION(BlueprintNativeEvent)
	TArray<URoomWrapper*> getPrimaryRooms(URoomGenerator* roomGenerator);

	UFUNCTION(BlueprintNativeEvent)
	void updateGrid(); //update

	//UFUNCTION(BlueprintNativeEvent)
	//void GeneratePrimaryRoomsForCrossGround(std::list<FRoom>* PrimaryRooms, FIntPoint MinSize, int MinArea);
};
