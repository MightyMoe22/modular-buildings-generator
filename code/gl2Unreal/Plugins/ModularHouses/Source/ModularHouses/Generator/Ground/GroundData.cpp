// Copyright Maurice Langer und Jakob Seitz 2020


#include "GroundData.h"

void UGroundData::initialize(UGroundGeneratorData* newGroundGeneratorData, UGroundPlan* newGroundPlan)
{
	this->groundGeneratorData = newGroundGeneratorData;
	this->Size = groundGeneratorData->Size;
	//this->maxMiddleSize = { FMath::Min(Size.Z * 2 - 1, Size.X - 4),FMath::Min(Size.Z * 2 - 1, Size.Y - 4) };
	int RoofHeightSpace = Size.Z - groundGeneratorData->maxRoofHeight;
	int maxRoofHeight = FMath::Min(RoofHeightSpace, groundGeneratorData->maxRoofHeight);
	int minRoofHeight = FMath::Min(RoofHeightSpace, groundGeneratorData->minRoofHeight);
	this->maxMiddleSize = { maxRoofHeight * 2,maxRoofHeight * 2};
	this->minMiddleSize = { minRoofHeight *2, minRoofHeight  *2 };
	this->minDistance = { groundGeneratorData->minRoofDistance,groundGeneratorData->minRoofDistance };
	this->maxDistance = { groundGeneratorData->maxRoofDistance,groundGeneratorData->maxRoofDistance };
	this->groundPlan = newGroundPlan;
}
