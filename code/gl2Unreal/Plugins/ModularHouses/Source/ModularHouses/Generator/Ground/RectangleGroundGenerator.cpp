// Copyright Maurice Langer und Jakob Seitz 2020


#include "RectangleGroundGenerator.h"

void URectangleGroundGenerator::GenerateRandomGround_Implementation(UGroundData* newGroundData)
{
	horizontalRectangles = {};
	verticalRectangles = {};
	this->groundData = newGroundData;
	mergedRectangles = groundData->groundPlan->mergedGroundRectangles;
	this->Size = groundData->Size;

	TArray<TScriptInterface<IRectangleInterface>> rectangles;

	auto stream = groundData->groundGeneratorData->getRandomStream();
	UE_LOG(LogTemp, Warning, TEXT("(RectangleGroundGenerator) randomized seed: %i"), stream.GetCurrentSeed())
	for (int i : { 0, 1 }) // for x,y
	{
		int activeX = stream.RandRange(groundData->minDistance[i], groundData->maxDistance[i]);
		int randomXSize = stream.RandRange(groundData->minMiddleSize[i], groundData->maxMiddleSize[i]);
		if (randomXSize % 2 == 1) randomXSize--;
		while (activeX + randomXSize + groundData->minDistance[i] < Size[i]) {
			URectangle* rectangle = NewObject<URectangle>(this);
			if (i == 0) {
				FIntPoint size = { randomXSize, Size.Y };
				FIntPoint topLeft = { activeX,0 };
				rectangle->createRectangle(size, topLeft);
				horizontalRectangles.Add(rectangle);
			}
			else {
				FIntPoint size = { Size.X , randomXSize };
				FIntPoint topLeft = { 0,activeX };
				rectangle->createRectangle(size, topLeft);
				verticalRectangles.Add(rectangle);
			}
			rectangles.Add(rectangle);
			activeX += stream.RandRange(groundData->minDistance[i], groundData->maxDistance[i]);
			activeX += randomXSize;
			randomXSize = stream.RandRange(groundData->minMiddleSize[i], groundData->maxMiddleSize[i]);
			if (randomXSize % 2 == 1) randomXSize--;
		}
	}
	
	int minRoofs = 1000;
	int maxRoofs = 1000;
	if (groundData->groundGeneratorData->minRoofNumber)
		minRoofs = groundData->groundGeneratorData->minRoofNumber;
	if (groundData->groundGeneratorData->maxRoofNumber) {
		maxRoofs = groundData->groundGeneratorData->maxRoofNumber;
		if (!groundData->groundGeneratorData->minRoofNumber)
			minRoofs = 1;
	}
	int roofNumber = stream.RandRange(minRoofs, maxRoofs);

	while (rectangles.Num() > roofNumber && rectangles.Num() > 0) {
		auto theChosenOne = rectangles[stream.RandRange(0, rectangles.Num() - 1)];
		horizontalRectangles.Remove(theChosenOne);
		verticalRectangles.Remove(theChosenOne);
		rectangles.Remove(theChosenOne);
	}

	mergedRectangles->mergeRectangles(rectangles);
	roomRectangles = mergedRectangles->getscaledURectangleMerger(1);
	createRoofData();
}

void URectangleGroundGenerator::drawGroundGrid_Implementation(UGroundGrid* GroundGrid)
{
	auto groundGeneratorData = groundData->groundGeneratorData;
	auto stream = groundGeneratorData->getRandomStream();
	//print
	for (auto comp : generateComponentsForActiveFloor())
		comp->Execute_printComponentOnGrid(comp.GetObject(), GroundGrid);

	if (groundGeneratorData->activeFloor == 1) {
		int numberOfDoors = stream.FRandRange(groundGeneratorData->minEntryDoors, groundGeneratorData->maxEntryDoors);
		generateDoors(GroundGrid, numberOfDoors);
	}
}

TArray<URoofData*> URectangleGroundGenerator::GetRoofData_Implementation()
{
	return roofDatas;
}

TArray<URoomWrapper*> URectangleGroundGenerator::getPrimaryRooms_Implementation(URoomGenerator* roomGenerator)
{
	TArray<URoomWrapper*> rooms;
	for (auto rectangle : roomRectangles->rectangalizeMergedArea()) {
		URoomWrapper* roomWrapper = NewObject<URoomWrapper>(this);
		roomWrapper->room = roomGenerator->generateRoom(rectangle->TopLeft, rectangle->Size);
		rooms.Add(roomWrapper);
	}
	return rooms;
}

void URectangleGroundGenerator::updateGrid_Implementation()
{
	for (auto roofData : roofDatas) {
		if (roofData->startedGenerating) {
			if(mergedRectangles->rectangles.Find(roofData->owningRectangle) != INDEX_NONE) //Not already removed
				mergedRectangles->rectangles.Remove(roofData->owningRectangle);
		}
	}
	groundData->groundPlan->createRoomRectangles();
	roomRectangles = groundData->groundPlan->roomRectangles;
}

void URectangleGroundGenerator::createRoofData()
{
	roofDatas = {};
	int i = 0;
	for (auto rectangle : horizontalRectangles) {
		URoofData* roof = NewObject<URoofData>(this);
		roof->Initialize_RoofData(i++, rectangle->TopLeft, rectangle->Size, D_North, rectangle);
		roofDatas.Add(roof);
	}
	for (auto rectangle : verticalRectangles) {
		URoofData* roof = NewObject<URoofData>(this);
		roof->Initialize_RoofData(i++, rectangle->TopLeft, rectangle->Size, D_East, rectangle);
		roofDatas.Add(roof);
	}
}

TArray<TScriptInterface<IComponentInterface>> URectangleGroundGenerator::generateComponentsForActiveFloor()
{
	TArray<TScriptInterface<IComponentInterface>> GroundComponents;

	if (UOuterWall::placedWalls.Num() != 0) {
		UOuterWall::placedWalls.Empty();
	}
	//print OuterWalls
	for (auto rectangle : mergedRectangles->rectangles) {
		//generate components
		auto topWall = NewObject<UOuterWall>();
		auto bottomWall = NewObject<UOuterWall>();
		auto leftWall = NewObject<UOuterWall>();
		auto rightWall = NewObject<UOuterWall>();

		topWall->InitWall(rectangle->Size.X, rectangle->TopLeft, EModuleDirection::North);
		topWall->addCorner(topWall->TopLeft, EModuleDirection::SouthEast);

		bottomWall->InitWall(rectangle->Size.X, rectangle->BottomLeft - FIntPoint(0, 1), EModuleDirection::South);
		bottomWall->addCorner(bottomWall->BottomRight, EModuleDirection::NorthWest);

		leftWall->InitWall(rectangle->Size.Y, rectangle->TopLeft, EModuleDirection::West);
		leftWall->addCorner(leftWall->BottomLeft, EModuleDirection::NorthEast);

		rightWall->InitWall(rectangle->Size.Y, rectangle->TopRight - FIntPoint(1, 0), EModuleDirection::East);
		rightWall->addCorner(rightWall->TopRight, EModuleDirection::SouthWest);

		auto mergedRoofRectangles = groundData->groundPlan->mergedRoofRectangles;

		//horizontal crossed
		if (horizontalRectangles.Contains(rectangle)) {
			for (auto wall : { leftWall, rightWall }) {
				wall->cuttingHouseRectangles = mergedRectangles->getVerticalCrossedRectangles(wall);
				wall->cuttingRoofs = mergedRoofRectangles->getVerticalCrossedRectangles(wall);
			}
		}
		//vertical crossed
		if (verticalRectangles.Contains(rectangle)) {
			for (auto wall : { topWall, bottomWall }) {
				wall->cuttingHouseRectangles = mergedRectangles->getHorizontalCrossedRectangles(wall);
				wall->cuttingRoofs = mergedRoofRectangles->getHorizontalCrossedRectangles(wall);
			}
		}

		GroundComponents.Append({ topWall, bottomWall, leftWall, rightWall });
	}

	//print Floor and Ceiling
	for (auto rectangle : roomRectangles->rectangalizeMergedArea()) {
		UGeneralComponent* floor = NewObject<UGeneralComponent>();
		UGeneralComponent* ceiling = NewObject<UGeneralComponent>();

		for (auto comp : { floor, ceiling })
			comp->initComponent(rectangle->Size, rectangle->TopLeft);

		floor->gridTiles.Add(FGridTileStruct(EModuleCategory::Floor, EModuleDirection::None, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));
		ceiling->gridTiles.Add(FGridTileStruct(EModuleCategory::Ceiling, EModuleDirection::None, EModuleEnvironment::Indoor, EModuleFacing::Facing_Inside));

		GroundComponents.Append({ floor, ceiling });
	}
	return GroundComponents;
}

void URectangleGroundGenerator::generateDoors(UGroundGrid* GroundGrid, int numberOfDoors)
{
	auto stream = groundData->groundGeneratorData->getRandomStream();
	auto placedWalls = UOuterWall::placedWalls;

	if (placedWalls.Num() == 0) return;

	TArray<FIntPoint> keys = TArray<FIntPoint>();
	placedWalls.GenerateKeyArray(keys);

	for (int i = 0; i < numberOfDoors; i++) {
		if (keys.Num() == 0) return;
		int32 randomIndex = stream.RandRange(0, keys.Num()-1);
		auto randPosition = keys[randomIndex];

		auto gridTiles = GroundGrid->getGridTilesAtPosition(randPosition);
		auto gridTilesRef = *gridTiles;
		auto direction = placedWalls[randPosition];
		auto oppDirection = FDirectionManipulator::rotateDirection(direction, 180);
		FIntPoint dir = FDirectionManipulator::directionToVector(direction);
		auto oppositeGridTiles = GroundGrid->getGridTilesAtPosition(randPosition + dir);
		auto oppositeGridTilesRef = *oppositeGridTiles;

		TArray<FGridTileStruct> removeList = TArray<FGridTileStruct>();
		TArray<FGridTileStruct> removeListOpp = TArray<FGridTileStruct>();
		//find all walls at pos
		for (FGridTileStruct gridTile : gridTilesRef) {
			if (gridTile.moduleCategory == EModuleCategory::Window && gridTile.moduleDirection == direction) {
				removeList.Add(gridTile);
			}
		}
		//replace them with doors
		for (auto gridTile : removeList) {
			gridTiles->remove(gridTile);
			FGridTileStruct newGridTile = gridTile;
			newGridTile.moduleCategory = EModuleCategory::Door;
			newGridTile.moduleEnvironment = EModuleEnvironment::Outdoor;

			gridTiles->push_front(newGridTile);
		}

		//find all walls at opposite pos
		for (FGridTileStruct gridTile : oppositeGridTilesRef) {
			if (gridTile.moduleCategory == EModuleCategory::Window && gridTile.moduleDirection == oppDirection) {
				removeListOpp.Add(gridTile);
			}
		}
		//replace them with doors
		for (auto gridTile : removeListOpp) {
			oppositeGridTiles->remove(gridTile);
			FGridTileStruct newGridTile = gridTile;
			newGridTile.moduleCategory = EModuleCategory::Door;
			newGridTile.moduleEnvironment = EModuleEnvironment::Outdoor;

			oppositeGridTiles->push_front(newGridTile);
		}

		removeListOpp.Empty();
		removeList.Empty();
		keys.Remove(randPosition);
		keys.Remove(randPosition + dir);
	}
	
}
