// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GroundGridGeneratorInterface.h"
#include "../Shapes/Rectangle/RectangleMerger/RectangleMerger.h"
#include "Generator/GroundGeneratorData.h"
#include "./Generator/Components/OuterWall.h"
#include "RectangleGroundGenerator.generated.h"

/**
 * 
 */
UCLASS()
class MODULARHOUSES_API URectangleGroundGenerator : public UObject, public IGroundGridGeneratorInterface
{
	GENERATED_BODY()

private:
	FIntVector Size;
	UGroundData* groundData;

	TArray<TScriptInterface<IRectangleInterface>> horizontalRectangles;
	TArray<TScriptInterface<IRectangleInterface>> verticalRectangles;
	TArray<URoofData*> roofDatas;
	URectangleMerger* mergedRectangles;
	URectangleMerger* roomRectangles;

public:
	UFUNCTION(BlueprintNativeEvent)
	void GenerateRandomGround(UGroundData* groundData);
	virtual void GenerateRandomGround_Implementation(UGroundData* groundData);
	
	/*UFUNCTION(BlueprintNativeEvent)
	bool isGridTileBorder(FIntPoint& location, EModuleDirection& direction);
	virtual bool isGridTileBorder_Implementation(FIntPoint& location, EModuleDirection& direction);*/

	UFUNCTION(BlueprintNativeEvent)
	void drawGroundGrid(UGroundGrid* GroundGrid);
	virtual void drawGroundGrid_Implementation(UGroundGrid* GroundGrid);

	UFUNCTION(BlueprintNativeEvent)
	TArray<URoofData*> GetRoofData();
	virtual TArray<URoofData*> GetRoofData_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	TArray<URoomWrapper*> getPrimaryRooms(URoomGenerator* roomGenerator);
	virtual TArray<URoomWrapper*> getPrimaryRooms_Implementation(URoomGenerator* roomGenerator);

	UFUNCTION(BlueprintNativeEvent)
	void updateGrid();
	void updateGrid_Implementation();

private:
	void createRoofData();
	TArray<TScriptInterface<IComponentInterface>> generateComponentsForActiveFloor();
	void generateDoors(UGroundGrid* GroundGrid, int numberOfDoors);
};
