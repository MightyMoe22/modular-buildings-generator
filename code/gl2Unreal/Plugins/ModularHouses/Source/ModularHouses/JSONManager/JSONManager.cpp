// Copyright Maurice Langer und Jakob Seitz 2020

#include "JSONManager.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "JsonDomBuilder.h"

JSONManager::JSONManager()
{
}

JSONManager::~JSONManager()
{
}

bool JSONManager::ReadJsonFile(FString FilePath, TSharedPtr<FJsonObject>& JsonObject)
{
	FString JsonString;
	FFileHelper::LoadFileToString(JsonString, *GeneratePathToFile(FilePath));
	return GenerateJsonObject(JsonString, JsonObject);
}

void JSONManager::WriteJsonFile(FString FilePath, TSharedPtr<FJsonObject> JsonObject)
{
	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);

	FFileHelper::SaveStringToFile(OutputString, *GeneratePathToFile(FilePath));
}

TArray<TSharedPtr<FJsonValue>> JSONManager::ConvertVectorToArray(FVector InputVector)
{
	FJsonDomBuilder::FArray OutputArray;
	OutputArray.Add(InputVector.X, InputVector.Y, InputVector.Z);

	return OutputArray.AsJsonValue()->AsArray();
}

FVector JSONManager::ConvertArrayToVector(TArray<TSharedPtr<FJsonValue>> InputArray)
{
	FVector OutputVector = FVector(0, 0, 0);

	if(InputArray.GetData() != nullptr)
	{
		OutputVector = FVector(InputArray[0]->AsNumber(), InputArray[1]->AsNumber(), InputArray[2]->AsNumber());
	}

	return OutputVector;
}

bool JSONManager::GenerateJsonObject(FString JsonString, TSharedPtr<FJsonObject>& JsonObject)
{
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(JsonString);
	TSharedPtr<FJsonObject> ReadJsonObject = MakeShareable(new FJsonObject);
	JsonObject = ReadJsonObject;

	return FJsonSerializer::Deserialize(JsonReader, JsonObject) && JsonObject.IsValid();
}

FString JSONManager::GeneratePathToFile(FString FilePath)
{
	FString FullFilePath = FPaths::ProjectPluginsDir();
	FullFilePath += FilePath;

	return FullFilePath;
}
