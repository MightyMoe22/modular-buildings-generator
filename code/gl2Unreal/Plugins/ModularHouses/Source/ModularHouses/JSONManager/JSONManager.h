// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModularBuildingComponentData.h"
#include "Json/Public/Serialization/JsonSerializer.h"

static class MODULARHOUSES_API JSONManager
{
public:
	JSONManager();
	~JSONManager();

	static bool ReadJsonFile(FString FilePath, TSharedPtr<FJsonObject>& JsonObject);
	static void WriteJsonFile(FString FilePath, TSharedPtr<FJsonObject> JsonObject);
	static TArray<TSharedPtr<FJsonValue>> ConvertVectorToArray(FVector InputVector);
	static FVector ConvertArrayToVector(TArray<TSharedPtr<FJsonValue>> InputArray);

private:

	static bool GenerateJsonObject(FString JsonString, TSharedPtr<FJsonObject>& JsonObject);
	static FString GeneratePathToFile(FString FilePath);

};
