// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Modules/ModuleData.h"
#include "Modules/ModuleActor.h"
#include <ModularHouses\Generator\FGridTileStruct.h>
#include "GridTileComponent.generated.h"


UCLASS(ClassGroup=(Grid), meta=(BlueprintSpawnableComponent, UsesHierarchy) )
class MODULARHOUSES_API UGridTileComponent : public UChildActorComponent
{
	GENERATED_BODY()

	///--------------------- Variables ---------------------
public:

	//All modules that are contained 'in' the grid tile
	UPROPERTY(VisibleAnywhere, Category = "Tile Content")
	TArray<AModuleActor*> AssignedModules;

	UPROPERTY(EditAnywhere, Category = "Tile Testing")
	EModuleDirection TestDirection = EModuleDirection::None;
	UPROPERTY(EditAnywhere, Category = "Tile Testing")
	EModuleCategory TestCategory = EModuleCategory::None;
	UPROPERTY(EditAnywhere, Category = "Tile Testing")
	EModuleFacing TestFacing = EModuleFacing::None;
	UPROPERTY(EditAnywhere, Category = "Tile Testing")
	uint32 TestListElements = 0;
	UPROPERTY(EditAnywhere, Category = "Tile Testing")
	uint32 TestSelectedElement = 0;
	
private:

	UPROPERTY()
	bool GridVisualizationStatus = true;

	UPROPERTY()
	UStaticMesh* GridVisualizationMesh;

	UPROPERTY()
	UStaticMeshComponent* GridVisualizationComponent;

	///--------------------- Functions ---------------------
public:
	// Sets default values for this component's properties
	UGridTileComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void LoadModuleIntoGridTile(FModuleData ModuleData, FGridTileStruct TileStruct);

private:

	UFUNCTION()
	void ToggleContentVisibility(bool EffectBlockVisibility = true);

	UFUNCTION()
	void SetModuleOrientation(AModuleActor* Module, FGridTileStruct TileStruct);

	UFUNCTION()
	void FlippModule(AModuleActor* Module, FGridTileStruct TileStruct);

	UFUNCTION()
	void SetGridVisualizationStatus(bool NewStatus);

	UFUNCTION()
	AModuleActor* CreateAndAddNewModule();

protected:

	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;

	UFUNCTION()
	void SetupGridVisualization();
	
};