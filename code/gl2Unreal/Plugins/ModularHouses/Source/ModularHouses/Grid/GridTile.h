// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Actor.h"
#include "Modules/ModuleData.h"
#include "Modules/ModuleActor.h"
#include "GridTile.generated.h"

UCLASS( ClassGroup=(Grid), meta=(BlueprintSpawnableComponent) )
class MODULARHOUSES_API AGridTile : public AActor
{
	GENERATED_BODY()

///--------------------- Variables ---------------------
public:	

	// Sets default values for this component's properties
	AGridTile();

	UPROPERTY()
	int AssignedModuleLimit = 5;

	//All modules that are contained 'in' the grid tile
	UPROPERTY(VisibleAnywhere, Category = "Tile Content")
	TArray<AModuleActor*> AssignedModules;

	UPROPERTY(EditAnywhere, Category = "Tile Content")
	EModuleDirection TestDirection = EModuleDirection::None;

private:

	UPROPERTY()
	bool GridVisualizationStatus = true;

	UPROPERTY()
	UStaticMesh* GridVisualizationMesh;

	UPROPERTY()
	UStaticMeshComponent* GridVisualizationComponent;


///--------------------- Functions ---------------------
public:

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void ToggleContentVisibility(bool EffectBlockVisibility = true);

	UFUNCTION(CallInEditor, Category = "Tile Content")
	void TestLoading();

	UFUNCTION()
	void LoadModuleIntoGridTile(FModuleData ModuleData, EModuleDirection Direction);

private:

	UFUNCTION()
	void SetModuleOrientation(AModuleActor* Module, EModuleDirection Direction);

	UFUNCTION()
	void SetGridVisualizationStatus(bool NewStatus);

	UFUNCTION()
	AModuleActor* CreateAndAddNewModule();

protected:

	virtual void Destroyed() override;
	virtual void OnConstruction(const FTransform& Transform) override;

	UFUNCTION()
	void SetupGridVisualization();
};
