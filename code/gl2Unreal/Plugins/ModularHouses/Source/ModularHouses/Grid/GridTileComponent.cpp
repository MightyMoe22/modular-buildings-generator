// Copyright Maurice Langer und Jakob Seitz 2020

#include "GridTileComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "JSONManager/JSONManager.h"
#include "Modules/ModularBuildingVolumeData.h"
//For Testing Function
#include "Modules/ModuleContainer.h"

UGridTileComponent::UGridTileComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetupGridVisualization();
}

// Called every frame
void UGridTileComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UGridTileComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	for(auto* object : AssignedModules)
	{
		AModuleActor* ModuleActor = (AModuleActor*) object;
		if(ModuleActor != nullptr)
		{
			ModuleActor->Destroy();
		}
	}
	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

void UGridTileComponent::ToggleContentVisibility(bool EffectBlockVisibility)
{
	if(EffectBlockVisibility)
	{
		GridVisualizationComponent->ToggleVisibility(true);
	}
}

/**
* Loads the given ModuleData into the GridTile by converting the ModuleData into a ModuleActor and applies the given orientation to it.
* @param ModuleData The ModuleData that should be loaded into the GridTile.
* @param Orientation The ModuleOrientation that defines how the ModuleActor should be rotated in the GridTile space.
*/
void UGridTileComponent::LoadModuleIntoGridTile(FModuleData ModuleData, FGridTileStruct TileStruct)
{

	AModuleActor* NewModule = CreateAndAddNewModule();
	if(NewModule != nullptr)
	{
		if(NewModule->LoadModuleData(ModuleData) && GridVisualizationStatus)
		{
			//If at least one module is loaded, set GridVisualization to invisible
			SetGridVisualizationStatus(false);
		}
		SetModuleOrientation(NewModule, TileStruct);
		if(TileStruct.flippedAtDirection != EModuleDirection::None)
		{
			FlippModule(NewModule, TileStruct);
		}
	}
}

void UGridTileComponent::SetModuleOrientation(AModuleActor* Module, FGridTileStruct TileStruct)
{
	switch(TileStruct.moduleDirection)
	{
		case EModuleDirection::South:
		case EModuleDirection::SouthEast:
			Module->AddActorLocalRotation(FRotator(0, 0, 0));
			break;
		case EModuleDirection::West:
		case EModuleDirection::SouthWest:
			Module->AddActorLocalRotation(FRotator(0, 90, 0));
			break;
		case EModuleDirection::North:
		case EModuleDirection::NorthWest:
			Module->AddActorLocalRotation(FRotator(0, 180, 0));
			break;
		case EModuleDirection::East:
		case EModuleDirection::NorthEast:
			Module->AddActorLocalRotation(FRotator(0, 270, 0));
			break;
		case EModuleDirection::None:
			break;
		default:
			break;
	}
}

void UGridTileComponent::FlippModule(AModuleActor* Module, FGridTileStruct TileStruct)
{
	FVector currentScale = Module->GetTransform().GetScale3D();
	Module->SetActorScale3D(FVector(currentScale.X, -currentScale.Y, currentScale.Z));
}

void UGridTileComponent::SetGridVisualizationStatus(bool NewStatus)
{
	GridVisualizationStatus = NewStatus;
	GridVisualizationComponent->SetVisibility(GridVisualizationStatus);
}

AModuleActor* UGridTileComponent::CreateAndAddNewModule()
{
	AModuleActor* AddedModule = nullptr;
	//Get world instance for spawning actors
	UWorld* TheWorld = GetWorld();
	int IndexOfLastModule = AssignedModules.Num();
	if(TheWorld != nullptr)
	{
		IndexOfLastModule++;
		AModuleActor* ModuleActor = (AModuleActor*) GetWorld()->SpawnActor(AModuleActor::StaticClass());
		ModuleActor->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
		//Set Module at the center of the component
		ModuleActor->SetActorRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), FVector(1, 1, 1)));
		AddedModule = AssignedModules[AssignedModules.Add(ModuleActor)];
	}
	return AddedModule;
}

void UGridTileComponent::SetupGridVisualization()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		GridVisualizationMesh = MeshAsset.Object;
	}
	GridVisualizationComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	GridVisualizationComponent->SetStaticMesh(GridVisualizationMesh);
	// Create hierarchy by setting the component as chide from RootComponent + keep childs position relative to parent 
	GridVisualizationComponent->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling;
	TSharedPtr<FJsonObject> JsonObject = NULL;
	if(JSONManager::ReadJsonFile(Path, JsonObject))
	{
		BoxScaling = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject).VolumeScale;
	} else
	{
		BoxScaling = FVector(1.f, 1.f, 1.f);
	}
	GridVisualizationComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}

