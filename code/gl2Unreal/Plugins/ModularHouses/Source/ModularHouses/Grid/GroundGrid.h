// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "list"
#include "Generator/FGridTileStruct.h"
#include "UObject/NoExportTypes.h"
#include "GroundGrid.generated.h"

UCLASS()
class MODULARHOUSES_API UGroundGrid : public UObject
{
	GENERATED_BODY()

private:
	std::list<FGridTileStruct>** GroundGrid;

public:
	FIntPoint Size;
	UGroundGrid() {}
	UGroundGrid(FIntPoint newSize) { initialize(newSize); }
	~UGroundGrid();
	void initialize(FIntPoint newSize);
	void addGridTileAtPosition(FGridTileStruct gridTile, FIntPoint position);
	std::list<FGridTileStruct>** getGroundGrid();
	std::list<FGridTileStruct>* getGridTilesAtPosition(FIntPoint position);
	void clearGridTilesAtPosition(FIntPoint position);
	void clearGroundGrid();
private:
	bool verifyPosition(FIntPoint position);
};
