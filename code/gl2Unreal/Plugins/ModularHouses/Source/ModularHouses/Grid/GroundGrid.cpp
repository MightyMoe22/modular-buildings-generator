// Copyright Maurice Langer und Jakob Seitz 2020

#include "GroundGrid.h"

void UGroundGrid::initialize(FIntPoint newSize)
{
	GroundGrid = new std::list<FGridTileStruct> * [newSize.X];
	for(int x = 0; x < newSize.X; x++)
	{
		GroundGrid[x] = new std::list<FGridTileStruct>[newSize.Y];
	}
	this->Size = newSize;
}

UGroundGrid::~UGroundGrid()
{
	for(int x = 0; x < Size.X; x++)
	{
		delete[] GroundGrid[x];
	}
	delete[] GroundGrid;
	GroundGrid = nullptr;
}

void UGroundGrid::addGridTileAtPosition(FGridTileStruct gridTile, FIntPoint position)
{
	if(verifyPosition(position)) GroundGrid[position.X][position.Y].push_front(gridTile);
	else UE_LOG(LogTemp, Error, TEXT("Index out of bounce!"));
}

std::list<FGridTileStruct>** UGroundGrid::getGroundGrid()
{
	return GroundGrid;
}

std::list<FGridTileStruct>* UGroundGrid::getGridTilesAtPosition(FIntPoint position)
{
	if(verifyPosition(position)) return &GroundGrid[position.X][position.Y];

	UE_LOG(LogTemp, Error, TEXT("Index out of bounce!"));
	return &GroundGrid[position.X][position.Y];
}

void UGroundGrid::clearGridTilesAtPosition(FIntPoint position)
{
	getGridTilesAtPosition(position)->clear();
}

void UGroundGrid::clearGroundGrid()
{
	for(int x = 0; x < Size.X; x++)
	{
		for(int y = 0; y < Size.Y; y++)
		{
			clearGridTilesAtPosition({x,y});
		}
	}
}

bool UGroundGrid::verifyPosition(FIntPoint position)
{
	return position.X < Size.X&& position.X >= 0 && position.Y < Size.Y&& position.Y >= 0;
}
