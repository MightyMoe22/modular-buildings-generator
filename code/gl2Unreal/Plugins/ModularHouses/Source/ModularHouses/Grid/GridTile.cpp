// Copyright Maurice Langer und Jakob Seitz 2020

#include "GridTile.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "JSONManager/JSONManager.h"
#include "Modules/ModularBuildingVolumeData.h"

// Sets default values for this component's properties
AGridTile::AGridTile()
{
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Root"));

	SetupGridVisualization();
}

void AGridTile::OnConstruction(const FTransform& Transform)
{
	UE_LOG(LogTemp, Warning, TEXT("Construced"));
	Super::OnConstruction(Transform);
}

void AGridTile::Destroyed()
{
	for(auto* object : AssignedModules)
	{
		AModuleActor* ModuleActor = (AModuleActor*) object;
		if(ModuleActor != nullptr)
		{
			ModuleActor->Destroy();
		}
	}

	Super::Destroyed();
}

// Called every frame
void AGridTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGridTile::ToggleContentVisibility(bool EffectBlockVisibility)
{
	if(EffectBlockVisibility)
	{
		GridVisualizationComponent->ToggleVisibility(true);
	}
}

void AGridTile::TestLoading()
{
	FModuleData dummy = FModuleData();
}

void AGridTile::LoadModuleIntoGridTile(FModuleData ModuleData, EModuleDirection Direction)
{
	AModuleActor* NewModule = CreateAndAddNewModule();
	if(NewModule != nullptr)
	{
		if(NewModule->LoadModuleData(ModuleData) && GridVisualizationStatus)
		{
			//If at least one module is loaded, set GridVisualization to invisible
			SetGridVisualizationStatus(false);
		}
		SetModuleOrientation(NewModule, Direction);
	}
}

void AGridTile::SetModuleOrientation(AModuleActor* Module, EModuleDirection Direction)
{
	switch(Direction)
	{
		case EModuleDirection::North:
			Module->AddActorLocalRotation(FRotator(0, 0, 0));
			break;
		case EModuleDirection::NorthEast:
			Module->AddActorLocalRotation(FRotator(0, 45, 0));
			break;
		case EModuleDirection::East:
			Module->AddActorLocalRotation(FRotator(0, 90, 0));
			break;
		case EModuleDirection::SouthEast:
			Module->AddActorLocalRotation(FRotator(0, 135, 0));
			break;
		case EModuleDirection::South:
			Module->AddActorLocalRotation(FRotator(0, 180, 0));
			break;
		case EModuleDirection::SouthWest:
			Module->AddActorLocalRotation(FRotator(0, 225, 0));
			break;
		case EModuleDirection::West:
			Module->AddActorLocalRotation(FRotator(0, 270, 0));
			break;
		case EModuleDirection::NorthWest:
			Module->AddActorLocalRotation(FRotator(0, 315, 0));
			break;
	}
}

void AGridTile::SetGridVisualizationStatus(bool NewStatus)
{
	GridVisualizationStatus = NewStatus;
	GridVisualizationComponent->SetVisibility(GridVisualizationStatus);
}

AModuleActor* AGridTile::CreateAndAddNewModule()
{
	AModuleActor* AddedModule = nullptr;
	//Get world instance for spawning actors
	UWorld* TheWorld = GetWorld();
	if(TheWorld != nullptr)
	{
		int IndexOfLastModule = AssignedModules.Num();
		if(IndexOfLastModule++ < AssignedModuleLimit)
		{
			FVector originLocation = this->GetTransform().GetLocation();
			AModuleActor* ModuleActor = (AModuleActor*) GetWorld()->SpawnActor(AModuleActor::StaticClass());
			ModuleActor->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
			ModuleActor->SetActorTransform(FTransform(
				FRotator(0, 0, 0),
				originLocation,
				FVector(1, 1, 1)));
			AddedModule = AssignedModules[AssignedModules.Add(ModuleActor)];
		}
	}
	return AddedModule;
}

void AGridTile::SetupGridVisualization()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		GridVisualizationMesh = MeshAsset.Object;
	}
	GridVisualizationComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	GridVisualizationComponent->SetStaticMesh(GridVisualizationMesh);
	// Create hierarchy by setting the component as chide from RootComponent + keep childs position relative to parent 
	GridVisualizationComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling;
	TSharedPtr<FJsonObject> JsonObject = NULL;
	if(JSONManager::ReadJsonFile(Path, JsonObject))
	{
		BoxScaling = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject).VolumeScale;
	} else
	{
		BoxScaling = FVector(1.f, 1.f, 1.f);
	}
	GridVisualizationComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}
