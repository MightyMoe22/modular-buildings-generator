// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Modules/ModularBuildingVolumeData.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Templates/SharedPointer.h"
#include "Dom/JsonObject.h"
#include "SetupVolumeSize.generated.h"

UCLASS(Blueprintable)
class MODULARHOUSES_API ASetupVolumeSize : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASetupVolumeSize();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup Volume Size", DisplayName = "Volume Size")
	FVector ReferenceVolumeSize = FVector(1, 1, 1);

	// Mesh that represents the module that need to be setup
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup Volume Size", DisplayName = "Mesh Example")
	UStaticMesh* DummyModuleMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* DummyModuleMeshComponent;

	UPROPERTY(BlueprintReadWrite)
	UStaticMesh* ReferenceVolumeMesh;

	UPROPERTY(BlueprintReadWrite)
	UStaticMeshComponent* ReferenceVolumeMeshComponent;

private:

	UPROPERTY()
	FModularBuildingVolumeData VolumeData;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void SetupVolumeSize();

	UFUNCTION(BlueprintCallable)
		void LoadModuleMesh();

	UFUNCTION(BlueprintCallable)
		void SetMeshToModuleComponent(UStaticMesh* Mesh);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Setup Volume Size")
		void SaveVolumeSize();
};
