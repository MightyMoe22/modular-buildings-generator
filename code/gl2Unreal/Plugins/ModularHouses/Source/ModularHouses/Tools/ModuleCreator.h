// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Modules/ModuleData.h"
#include "Modules/ModuleContainer.h"
#include "ModuleCreator.generated.h"

UCLASS(Blueprintable)
class MODULARHOUSES_API AModuleCreator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AModuleCreator();

	// Mesh that represents the module that need to be setup
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	UStaticMesh *ModuleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	FString ModuleName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	EModuleFacing ModuleFacing = EModuleFacing::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	EModuleCategory ModuleCategory = EModuleCategory::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	EModuleMaterial ModuleMaterial = EModuleMaterial::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	EModuleEnvironment ModuleEnvironment = EModuleEnvironment::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	FRotator ModuleRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	bool ModuleIsMisc;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Creator")
	UStaticMeshComponent *ModuleMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	TMap<FString, FModuleData> NewAddedModules;

	UPROPERTY(VisibleAnywhere, Category = "Module Creator")
	TMap<FString, FModuleData> ModulesInContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator", Meta = (ClampMin = 0))
	int ModuleIDToLoadForEdit = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	FTransform ModuleTransform = FTransform();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Module Creator")
	UModuleContainer* ModuleContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module Creator")
	FModuleData ModuleDataInConfig;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Module Creator")
	bool EditMode = false;

private:
	// Mesh that represent the actor itself
	UPROPERTY()
	UStaticMesh *ReferenceVolumeMesh;

	UPROPERTY()
	UStaticMeshComponent *ReferenceVolumeMeshComponent;

	bool bModuleInConstruction = false;

	UModuleDataObject* ModuleToEdit;

protected:
	UFUNCTION()
	void SetupReferenceVolume();

	UFUNCTION()
	void LoadModuleMesh();

	UFUNCTION(BlueprintCallable, Category = "Module Creator")
	void SetMeshToModuleComponent(UStaticMesh *Mesh, bool FullReset = false);

	UFUNCTION()
	void LoadModuleContainer();

	UFUNCTION()
	bool IsModuleComplete();

	UFUNCTION()
	void ResetModuleSlot();

	UFUNCTION()
	void ResetModuleConfig();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Module Creator")
	void NewModule();

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Module Creator")
	bool SaveModule();

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Module Creator")
	void RemoveModule();

	UFUNCTION(CallInEditor, BlueprintCallable, Category = "Module Creator")
	bool LoadModule();
};
