// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModuleCreator.h"
#include "ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Misc/FileHelper.h"
#include "Containers/Array.h"
#include "JSONManager/JSONManager.h"
#include "Modules/ModularBuildingVolumeData.h"


// Sets default values
AModuleCreator::AModuleCreator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	ModuleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("ModuleComponent");
	ModuleMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SetMeshToModuleComponent(nullptr, true);

	NewAddedModules = TMap<FString, FModuleData>();
	ModuleDataInConfig = FModuleData();

	LoadModuleContainer();
	SetupReferenceVolume();
}

void AModuleCreator::SetupReferenceVolume()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/cubeWithoutFacesNE.cubeWithoutFacesNE'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		ReferenceVolumeMesh = MeshAsset.Object;
	}
	ReferenceVolumeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	ReferenceVolumeMeshComponent->SetStaticMesh(ReferenceVolumeMesh);
	// Create hierarchy by setting the component as chide from RootComponent + keep childs position relative to parent
	ReferenceVolumeMeshComponent->SetupAttachment(RootComponent);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling;
	TSharedPtr<FJsonObject> JsonObject = NULL;
	if(JSONManager::ReadJsonFile(Path, JsonObject))
	{
		BoxScaling = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject).VolumeScale;
	} else
	{
		BoxScaling = FVector(1, 1, 1);
	}

	ReferenceVolumeMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}

void AModuleCreator::LoadModuleMesh()
{
	if(ModuleMesh != nullptr)
	{
		SetMeshToModuleComponent(ModuleMesh, true);
	}
}

void AModuleCreator::SetMeshToModuleComponent(UStaticMesh *Mesh, bool FullReset)
{
	ModuleMeshComponent->SetStaticMesh(Mesh);
	if(FullReset)
	{
		ModuleTransform = FTransform(FRotator(0, 0, 0), FVector(0, 0, 0));
		ModuleMeshComponent->SetRelativeTransform(ModuleTransform);
	}
}

void AModuleCreator::LoadModuleContainer()
{
	ModuleContainer = UModuleContainer::GetInstance();
}

bool AModuleCreator::IsModuleComplete()
{
	bool isComplete = false;

	if(!ModuleName.IsEmpty() && 
	   ModuleMesh != nullptr && 
	   ModuleCategory != EModuleCategory::None && 
	   ModuleFacing != EModuleFacing::None)
	{
		isComplete = true;
	}

	return isComplete;
}

void AModuleCreator::ResetModuleSlot()
{
	ModuleMesh = nullptr;
	SetMeshToModuleComponent(nullptr, true);
}

void AModuleCreator::ResetModuleConfig()
{
	ResetModuleSlot();
	ModuleDataInConfig = FModuleData();
	ModuleToEdit = nullptr;
	EditMode = false;
	
	ModuleName = ModuleDataInConfig.Name;
	ModuleCategory = ModuleDataInConfig.Category;
	ModuleFacing = ModuleDataInConfig.Facing;
	ModuleMaterial = ModuleDataInConfig.Material;
	ModuleEnvironment = ModuleDataInConfig.Environment;
	ModuleRotation = ModuleDataInConfig.Rotation;
	ModuleIsMisc = false;
}

void AModuleCreator::NewModule()
{
	ResetModuleConfig();
}

bool AModuleCreator::SaveModule()
{
	bool saved = false;
	if(IsModuleComplete())
	{
		FString ModuleStaticMeshPath = ModuleMesh->GetPathName();

		if(EditMode)
		{
			ModuleContainer->RemoveModuleFromContainer(ModuleToEdit->ModuleData.Name);
			EditMode = false;
		}

		ModuleDataInConfig.Environment = ModuleEnvironment;
		ModuleDataInConfig.Material = ModuleMaterial;
		ModuleDataInConfig.Facing = ModuleFacing;
		ModuleDataInConfig.Category = ModuleCategory;
		ModuleDataInConfig.Name = ModuleName;
		ModuleDataInConfig.StaticMeshPath = ModuleStaticMeshPath;
		ModuleDataInConfig.Position = ModuleMeshComponent->GetRelativeTransform().GetLocation();
		ModuleDataInConfig.Rotation = ModuleRotation;
		ModuleDataInConfig.Scale = ModuleMeshComponent->GetRelativeTransform().GetScale3D();
		ModuleDataInConfig.IsMisc = ModuleIsMisc;

		ModuleContainer->AddModuleToContainer(ModuleDataInConfig);
		saved = true;
	}

	return saved;
}

void AModuleCreator::RemoveModule()
{
	if(EditMode)
	{
		ModuleContainer->RemoveModuleFromContainer(ModuleName);
		ResetModuleConfig();
	}
	else
	{
		ResetModuleConfig();
	}
}

//Used to load a already existing module into the module slot
bool AModuleCreator::LoadModule()
{
	bool loaded = false;
	if(ModuleIDToLoadForEdit < ModuleContainer->AllModuleNames.Num())
	{
		FString NameOfModuleToLoad = ModuleContainer->AllModuleNames[ModuleIDToLoadForEdit];
		ModuleToEdit = *ModuleContainer->AllModuleDataObjects.Find(NameOfModuleToLoad);

		if(ModuleToEdit != nullptr)
		{
			auto MeshAsset = LoadObject<UStaticMesh>(NULL, *ModuleToEdit->ModuleData.StaticMeshPath, NULL, LOAD_None, NULL);
			if(MeshAsset != nullptr)
			{
				ModuleMesh = MeshAsset;
				SetMeshToModuleComponent(ModuleMesh, true);
				ModuleEnvironment = ModuleToEdit->ModuleData.Environment;
				ModuleMaterial = ModuleToEdit->ModuleData.Material;
				ModuleFacing = ModuleToEdit->ModuleData.Facing;
				ModuleCategory = ModuleToEdit->ModuleData.Category;
				ModuleName = ModuleToEdit->ModuleData.Name;
				ModuleTransform = FTransform(ModuleToEdit->ModuleData.Rotation, 
											 ModuleToEdit->ModuleData.Position, 
											 ModuleToEdit->ModuleData.Scale);
				ModuleRotation = ModuleToEdit->ModuleData.Rotation;
				ModuleIsMisc = ModuleToEdit->ModuleData.IsMisc;
				ModuleMeshComponent->SetRelativeTransform(ModuleTransform);
				
				EditMode = true;
				loaded = true;
			}
		}
	}
	return loaded;
}

// Called every frame
void AModuleCreator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AModuleCreator::PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent)
{
	//Get the name of the property that was changed  
	//FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	//Get the name of the member property that was changed
	FName PropertyName = (PropertyChangedEvent.MemberProperty != nullptr) ? PropertyChangedEvent.MemberProperty->GetFName() : NAME_None;

	// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
	// in the future this will fail to compile and we can update it.  
	if(PropertyName == GET_MEMBER_NAME_CHECKED(AModuleCreator, ModuleMesh))
	{
		LoadModuleMesh();
	}
	if(PropertyName == GET_MEMBER_NAME_CHECKED(AModuleCreator, ModuleTransform))
	{
		ModuleMeshComponent->SetRelativeTransform(ModuleTransform);
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}