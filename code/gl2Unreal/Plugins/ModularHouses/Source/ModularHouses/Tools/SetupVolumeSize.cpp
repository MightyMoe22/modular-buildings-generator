// Copyright Maurice Langer und Jakob Seitz 2020

#include "SetupVolumeSize.h"
#include "ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Misc/FileHelper.h"
#include "Containers/Array.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "JSONManager/JSONManager.h"

// Sets default values
ASetupVolumeSize::ASetupVolumeSize()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	DummyModuleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("ModuleComponent");
	//DummyModuleMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
	DummyModuleMeshComponent->SetupAttachment(RootComponent);

	SetupVolumeSize();
}

void ASetupVolumeSize::SetupVolumeSize()
{
	// Load basic cube mesh from UE4
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/ModularHouses/Meshes/EmptyFrame.EmptyFrame'"));
	// Check if mesh was loaded successfully and assign mesh to ReferenceVolumeMesh
	if(MeshAsset.Object != nullptr)
	{
		ReferenceVolumeMesh = MeshAsset.Object;
	}
	ReferenceVolumeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Volume");
	// Set static mesh to component 
	ReferenceVolumeMeshComponent->SetStaticMesh(ReferenceVolumeMesh);
	// Create hierarchy by setting the component as childe from RootComponent + keep childs possiton relative to parent 
	//ReferenceVolumeMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
	ReferenceVolumeMeshComponent->SetupAttachment(RootComponent);

	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling;
	TSharedPtr<FJsonObject> JsonObject = NULL;
	if(JSONManager::ReadJsonFile(Path, JsonObject))
	{
		VolumeData = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject);

		BoxScaling = VolumeData.VolumeScale;
	} else
	{
		BoxScaling = FVector(1, 1, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f"), BoxScaling.X, BoxScaling.Y, BoxScaling.Z);
	ReferenceVolumeMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), BoxScaling));
}


void ASetupVolumeSize::LoadModuleMesh()
{
	if(DummyModuleMesh != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("PingPong2"));
		SetMeshToModuleComponent(DummyModuleMesh);
	}
}

void ASetupVolumeSize::SetMeshToModuleComponent(UStaticMesh* Mesh)
{
	UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f"), this->GetTransform().GetScale3D().X, this->GetTransform().GetScale3D().Y, this->GetTransform().GetScale3D().Z);
	DummyModuleMeshComponent->SetStaticMesh(Mesh);
	DummyModuleMeshComponent->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 0, 0)));
}

void ASetupVolumeSize::SaveVolumeSize()
{
	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	VolumeData.VolumeScale = FVector(ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().X,
									 ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().Y,
									 ReferenceVolumeMeshComponent->GetRelativeTransform().GetScale3D().Z);

	JSONManager::WriteJsonFile(Path, FModularBuildingVolumeData::ModularBuildingVolumeDataToJson(VolumeData));
}

// Called when the game starts or when spawned
void ASetupVolumeSize::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASetupVolumeSize::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASetupVolumeSize::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.MemberProperty->GetFName() : NAME_None;

	// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
	// in the future this will fail to compile and we can update it.  
	if(PropertyName == GET_MEMBER_NAME_CHECKED(ASetupVolumeSize, DummyModuleMesh))
	{
		LoadModuleMesh();
	}
	if(PropertyName == GET_MEMBER_NAME_CHECKED(ASetupVolumeSize, ReferenceVolumeSize))
	{
		ReferenceVolumeMeshComponent->SetRelativeScale3D(ReferenceVolumeSize);
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}

