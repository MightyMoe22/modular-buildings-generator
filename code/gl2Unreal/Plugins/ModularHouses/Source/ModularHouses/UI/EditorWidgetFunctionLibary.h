// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Editor/Blutility/Classes/EditorUtilityWidget.h"
#include "Editor/Blutility/Classes/EditorUtilityWidgetBlueprint.h"
#include "Editor/Blutility/Public/IBlutilityModule.h"
#include "Runtime/Core/Public/Modules/ModuleManager.h"
#include "Editor/LevelEditor/Public/LevelEditor.h"
#include "Editor/UMGEditor/Public/WidgetBlueprint.h"
#include "Kismet/BlueprintFunctionLibrary.h"


#include "EditorWidgetFunctionLibary.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class MODULARHOUSES_API UEditorWidgetFunctionLibary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static void StartWidget(UWidgetBlueprint* Blueprint);
};
