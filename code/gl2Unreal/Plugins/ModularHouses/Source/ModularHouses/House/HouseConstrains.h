// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HouseConstrains.generated.h"


USTRUCT()
struct FFloorConstrain
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	int32 testValue;

	FFloorConstrain() 
	{
		testValue = 4;
	}
};

USTRUCT()
struct FRoomConstrain
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	int32 testValue;

	FRoomConstrain()
	{
		testValue = 4;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODULARHOUSES_API UHouseConstrains : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHouseConstrains();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, Category = "Constrains")
	FFloorConstrain floorConstrain;

	UPROPERTY(EditAnywhere, Category = "Constrains")
	FRoomConstrain roomConstrain;
};
