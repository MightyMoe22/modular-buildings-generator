// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "HouseVolume.h"
#include "DrawDebugHelpers.h"

//Generator
#include "Generator/GroundGenerator.h"
#include "Generator/FloorGenerator.h"

//ModulePicker
#include "Modules/ModulePicker.h"
#include "HouseProperty.generated.h"



UCLASS(Blueprintable, ClassGroup = (House), meta = (BlueprintSpawnableComponent))
class MODULARHOUSES_API AHouseProperty : public AActor
{
	GENERATED_BODY()
	
public:	
	AHouseProperty();

	UPROPERTY()
	int NextFloor = 0;

	UPROPERTY(VisibleAnywhere, AdvancedDisplay)
	FHouseVolume HouseVolume;

	// House dimension
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "House Dimension", Meta = (ClampMin = "1", ClampMax = "50"))
	FIntVector VolumeDimensions = {20, 15, 10};

	UPROPERTY()
	FIntVector OldVolumeDimensions = {0, 0, 0};

	UPROPERTY(BlueprintReadWrite)
	TScriptInterface<IGroundGeneratorInterface> generator;

	UPROPERTY(BlueprintReadWrite)
	UFloorManager* floorManager;

	UPROPERTY(BlueprintReadWrite)
	UFloorGenerator* floorGenerator;

	UPROPERTY(BlueprintReadWrite)
	UGroundGeneratorData* groundGeneratorData;

	UPROPERTY(BlueprintReadWrite)
	EModuleMaterial innerMaterial = EModuleMaterial::MaterialSlot1;

	UPROPERTY(BlueprintReadWrite)
	EModuleMaterial outerMaterial = EModuleMaterial::MaterialSlot1;
private:
	
	UFUNCTION(BlueprintCallable)
	void drawDebugCube();
	
	UFUNCTION(BlueprintCallable)
	void updateVolume();

	UFUNCTION(BlueprintCallable)
	void FillVolume();

	UFUNCTION(BlueprintCallable)
	UGridTileComponent* createGridTileAtLocation(FIntVector tilelocation);
	
	UFUNCTION(BlueprintCallable)
	void ZeroVolume();

	UFUNCTION(BlueprintCallable)
	void RemoveOldVolume();

	UFUNCTION(BlueprintCallable)
	void completeHouseGeneration();

public:
	UFUNCTION(BlueprintCallable)
	void FinishSetup();

	UFUNCTION(BlueprintCallable)
	void NextStep();

	UFUNCTION()
	void ChangeVolumeVisibility(int VisibilityLevel);
};
