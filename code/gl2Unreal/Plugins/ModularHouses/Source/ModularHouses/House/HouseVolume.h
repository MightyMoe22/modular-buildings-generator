// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "ObjectMacros.h"
#include "HouseVolume.generated.h"

USTRUCT(Blueprintable, meta = (UsesHierarchy))
struct MODULARHOUSES_API FHouseVolume
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FIntVector UObjectMatrixDimensions;

	FORCEINLINE void SetUObjectToMatrix(UChildActorComponent* NewObject, FIntVector Location)
	{
		//Location.Z * UObjectMatrixDimensions.X * UObjectMatrixDimensions.Y    walks us one XY block at a time
		//Location.Y * UObjectMatrixDimensions.X Then walks us to the correct line in the XY block we want
		//And then we just walk Location.X forward to the element we want.
		UObjectMatrix[Location.Z * UObjectMatrixDimensions.X * UObjectMatrixDimensions.Y + Location.Y * UObjectMatrixDimensions.X + Location.X] = NewObject;
	}

	FORCEINLINE UChildActorComponent* GetUObjectFromMatrix(FIntVector Location)
	{
		return UObjectMatrix[Location.Z * UObjectMatrixDimensions.X * UObjectMatrixDimensions.Y + Location.Y * UObjectMatrixDimensions.X + Location.X];
	}

	FORCEINLINE void SetNumZeroed()
	{
		UObjectMatrix.SetNumZeroed(UObjectMatrixDimensions.X * UObjectMatrixDimensions.Y * UObjectMatrixDimensions.Z);
	}

	FORCEINLINE void Reset(FIntVector NewDimensions)
	{
		UObjectMatrix.Empty();
		UObjectMatrixDimensions = NewDimensions;
	}

private:
	//The matrix is laid out sequentially in memory,
	UPROPERTY()
	TArray<UChildActorComponent*> UObjectMatrix;
};
