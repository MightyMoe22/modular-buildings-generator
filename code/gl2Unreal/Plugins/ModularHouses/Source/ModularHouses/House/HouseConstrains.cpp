// Copyright Maurice Langer und Jakob Seitz 2020


#include "HouseConstrains.h"

// Sets default values for this component's properties
UHouseConstrains::UHouseConstrains()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UHouseConstrains::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UHouseConstrains::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}