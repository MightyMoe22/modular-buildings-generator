// Copyright Maurice Langer und Jakob Seitz 2020

#include "HouseProperty.h"
#include "Grid/GridTileComponent.h"
#include "Engine/World.h"
#include "ComponentReregisterContext.h"
#include "JSONManager/JSONManager.h"
#include "Modules/ModularBuildingVolumeData.h"
#include "Generator/GroundGenerator.h"
#include "Modules/ModuleContainer.h"

// Sets default values
AHouseProperty::AHouseProperty()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create Root Component
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");


	generator = NewObject<UGroundGenerator>();
	floorManager = NewObject<UFloorManager>();
	floorGenerator = NewObject<UFloorGenerator>();
	floorGenerator->generator = generator;
	floorGenerator->floorManager = floorManager;
	groundGeneratorData = generator->groundGeneratorData;
}

void AHouseProperty::ZeroVolume()
{
	HouseVolume.SetNumZeroed();
}

void AHouseProperty::RemoveOldVolume()
{
	FIntVector TileLocation;
	for(int32 x = 0; x < OldVolumeDimensions.X; x++)
	{
		for(int32 y = 0; y < OldVolumeDimensions.Y; y++)
		{
			for(int32 z = 0; z < OldVolumeDimensions.Z; z++)
			{
				TileLocation = FIntVector(x, y, z);
				if(HouseVolume.GetUObjectFromMatrix(TileLocation) != nullptr)
				{
					HouseVolume.GetUObjectFromMatrix(TileLocation)->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
					HouseVolume.GetUObjectFromMatrix(TileLocation)->DestroyComponent();
				}
			}
		}
	}
	OldVolumeDimensions = VolumeDimensions;
}

void AHouseProperty::completeHouseGeneration()
{
	if(NextFloor == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("You have to finish setup first!"));
		return;
	}
	for(int i = NextFloor; i <= VolumeDimensions.Z; i++)
	{
		NextStep();
	}
}

void AHouseProperty::updateVolume()
{
	RemoveOldVolume();
	HouseVolume.Reset(VolumeDimensions);
	ZeroVolume();
	FillVolume();
}

void AHouseProperty::drawDebugCube()
{
	int X = VolumeDimensions.X;
	int Y = VolumeDimensions.Y;
	int Z = VolumeDimensions.Z;

	FVector offset = this->GetActorTransform().GetLocation();
	FQuat rotation = this->GetActorTransform().GetRotation();
	FVector Size = FVector(X, Y, Z) * 100 / 2;
	FVector localCenter = rotation.RotateVector(Size);
	FVector center = offset + localCenter;
	FColor aqua = FColor(5, 125, 111);
	DrawDebugBox(GetWorld(), center, Size, rotation, aqua, false, 30000, 0, 10);
}

void AHouseProperty::FillVolume()
{
	if(GetWorld() == nullptr)
		return;

	drawDebugCube();
}


UGridTileComponent* AHouseProperty::createGridTileAtLocation(FIntVector tileLocation)
{
	TSharedPtr<FJsonObject> JsonObject = NULL;
	FString Path = TEXT("/ModularHouses/Content/JSON/BuildingBlockMETA.json");
	FVector BoxScaling = {1, 1, 1};
	FVector startLocationTransform = {50, 50, 50};
	UGridTileComponent* newTile = NewObject<UGridTileComponent>(this);

	if(JSONManager::ReadJsonFile(Path, JsonObject))
		BoxScaling = FModularBuildingVolumeData::JsonToModularBuildingVolumeData(JsonObject).VolumeScale;

	newTile->RegisterComponent();
	newTile->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	newTile->SetRelativeTransform(FTransform(FRotator(0, 0, 0),
								  FVector((startLocationTransform.X + tileLocation.X * 100) * BoxScaling.X,
								  (startLocationTransform.Y + tileLocation.Y * 100) * BoxScaling.Y,
								  (startLocationTransform.Z + tileLocation.Z * 100) * BoxScaling.Z),
								  FVector(1, 1, 1)));
	newTile->SetMobility(EComponentMobility::Static);
	HouseVolume.SetUObjectToMatrix(newTile, tileLocation);
	return newTile;
}

void AHouseProperty::FinishSetup()
{
	RemoveOldVolume();
	NextFloor = 1;
	floorGenerator->GenerateFloors(VolumeDimensions);
	NextStep(); //FirstStep
}

void AHouseProperty::NextStep()
{
	if(NextFloor == VolumeDimensions.Z + 1)
	{
		UE_LOG(LogTemp, Error, TEXT("No Next Floor. Number of Floors: %i"), VolumeDimensions.Z);
		return;
	}
	if(NextFloor == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("You have to finish setup first!"));
		return;
	}
	ChangeVolumeVisibility(NextFloor);
	NextFloor++;
}

void AHouseProperty::ChangeVolumeVisibility(int VisibilityLevel)
{
	FIntVector TileLocation;
	TileLocation.Z = VisibilityLevel - 1;
	UGroundGrid* grid = floorManager->getFloor(VisibilityLevel - 1);
	auto gridArray = grid->getGroundGrid();
	if(gridArray == nullptr || gridArray == NULL)
		return;
	UModulePicker* modulePicker = NewObject<UModulePicker>(this);

	modulePicker->SetupModulePicker(outerMaterial, innerMaterial, gridArray);

	if(VisibilityLevel <= VolumeDimensions.Z)
	{
		for(int32 x = 0; x < VolumeDimensions.X; ++x)
			for(int32 y = 0; y < VolumeDimensions.Y; ++y)
			{
				TileLocation.X = x;
				TileLocation.Y = y;

				if(HouseVolume.GetUObjectFromMatrix(TileLocation) != nullptr)
				{
					HouseVolume.GetUObjectFromMatrix(TileLocation)->DestroyComponent();
				}

				std::list<FGridTileStruct> GridTileList = gridArray[x][y];
				if(GridTileList.empty()) continue;

				TMap<FGridTileStruct, UModuleDataObject*> modulesForTile = modulePicker->PickModulesAt(x, y);
				if(modulesForTile.Num() == 0) continue;

				UGridTileComponent* newGridTile = createGridTileAtLocation(TileLocation);
				for(TMap<FGridTileStruct, UModuleDataObject*>::TIterator mdo = modulesForTile.CreateIterator(); mdo; ++mdo)
				{
					newGridTile->LoadModuleIntoGridTile(mdo.Value()->ModuleData, mdo.Key());
				}
			}
	}
}
