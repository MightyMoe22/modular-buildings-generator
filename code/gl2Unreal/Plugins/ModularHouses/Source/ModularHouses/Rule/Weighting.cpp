// Copyright Maurice Langer und Jakob Seitz 2020

#include "Weighting.h"

void UWeighting::SetWeight(float value)
{
	if(value >= 0.0f && value <= 100.0f)
	{
		weight = value;
	}
}

float UWeighting::GetWeight()
{
	return weight;
}
