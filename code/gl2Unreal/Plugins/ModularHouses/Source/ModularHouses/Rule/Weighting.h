// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "EngineMinimal.h"
#include "Weighting.generated.h"

UCLASS(BlueprintType)
class MODULARHOUSES_API UWeighting : public UObject
{
	GENERATED_BODY()

private:

	float weight;

public:

	UFUNCTION(BlueprintCallable, Category = "Weighting")
	void SetWeight(float value);

	UFUNCTION(BlueprintCallable, Category = "Weighting")
	float GetWeight();
	
};