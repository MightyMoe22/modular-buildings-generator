// Copyright Maurice Langer und Jakob Seitz 2020

#include "Rule.h"

void URule::SetupRule(float value)
{
	Weighting = NewObject<UWeighting>();
	Weighting->SetWeight(value);
}
