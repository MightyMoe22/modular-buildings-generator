// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "EngineMinimal.h"
#include "Rule.h"
#include "Generator/Direction.h"
#include "HouseRule.generated.h"

UCLASS(BlueprintType)
class MODULARHOUSES_API UHouseRule : public URule
{
	GENERATED_BODY()

public:

	UHouseRule();
	
	//If Specific, this rule describes a specific module not a module category.
	UPROPERTY(BlueprintReadOnly, Category = "House Rule")
	bool bSpecific;

	UPROPERTY(BlueprintReadOnly, Category = "House Rule")
	EModuleCategory moduleCategory;

	UPROPERTY(BlueprintReadOnly, Category = "House Rule")
	UModuleDataObject* moduleDataObject;

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	bool SetupHouseRuleWithMDO(UModuleDataObject* mDataObject, float value);

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	bool SetupHouseRuleWithMC(EModuleCategory mCategory, float value);

};