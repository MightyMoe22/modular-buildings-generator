// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModuleRule.h"
#include "JSONManager\JSONManager.h"
#include <Runtime\JsonUtilities\Public\JsonDomBuilder.h>

const FString WEIGHTING = "Weighting";
const FString ESSENTIAL = "Essential";
const FString AFFECTEDM = "AffectedModule";
const FString RULEVALIDITY = "RuleValidity";

UModuleRule::UModuleRule()
{
	bSetupNeeded = true;
	LinkedRule = nullptr;
}

bool UModuleRule::SetupModuleRule(FString NameOfAffectModule,
								  float weightValue,
								  TArray<EDirection> directions,
								  bool essential = false)
{
	bool worked = bSetupNeeded;
	if(bSetupNeeded)
	{
		AffectedModuleName = NameOfAffectModule;
		SetupRule(weightValue);
		RuleValidity = directions;
		bEssential = essential;
		bSetupNeeded = false;
	}
	return worked;
}

void UModuleRule::SetParentModuleDataObject(UModuleDataObject* moduleDataObject)
{
	ParentModuleDataObject = moduleDataObject;
}

void UModuleRule::SetLinkedRule(UModuleRule* rule)
{
	LinkedRule = rule;
}

UModuleRule* UModuleRule::CreateReverseRule(UModuleRule* RuleToReverse, UModuleDataObject* moduleDataObject)
{
	UModuleRule* reverseRule = NewObject<UModuleRule>();
	reverseRule->SetupModuleRule(RuleToReverse->ParentModuleDataObject->ModuleData.Name,
								 RuleToReverse->Weighting->GetWeight(),
								 RuleToReverse->GetReverseDirections(),
								 RuleToReverse->bEssential);
	reverseRule->SetLinkedRule(RuleToReverse);
	reverseRule->SetParentModuleDataObject(moduleDataObject);

	return reverseRule;
}

TArray<EDirection> UModuleRule::GetReverseDirections()
{
	TArray<EDirection> reverseDirections;
	for(EDirection direction : RuleValidity)
	{
		switch(direction)
		{
			case EDirection::North:
				reverseDirections.Add(EDirection::South);
				break;
			case EDirection::East:
				reverseDirections.Add(EDirection::West);
				break;
			case EDirection::South:
				reverseDirections.Add(EDirection::North);
				break;
			case EDirection::West:
				reverseDirections.Add(EDirection::East);
				break;
			case EDirection::Up:
				reverseDirections.Add(EDirection::Down);
				break;
			case EDirection::Down:
				reverseDirections.Add(EDirection::Up);
				break;
			case EDirection::Here:
				reverseDirections.Add(EDirection::Here);
				break;
			default:
				break;
		}
	}
	return reverseDirections;
}

UModuleRule* UModuleRule::JsonToModuleRule(TSharedPtr<FJsonObject> JsonObject)
{
	UModuleRule* ModuleRules = NewObject<UModuleRule>();

	ModuleRules->SetupModuleRule(JsonObject->GetStringField(AFFECTEDM),
								 JsonObject->GetNumberField(WEIGHTING),
								 UModuleRule::ConvertToRuleValidity(JsonObject->GetArrayField(RULEVALIDITY)),
								 JsonObject->GetBoolField(ESSENTIAL));
	return ModuleRules;
}

TSharedPtr<FJsonObject> UModuleRule::ModuleRuleToJson(UModuleRule* ModuleRule)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	// Fill JsonObject with ModuleData
	JsonObject->SetNumberField(WEIGHTING, ModuleRule->Weighting->GetWeight());
	JsonObject->SetBoolField(ESSENTIAL, ModuleRule->bEssential);
	JsonObject->SetStringField(AFFECTEDM, ModuleRule->AffectedModuleName);
	JsonObject->SetArrayField(RULEVALIDITY, ModuleRule->RuleValidityAsUint8());

	return JsonObject;
}

TArray<EDirection> UModuleRule::ConvertToRuleValidity(TArray<TSharedPtr<FJsonValue>> IDs)
{
	TArray<EDirection> ruleValidity;

	for(int i = 0; i < IDs.Num(); ++i)
	{
		uint8 value = IDs[i]->AsNumber();
		ruleValidity.Add((EDirection) value);
	}

	return ruleValidity;
}

TArray<TSharedPtr<FJsonValue>> UModuleRule::RuleValidityAsUint8()
{
	FJsonDomBuilder::FArray OutputArray;

	for(EDirection dir : RuleValidity)
	{
		OutputArray.Add((uint8) dir);
	}

	return OutputArray.AsJsonValue()->AsArray();
}
