// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "EngineMinimal.h"
#include "Rule.h"
#include "Generator/Direction.h"
#include "Modules/ModuleDataObject.h"
#include "ModuleRule.generated.h"

UCLASS(BlueprintType)
class MODULARHOUSES_API UModuleRule : public URule
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "Module Rule")
	bool bEssential;

	UPROPERTY(BlueprintReadOnly, Category = "Module Rule")
	FString AffectedModuleName;

	UPROPERTY(BlueprintReadOnly, Category = "Module Rule")
	UModuleDataObject* ParentModuleDataObject;

	UPROPERTY(BlueprintReadOnly, Category = "Module Rule")
	UModuleRule* LinkedRule;

	UPROPERTY(BlueprintReadOnly, Category = "Module Rule")
	TArray<EDirection> RuleValidity;

private:

	bool bSetupNeeded;

public:

	UModuleRule();

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	bool SetupModuleRule(FString NameOfAffectModule, float weightValue, TArray<EDirection> directions, bool essential);

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	void SetParentModuleDataObject(UModuleDataObject* moduleDataObject);

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	void SetLinkedRule(UModuleRule* rule);

	UFUNCTION(BlueprintCallable, Category = "Module Rule")
	static UModuleRule* CreateReverseRule(UModuleRule* RuleToReverse, UModuleDataObject* moduleDataObject);

	static UModuleRule* JsonToModuleRule(TSharedPtr<FJsonObject> JsonObject);
	static TSharedPtr<FJsonObject> ModuleRuleToJson(UModuleRule* ModuleRule);

private:

	static TArray<EDirection> ConvertToRuleValidity(TArray<TSharedPtr<FJsonValue>> IDs);
	TArray<TSharedPtr<FJsonValue>> RuleValidityAsUint8();
	TArray<EDirection> GetReverseDirections();

};