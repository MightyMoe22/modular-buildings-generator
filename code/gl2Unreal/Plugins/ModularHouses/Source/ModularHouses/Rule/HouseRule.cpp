// Copyright Maurice Langer und Jakob Seitz 2020

#include "HouseRule.h"

UHouseRule::UHouseRule()
{
	bSpecific = false;
}

bool UHouseRule::SetupHouseRuleWithMDO(UModuleDataObject* mDataObject, float value)
{
	bSpecific = true;
	moduleDataObject = mDataObject;
	moduleCategory = EModuleCategory::None;
	SetupRule(value);

	return false;
}

bool UHouseRule::SetupHouseRuleWithMC(EModuleCategory mCategory, float value)
{
	moduleDataObject = nullptr;
	moduleCategory = mCategory;
	SetupRule(value);

	return false;
}

