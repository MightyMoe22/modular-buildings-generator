// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "EngineMinimal.h"
#include "Weighting.h"
#include "Modules/ModuleDataObject.h"
#include "Rule.generated.h"

UCLASS(BlueprintType)
class MODULARHOUSES_API URule : public UObject
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Rule")
	UWeighting* Weighting;

public:

	UFUNCTION(BlueprintCallable, Category = "Rule")
	void SetupRule(float value);

};
