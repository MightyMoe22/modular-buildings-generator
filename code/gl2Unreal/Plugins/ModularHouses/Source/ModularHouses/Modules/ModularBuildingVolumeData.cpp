// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModularBuildingVolumeData.h"
#include "JSONManager/JSONManager.h"

const FString VOLUME = "VolumeSize";

FModularBuildingVolumeData FModularBuildingVolumeData::BuildModularBuildingVolumeData(FVector VolumeSize)
{
	FModularBuildingVolumeData ModularBuildingVolumeData;

	ModularBuildingVolumeData.VolumeScale = VolumeSize;

	return ModularBuildingVolumeData;
}

FModularBuildingVolumeData FModularBuildingVolumeData::JsonToModularBuildingVolumeData(TSharedPtr<FJsonObject> JsonObject)
{
	FModularBuildingVolumeData ModularBuildingVolumeData;

	ModularBuildingVolumeData.VolumeScale = JSONManager::ConvertArrayToVector(JsonObject->GetArrayField(VOLUME));

	return ModularBuildingVolumeData;
}

TSharedPtr<FJsonObject> FModularBuildingVolumeData::ModularBuildingVolumeDataToJson(FModularBuildingVolumeData ModularBuildingVolumeData)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	const TArray<TSharedPtr<FJsonValue>> VolumeScale = JSONManager::ConvertVectorToArray(ModularBuildingVolumeData.VolumeScale);

	//Fill JsonObject with VolumeData
	JsonObject->SetArrayField(VOLUME, VolumeScale);

	return JsonObject;
}
