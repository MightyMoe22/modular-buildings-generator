// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Modules/ModuleData.h"
#include "ModuleActor.generated.h"

UENUM(BlueprintType)
enum class ModuleOrientation : uint8
{
	MONormal UMETA(DisplayName = "Normal"),
	MOLeft UMETA(DisplayName = "Left"),
	MORight UMETA(DisplayName = "Right"),
	MOBackwards UMETA(DisplayName = "Backwards")
};


UCLASS(BlueprintType, meta = (BlueprintSpawnableComponent))
class MODULARHOUSES_API AModuleActor : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, Category = "Module Actor")
	FModuleData ModuleData;

public:
	// Sets default values for this component's properties
	AModuleActor();

	UFUNCTION(BlueprintCallable)
	bool LoadModuleData(FModuleData ModuleDataToLoad);

};