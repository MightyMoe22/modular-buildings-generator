// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModularBuildingComponentData.h"

FModularBuildingComponentData FModularBuildingComponentData::BuildModularBuildingComponentData(FString ModuleStaticMeshPath,
																							   FVector ModulePosition,
																							   FRotator ModuleRotation)
{
	FModularBuildingComponentData ModularBuildingComponentData;

	ModularBuildingComponentData.ModuleStaticMeshPath = ModuleStaticMeshPath;
	ModularBuildingComponentData.ModulePosition = ModulePosition;
	ModularBuildingComponentData.ModuleRotation = ModuleRotation;

	return ModularBuildingComponentData;
}

FModularBuildingComponentData FModularBuildingComponentData::JsonToModularBuildingComponentData(TSharedPtr<FJsonObject> JsonObject)
{
	FModularBuildingComponentData ModularBuildingComponentData;

	ModularBuildingComponentData.ModuleStaticMeshPath = JsonObject->GetStringField(TEXT("ModuleStaticMeshPath"));
	ModularBuildingComponentData.ModulePosition.X = JsonObject->GetNumberField(TEXT("ModuleRelativePosition_X"));
	ModularBuildingComponentData.ModulePosition.Y = JsonObject->GetNumberField(TEXT("ModuleRelativePosition_Y"));
	ModularBuildingComponentData.ModulePosition.Z = JsonObject->GetNumberField(TEXT("ModuleRelativePosition_Z"));
	float Rot_X = JsonObject->GetNumberField(TEXT("Rot_X"));
	float Rot_Y = JsonObject->GetNumberField(TEXT("Rot_Y"));
	float Rot_Z = JsonObject->GetNumberField(TEXT("Rot_Z"));
	ModularBuildingComponentData.ModuleRotation = FVector(Rot_X, Rot_Z, Rot_Y).ToOrientationQuat().Rotator();

	return ModularBuildingComponentData;
}

TSharedPtr<FJsonObject> FModularBuildingComponentData::ModularBuildingComponentDataToJson(FModularBuildingComponentData ModularBuildingComponentData)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	// Fill JsonObject with ModuleData
	JsonObject->SetStringField("ModuleStaticMeshPath", ModularBuildingComponentData.ModuleStaticMeshPath);
	JsonObject->SetNumberField("ModuleRelativePosition_X", ModularBuildingComponentData.ModulePosition.X);
	JsonObject->SetNumberField("ModuleRelativePosition_Y", ModularBuildingComponentData.ModulePosition.Y);
	JsonObject->SetNumberField("ModuleRelativePosition_Z", ModularBuildingComponentData.ModulePosition.Z);
	JsonObject->SetNumberField("Rot_X", ModularBuildingComponentData.ModuleRotation.Euler().X);
	JsonObject->SetNumberField("Rot_Y", ModularBuildingComponentData.ModuleRotation.Euler().Y);
	JsonObject->SetNumberField("Rot_Z", ModularBuildingComponentData.ModuleRotation.Euler().Z);

	return JsonObject;
}
