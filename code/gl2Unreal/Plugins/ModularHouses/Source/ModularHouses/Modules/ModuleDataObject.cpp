// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModuleDataObject.h"
#include "Rule/ModuleRule.h"

UModuleDataObject::UModuleDataObject()
{
}

void UModuleDataObject::StoreModuleData(FModuleData DataToStore)
{
	ModuleData = DataToStore;
}

void UModuleDataObject::AddModuleRule(UModuleRule* ModuleRule)
{
	ModuleRules.Add(ModuleRule->AffectedModuleName, ModuleRule);
}

void UModuleDataObject::RemoveModuleRule(UModuleRule* ModuleRule)
{
	ModuleRules.Remove(ModuleRule->AffectedModuleName);
}
