// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModuleContainer.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "Misc/FileHelper.h"
#include "JSONManager/JSONManager.h"
#include "JsonDomBuilder.h"
#include "Engine.h"

static UModuleContainer* ModuleContainerInstance = nullptr;

/** Get the instance of UModuleContainers, this implementation defines UModuleContainer as Singleton.
* @returns instance of UModuleContainer
*/
UModuleContainer* UModuleContainer::GetInstance()
{
	if(ModuleContainerInstance == nullptr)
	{
		ModuleContainerInstance = NewObject<UModuleContainer>();
		FString Path = TEXT(MODULE_CONTAINER_PATH);
		TSharedPtr<FJsonObject> JsonObject = NULL;
		if(JSONManager::ReadJsonFile(Path, JsonObject))
		{
			JsonToModuleContainer(JsonObject);
		}
	}
	return ModuleContainerInstance;
}

UModuleContainer::UModuleContainer()
{
}

void UModuleContainer::LinkRules()
{
	for(UModuleRule* rule : unlinkedRules)
	{
		if(rule->LinkedRule == nullptr)
		{
			UModuleDataObject* affectedModule = *AllModuleDataObjects.Find(rule->AffectedModuleName);
			UModuleRule* RuleToLink = *affectedModule->ModuleRules.Find(rule->ParentModuleDataObject->ModuleData.Name);
			rule->LinkedRule = RuleToLink;
			RuleToLink->LinkedRule = rule;
		}
	}
	unlinkedRules.Empty();
}

/**
 * Adds module to container if name is unique.
 *
 * @param NewModule: the module that should be added
 * @returns true = module successful added, false = module could not be added because of name duplication
 */
bool UModuleContainer::AddModuleToContainer(FModuleData NewModule)
{
	bool allowedToAdd = true;
	for(FString& mNames : AllModuleNames)
	{
		if(NewModule.Name.Equals(mNames))
		{
			allowedToAdd = false;
			break;
		}
	}
	if(allowedToAdd)
	{
		AllModuleNames.AddUnique(NewModule.Name);
		UModuleDataObject* tempModuleDataObject = NewObject<UModuleDataObject>();
		tempModuleDataObject->StoreModuleData(NewModule);
		AllModuleDataObjects.Add(NewModule.Name, tempModuleDataObject);
		ModuleToOrderedModules(tempModuleDataObject);
		SaveContainer();
	}

	return allowedToAdd;
}

/**
 * Return a list of modules that are from a given category with a given facing.
 *
 * @param Category: the category of the modules you want to get
 * @param Facing: the facing of the modules you want to get
 * @returns A list of modules with the given constrains OR nullptr if no modules where found with the given constrains
 */
TArray<FModuleData> UModuleContainer::GetModules(EModuleCategory Category, EModuleFacing Facing)
{
	TArray<FModuleData> ReturnList;
	if(SortedModules.Find(Category) != nullptr && SortedModules.Find(Category)->Find(Facing) != nullptr)
	{
		ReturnList = *SortedModules.Find(Category)->Find(Facing);
	}
	return ReturnList;
}

void UModuleContainer::ModuleToSortedModules(const UModuleDataObject* ModuleToSort)
{
	//Get pointer to CategoryMap of the given module category
	TMap<EModuleFacing, TArray<FModuleData>>* CategoryMap = SortedModules.Find(ModuleToSort->ModuleData.Category);
	//If CategoryMap doesn't exist, create new empty map
	if(CategoryMap == nullptr)
	{
		TMap<EModuleFacing, TArray<FModuleData>> newCategoryMap;
		CategoryMap = &SortedModules.Add(ModuleToSort->ModuleData.Category, newCategoryMap);
	}

	//Get pointer to ModuleList sorted by facing
	TArray<FModuleData>* ModuleList = CategoryMap->Find(ModuleToSort->ModuleData.Facing);
	//If ModuleList doesn't exist, create new empty list
	if(ModuleList == nullptr)
	{
		TArray<FModuleData> newModuleList;
		ModuleList = &CategoryMap->Add(ModuleToSort->ModuleData.Facing, newModuleList);
	}

	ModuleList->Add(ModuleToSort->ModuleData);
}

void UModuleContainer::ModuleToOrderedModules(UModuleDataObject* ModuleDataObject)
{
	//Get pointer to materialMap corresponding to given material of ModuleDataObject
	TMap<EModuleCategory, TMap<EModuleFacing, TArray<UModuleDataObject*>>>* materialMap =
		OrderedModules.Find(ModuleDataObject->ModuleData.Material);
	//If MaterialMap doesn't exist, create new empty map
	if(materialMap == nullptr)
	{
		TMap<EModuleCategory, TMap<EModuleFacing, TArray<UModuleDataObject*>>> newMaterialMap;
		materialMap = &OrderedModules.Add(ModuleDataObject->ModuleData.Material, newMaterialMap);
	}

	//Get pointer to categoryMap corresponding to given category of ModuleDataObject
	TMap<EModuleFacing, TArray<UModuleDataObject*>>* categoryMap = materialMap->Find(ModuleDataObject->ModuleData.Category);
	//If categoryMap doesn't exist, create new empty map
	if(categoryMap == nullptr)
	{
		TMap<EModuleFacing, TArray<UModuleDataObject*>> newCategoryMap;
		categoryMap = &materialMap->Add(ModuleDataObject->ModuleData.Category, newCategoryMap);
	}

	//Get pointer to moduleArray corresponding to given facing of ModuleDataObject
	TArray<UModuleDataObject*>* moduleArray = categoryMap->Find(ModuleDataObject->ModuleData.Facing);
	//If moduleArray doesn't exist, create new empty array
	if(moduleArray == nullptr)
	{
		TArray<UModuleDataObject*> newModuleArray;
		moduleArray = &categoryMap->Add(ModuleDataObject->ModuleData.Facing, newModuleArray);
	}

	//Add ModuleDataObject to array
	moduleArray->Add(ModuleDataObject);
}

bool UModuleContainer::RemoveModuleFromContainer(FString ModuleName)
{
	bool moduleDeleted = false;
	if(AllModuleNames.FindByKey(ModuleName) != nullptr)
	{
		AllModuleNames.Remove(ModuleName);
		UModuleDataObject* tempMDO = *AllModuleDataObjects.Find(ModuleName);
		RemoveModuleFromOrderedModules(tempMDO);
		AllModuleDataObjects.Remove(ModuleName);
		SaveContainer();
		moduleDeleted = true;
	}

	return moduleDeleted;
}

void UModuleContainer::RemoveModuleFromOrderedModules(UModuleDataObject* ModuleDataObject)
{
	TArray<UModuleDataObject*>* tempArray = GetModulesByOptions(ModuleDataObject->ModuleData.Material,
																ModuleDataObject->ModuleData.Category,
																ModuleDataObject->ModuleData.Facing);
	if(tempArray->Num() > 0)
	{
		tempArray->Remove(ModuleDataObject);
	}
}

TArray<UModuleDataObject*>* UModuleContainer::GetModulesByOptions(EModuleMaterial Material, EModuleCategory Category, EModuleFacing Facing)
{
	//Get pointer to materialMap corresponding to given material of ModuleDataObject
	TMap<EModuleCategory, TMap<EModuleFacing, TArray<UModuleDataObject*>>>* materialMap =
		OrderedModules.Find(Material);
	if(materialMap != nullptr)
	{
		//Get pointer to categoryMap corresponding to given category of ModuleDataObject
		TMap<EModuleFacing, TArray<UModuleDataObject*>>* categoryMap = materialMap->Find(Category);
		if(categoryMap != nullptr)
		{
			//Get pointer to moduleArray corresponding to given facing of ModuleDataObject
			TArray<UModuleDataObject*>* moduleArray = categoryMap->Find(Facing);
			if(moduleArray != nullptr)
			{
				//Add ModuleDataObject to array
				return moduleArray;
			}
		}
	}
	TArray<UModuleDataObject*>* empty = nullptr;
	return empty;
}

TArray<UModuleDataObject*> UModuleContainer::GetModulesByMultiOptions(EModuleMaterial Material, TArray<EModuleCategory> Categorys, TArray<EModuleFacing> Facings)
{
	TArray<UModuleDataObject*> fittingModules;
	if(Categorys.Num() > 0 && Facings.Num() > 0)
	{
		for(EModuleCategory category : Categorys)
		{
			for(EModuleFacing facing : Facings)
			{
				if(facing == EModuleFacing::None)
				{
					if(GetModulesByOptions(Material, category, EModuleFacing::Facing_Inside) != nullptr)
						fittingModules.Append(*GetModulesByOptions(Material, category, EModuleFacing::Facing_Inside));
					if(GetModulesByOptions(Material, category, EModuleFacing::Facing_Outside) != nullptr)
						fittingModules.Append(*GetModulesByOptions(Material, category, EModuleFacing::Facing_Outside));
					if(GetModulesByOptions(Material, category, EModuleFacing::Facing_Left) != nullptr)
						fittingModules.Append(*GetModulesByOptions(Material, category, EModuleFacing::Facing_Left));
					if(GetModulesByOptions(Material, category, EModuleFacing::Facing_Right) != nullptr)
						fittingModules.Append(*GetModulesByOptions(Material, category, EModuleFacing::Facing_Right));
				} else
				{
					if(GetModulesByOptions(Material, category, facing) != nullptr)
						fittingModules.Append(*GetModulesByOptions(Material, category, facing));
				}
			}
		}
	}
	return fittingModules;
}

void UModuleContainer::SaveContainer()
{
	FString Path = TEXT(MODULE_CONTAINER_PATH);
	JSONManager::WriteJsonFile(Path, UModuleContainer::ModuleContainerToJson());
}

void UModuleContainer::JsonToModuleContainer(TSharedPtr<FJsonObject> JsonObject)
{
	UModuleContainer* ModuleContainer = UModuleContainer::GetInstance();

	//Get all names from json and save them into 'AllModuleNames' for later use of finding all ModuleData that needs to be loaded
	TArray<TSharedPtr<FJsonValue>> NameArray = JsonObject->GetArrayField("Module_Names");
	if(NameArray.GetData() != nullptr)
	{
		for(TSharedPtr<FJsonValue> Name : NameArray)
		{
			ModuleContainer->AllModuleNames.Add(Name->AsString());
		}
	}

	//Load all modules from json and put them into 'SortedModules'
	TSharedPtr<FJsonObject> Modules = JsonObject->GetObjectField("Modules");
	//Load all rules from json
	TSharedPtr<FJsonObject> Rules = JsonObject->GetObjectField("ModuleRules");
	if(Modules.Get() != nullptr)
	{
		for(FString Name : ModuleContainer->AllModuleNames)
		{
			FModuleData tempModule = FModuleData::JsonToModuleData(Modules->GetObjectField(Name));
			UModuleDataObject* tempModuleDataObject = NewObject<UModuleDataObject>();
			tempModuleDataObject->StoreModuleData(tempModule);

			if(Rules.Get() != nullptr)
			{
				LoadSavedRulesIntoModule(Rules, tempModuleDataObject);
			}

			ModuleContainer->ModuleToSortedModules(tempModuleDataObject);
			ModuleContainer->ModuleToOrderedModules(tempModuleDataObject);
			ModuleContainer->AllModuleDataObjects.Add(Name, tempModuleDataObject);
		}
	}

	ModuleContainer->LinkRules();
}

void UModuleContainer::LoadSavedRulesIntoModule(TSharedPtr<FJsonObject> JsonObject, UModuleDataObject* ModuleDataObject)
{
	UModuleContainer* ModuleContainer = UModuleContainer::GetInstance();
	//Get Module-Rule-Block
	TSharedPtr<FJsonObject> ModuleRules = JsonObject->GetObjectField(ModuleDataObject->ModuleData.Name);

	if(ModuleRules.Get() != nullptr)
	{
		for(auto It = ModuleRules->Values.CreateConstIterator(); It; ++It)
		{
			UModuleRule* tempRule = UModuleRule::JsonToModuleRule(ModuleRules->GetObjectField(It.Key()));
			tempRule->SetParentModuleDataObject(ModuleDataObject);
			ModuleDataObject->AddModuleRule(tempRule);
			ModuleContainer->unlinkedRules.Add(tempRule);
		}
	}
}


TSharedPtr<FJsonObject> UModuleContainer::ModuleContainerToJson()
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	UModuleContainer* ModuleContainer = UModuleContainer::GetInstance();

	//Create Name-Block of the save file
	FJsonDomBuilder::FArray NameArray;
	for(FString Name : ModuleContainer->AllModuleNames)
	{
		NameArray.Add(Name);
	}
	//Save Name-Block into JsonObject
	JsonObject->SetArrayField("Module_Names", NameArray.AsJsonValue()->AsArray());

	//Create Module-Block of the save file
	TSharedPtr<FJsonObject> Modules = MakeShareable(new FJsonObject);
	for(TMap<FString, UModuleDataObject*>::TIterator MCIter = ModuleContainer->AllModuleDataObjects.CreateIterator(); MCIter; ++MCIter)
	{
		Modules->SetObjectField(MCIter->Key, FModuleData::ModuleDataToJson(MCIter->Value->ModuleData));
	}
	//Save Module-Block into JsonObject
	JsonObject->SetObjectField("Modules", Modules);

	//Create Rules-Block of the save file
	TSharedPtr<FJsonObject> ModuleRules = MakeShareable(new FJsonObject);
	for(TMap<FString, UModuleDataObject*>::TIterator MCIter = ModuleContainer->AllModuleDataObjects.CreateIterator(); MCIter; ++MCIter)
	{

		//Create Module-Rule-Block for each Module
		TSharedPtr<FJsonObject> RuleObject = MakeShareable(new FJsonObject);
		for(TPair<FString, UModuleRule*> Rule : MCIter->Value->ModuleRules)
		{
			//Add rules into Module-Rule-Block
			RuleObject->SetObjectField(Rule.Value->AffectedModuleName, UModuleRule::ModuleRuleToJson(Rule.Value));
		}
		//Add Module-Rule-Block to Rules-Block
		ModuleRules->SetObjectField(MCIter->Key, RuleObject);
	}
	//Save Rules-Block into JsonObject
	JsonObject->SetObjectField("ModuleRules", ModuleRules);

	return JsonObject;
}