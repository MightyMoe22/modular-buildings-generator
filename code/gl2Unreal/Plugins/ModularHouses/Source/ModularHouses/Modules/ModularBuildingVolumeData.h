// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "Json/Public/Serialization/JsonSerializer.h"
#include "ModularBuildingVolumeData.generated.h"

USTRUCT()
struct FModularBuildingVolumeData
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY()
	FVector VolumeScale;

public:
	static FModularBuildingVolumeData BuildModularBuildingVolumeData(FVector VolumeSize);
	static FModularBuildingVolumeData JsonToModularBuildingVolumeData(TSharedPtr<FJsonObject> JsonObject);
	static TSharedPtr<FJsonObject> ModularBuildingVolumeDataToJson(FModularBuildingVolumeData ModularBuildingVolumeData);
};