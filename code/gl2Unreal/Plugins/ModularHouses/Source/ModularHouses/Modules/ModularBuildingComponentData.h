// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "Json/Public/Serialization/JsonSerializer.h"
#include "ModularBuildingComponentData.generated.h"

USTRUCT()
struct FModularBuildingComponentData
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere)
	FString ModuleStaticMeshPath;
	UPROPERTY()
	FVector ModulePosition;
	UPROPERTY()
	FRotator ModuleRotation;

public:
	static FModularBuildingComponentData BuildModularBuildingComponentData(
		FString ModuleStaticMeshPath,
		FVector ModulePosition,
		FRotator ModuleRotation
	);
	static FModularBuildingComponentData JsonToModularBuildingComponentData(TSharedPtr<FJsonObject> JsonObject);
	static TSharedPtr<FJsonObject> ModularBuildingComponentDataToJson(FModularBuildingComponentData ModularBuildingComponentData);
};
