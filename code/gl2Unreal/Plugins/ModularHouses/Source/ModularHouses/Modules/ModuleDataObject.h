// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Modules/ModuleData.h"
#include "ModuleDataObject.generated.h"

UCLASS(BlueprintType)
class MODULARHOUSES_API UModuleDataObject : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, Category = "Module Data Object")
	FModuleData ModuleData;

	UPROPERTY(BlueprintReadWrite, Category = "Module Data Object")
	TMap<FString, UModuleRule*> ModuleRules;

public:

	UModuleDataObject();

	UFUNCTION(BlueprintCallable, Category = "Module Data Object")
	void StoreModuleData(FModuleData DataToStore);

	UFUNCTION(BlueprintCallable, Category = "Module Data Object")
	void AddModuleRule(UModuleRule* ModuleRule);

	UFUNCTION(BlueprintCallable, Category = "Module Data Object")
	void RemoveModuleRule(UModuleRule* ModuleRule);
	
};
