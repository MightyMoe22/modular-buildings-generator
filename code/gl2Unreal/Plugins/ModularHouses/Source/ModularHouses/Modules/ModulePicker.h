// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Generator/FGridTileStruct.h"
#include "list"
#include "ModuleDataObject.h"
#include "ModuleContainer.h"
#include "ModulePicker.generated.h"

UCLASS()
class MODULARHOUSES_API UModulePicker : public UObject
{
	GENERATED_BODY()

public:
	EModuleMaterial OutdoorMaterial;
	EModuleMaterial IndoorMaterial;
	std::list<FGridTileStruct>** FloorGrid;

public:
	TMap<FGridTileStruct, UModuleDataObject*> PickModulesAt(int32 x, int32 y);
	void SetupModulePicker(EModuleMaterial Outdoor, EModuleMaterial Indoor, std::list<FGridTileStruct>** Groundgrid);

private:
	UModuleDataObject* PickModuleFor(FGridTileStruct GridTileStruct);
	UModuleDataObject* RandomSelect(TArray<UModuleDataObject*> possibleModules, FGridTileStruct GridTileStruct);
	TArray<UModuleDataObject*> GetPossibleModules(FGridTileStruct GridTileStruct);
	void SplittMiscsFromRest(TArray<UModuleDataObject*>& possibleModules, TArray<UModuleDataObject*>& outMiscs);
};
