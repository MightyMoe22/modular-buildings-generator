// Copyright Maurice Langer und Jakob Seitz 2020
#include "ModuleData.h"
#include "JSONManager/JSONManager.h"

const FString FACING = "Facing";
const FString CATEGORY = "Category";
const FString MATERIAL = "Material";
const FString ENVIRONMENT = "Environment";
const FString NAME = "Name";
const FString PATH = "Mesh_Path";
const FString POSITION = "Relative_Position";
const FString ROTATION = "Relative_Rotation";
const FString SCALE = "Relative_Scale";
const FString ISMISC = "Is_Misc";


FModuleData::FModuleData()
{
	Facing = EModuleFacing::None;
	Category = EModuleCategory::None;
	Material = EModuleMaterial::None;
	Environment = EModuleEnvironment::None;
	Name.Empty();
	StaticMeshPath.Empty();
	Position = FVector().ZeroVector;
	Rotation = FRotator().ZeroRotator;
	Scale = FVector().OneVector;
	IsMisc = false;
}

FModuleData FModuleData::BuildModuleData(EModuleFacing Facing, 
										 EModuleCategory Category, 
										 EModuleMaterial Material, 
										 EModuleEnvironment Environment, 
										 FString Name,
										 FString StaticMeshPath, 
										 FVector Position,
										 FRotator Rotation, 
										 FVector Scale,
										 bool IsMisc)
{
	FModuleData  ModularData;

	ModularData.Facing = Facing;
	ModularData.Category = Category;
	ModularData.Material = Material;
	ModularData.Environment = Environment;
	ModularData.Name = Name;
	ModularData.StaticMeshPath = StaticMeshPath;
	ModularData.Position = Position;
	ModularData.Rotation = Rotation;
	ModularData.Scale = Scale;
	ModularData.IsMisc = IsMisc;

	return ModularData;
}

FModuleData FModuleData::JsonToModuleData(TSharedPtr<FJsonObject> JsonObject)
{
	FModuleData ModuleData;

	ModuleData.Facing = FModuleData::ConvertToModuleFacing(JsonObject->GetNumberField(FACING));
	ModuleData.Category = FModuleData::ConvertToModuleCategory(JsonObject->GetNumberField(CATEGORY));
	ModuleData.Material = FModuleData::ConvertToModuleMaterial(JsonObject->GetNumberField(MATERIAL));
	ModuleData.Environment = FModuleData::ConvertToModuleEnvironment(JsonObject->GetNumberField(ENVIRONMENT));
	ModuleData.Name = JsonObject->GetStringField(NAME);
	ModuleData.StaticMeshPath = JsonObject->GetStringField(PATH);
	ModuleData.Position = JSONManager::ConvertArrayToVector(JsonObject->GetArrayField(POSITION));
	FVector EulerAngles = JSONManager::ConvertArrayToVector(JsonObject->GetArrayField(ROTATION));
	//Saved as Y,Z,X -> X = Y, Y = Z, Z = X
	ModuleData.Rotation = FRotator(EulerAngles.X, EulerAngles.Y, EulerAngles.Z);
	ModuleData.Scale = JSONManager::ConvertArrayToVector(JsonObject->GetArrayField(SCALE));
	ModuleData.IsMisc = JsonObject->GetBoolField(ISMISC);

	return ModuleData;
}

TSharedPtr<FJsonObject> FModuleData::ModuleDataToJson(FModuleData ModularData)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	const TArray<TSharedPtr<FJsonValue>> Position = JSONManager::ConvertVectorToArray(ModularData.Position);
	//Y,Z,X because rotation has different axis-order
	FVector RotationAsVector = FVector(ModularData.Rotation.Euler().Y, ModularData.Rotation.Euler().Z, ModularData.Rotation.Euler().X);
	const TArray<TSharedPtr<FJsonValue>> Rotation = JSONManager::ConvertVectorToArray(RotationAsVector);
	const TArray<TSharedPtr<FJsonValue>> Scale = JSONManager::ConvertVectorToArray(ModularData.Scale);

	// Fill JsonObject with ModuleData
	JsonObject->SetNumberField(FACING, (uint8) ModularData.Facing);
	JsonObject->SetNumberField(CATEGORY, (uint8) ModularData.Category);
	JsonObject->SetNumberField(MATERIAL, (uint8) ModularData.Material);
	JsonObject->SetNumberField(ENVIRONMENT, (uint8) ModularData.Environment);
	JsonObject->SetStringField(NAME, ModularData.Name);
	JsonObject->SetStringField(PATH, ModularData.StaticMeshPath);
	JsonObject->SetArrayField(POSITION, Position);
	JsonObject->SetArrayField(ROTATION, Rotation);
	JsonObject->SetArrayField(SCALE, Scale);
	JsonObject->SetBoolField(ISMISC, ModularData.IsMisc);

	return JsonObject;
}

EModuleFacing FModuleData::ConvertToModuleFacing(uint8 ID)
{
	return (EModuleFacing) ID;
}

EModuleCategory FModuleData::ConvertToModuleCategory(uint8 ID)
{
	return (EModuleCategory) ID;
}

EModuleMaterial FModuleData::ConvertToModuleMaterial(uint8 ID)
{
	return (EModuleMaterial) ID;
}

EModuleEnvironment FModuleData::ConvertToModuleEnvironment(uint8 ID)
{
	return (EModuleEnvironment) ID;
}

EModuleDirection FDirectionManipulator::rotateDirection(EModuleDirection oldDirection, int rotation)
{
	if (!(rotation % 45 == 0)) {
		UE_LOG(LogTemp, Error, TEXT("try to rotate with unallowed angle: %i"), rotation);
		return EModuleDirection::None;
	}

	int oldDirectionInDegree = getAngleForDirection(oldDirection);
	int newDirection = oldDirectionInDegree + rotation;
	return getDirectionForAngle(newDirection);
}

int FDirectionManipulator::getAngleForDirection(EModuleDirection direction)
{
	switch (direction)
	{
	case EModuleDirection::None:
	case EModuleDirection::North: return 0;
	case EModuleDirection::NorthEast: return 45;
	case EModuleDirection::East: return 90;
	case EModuleDirection::SouthEast: return 135;
	case EModuleDirection::South:return 180;
	case EModuleDirection::SouthWest: return 225;
	case EModuleDirection::West: return 270;
	case EModuleDirection::NorthWest:return 315;
	UE_LOG(LogTemp, Error, TEXT("Bad convertation of angle in degree (should never happen)"));
	default: return 0;
	}
}

EModuleDirection FDirectionManipulator::getDirectionForAngle(int angle)
{
	if (angle % 360 == 0) return EModuleDirection::North;
	if (angle % 360 == 45) return EModuleDirection::NorthEast;
	if (angle % 360 == 90) return EModuleDirection::East;
	if (angle % 360 == 135) return EModuleDirection::SouthEast;
	if (angle % 360 == 180) return EModuleDirection::South;
	if (angle % 360 == 225) return EModuleDirection::SouthWest;
	if (angle % 360 == 270) return EModuleDirection::West;
	if (angle % 360 == 315) return EModuleDirection::NorthWest;
	UE_LOG(LogTemp, Error, TEXT("Bad convertation of direction in angle"));
	return EModuleDirection::None;
}

EModuleDirection FDirectionManipulator::mergeDirections(EModuleDirection d1, EModuleDirection d2)
{
	if (d1 == EModuleDirection::North && d2 == EModuleDirection::East) return EModuleDirection::NorthEast;
	if (d1 == EModuleDirection::South && d2 == EModuleDirection::East) return EModuleDirection::SouthEast;
	if (d1 == EModuleDirection::South && d2 == EModuleDirection::West) return EModuleDirection::SouthWest;
	if (d1 == EModuleDirection::North && d2 == EModuleDirection::West) return EModuleDirection::NorthWest;
	return EModuleDirection();
}

TArray<EModuleDirection> FDirectionManipulator::splitDirection(EModuleDirection d1)
{
	if (d1 == EModuleDirection::NorthEast) return { EModuleDirection::North, EModuleDirection::East };
	if (d1 == EModuleDirection::NorthWest) return { EModuleDirection::North, EModuleDirection::West };
	if (d1 == EModuleDirection::SouthWest) return { EModuleDirection::South, EModuleDirection::West };
	if (d1 == EModuleDirection::SouthEast) return { EModuleDirection::South, EModuleDirection::East };
	UE_LOG(LogTemp, Error, TEXT("Directions could not be split!"));
	return TArray<EModuleDirection>();
}

FIntPoint FDirectionManipulator::directionToVector(EModuleDirection direction)
{
	switch (direction)
	{
	case EModuleDirection::North:
		return { 0,-1 };
	case EModuleDirection::East:
		return { 1,0 };
	case EModuleDirection::South:
		return { 0,1 };
	case EModuleDirection::West:
		return { -1, 0 };
	default:
		return { 0,0 };
	}
}

