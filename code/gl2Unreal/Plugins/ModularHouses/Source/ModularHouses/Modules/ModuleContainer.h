// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Json/Public/Serialization/JsonSerializer.h"
#include "ModuleData.h"
#include "ModuleDataObject.h"
#include "Rule/ModuleRule.h"
#include "ModuleContainer.generated.h"

#define MODULE_CONTAINER_PATH "/ModularHouses/Content/JSON/ModuleContainer.json"

UCLASS(BlueprintType)
class MODULARHOUSES_API UModuleContainer : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Module Container")
	TArray<FString> AllModuleNames;

	UPROPERTY(BlueprintReadOnly, Category = "Module Container")
	TMap<FString, UModuleDataObject*> AllModuleDataObjects;

private:

	TMap<EModuleCategory, TMap<EModuleFacing, TArray<FModuleData>>> SortedModules;
	TMap<EModuleMaterial, TMap<EModuleCategory, TMap<EModuleFacing, TArray<UModuleDataObject*>>>> OrderedModules;
	TArray<UModuleRule*> unlinkedRules;

public:

	/* Static access method. */
	UFUNCTION(BlueprintCallable, Category = "Module Container")
	static UModuleContainer* GetInstance();

	bool AddModuleToContainer(FModuleData NewModule);
	TArray<FModuleData> GetModules(EModuleCategory Category, EModuleFacing Facing);
	void ModuleToSortedModules(const UModuleDataObject* ModuleToSort);

	UFUNCTION(BlueprintCallable, Category = "Module Container")
	bool RemoveModuleFromContainer(FString ModuleName);
	UFUNCTION(BlueprintCallable, Category = "Module Container")
	void SaveContainer();

	
	static TSharedPtr<FJsonObject> ModuleContainerToJson();
	TArray<UModuleDataObject*>* GetModulesByOptions(EModuleMaterial Material, EModuleCategory Category, EModuleFacing Facing);
	TArray<UModuleDataObject*> GetModulesByMultiOptions(EModuleMaterial Material, TArray<EModuleCategory> Categorys, TArray<EModuleFacing> Facings);

private:

	/* Private constructor to prevent instancing. */
	UModuleContainer();

	void LinkRules();
	static void JsonToModuleContainer(TSharedPtr<FJsonObject> JsonObject);
	static void LoadSavedRulesIntoModule(TSharedPtr<FJsonObject> JsonObject, UModuleDataObject* ModuleDataObject);
	void ModuleToOrderedModules(UModuleDataObject* ModuleDataObject);
	void RemoveModuleFromOrderedModules(UModuleDataObject* ModuleDataObject);
};
