// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModulePicker.h"

void UModulePicker::SetupModulePicker(EModuleMaterial Outdoor, EModuleMaterial Indoor, std::list<FGridTileStruct>** Groundgrid)
{
	OutdoorMaterial = Outdoor;
	IndoorMaterial = Indoor;
	FloorGrid = Groundgrid;
}

void UModulePicker::SplittMiscsFromRest(TArray<UModuleDataObject*>& possibleModules, TArray<UModuleDataObject*>& outMiscs)
{
	TArray<UModuleDataObject*> nonMisc;
	for(auto mdo : possibleModules)
	{
		if(mdo->ModuleData.IsMisc)
		{
			outMiscs.Add(mdo);
		} else
		{
			nonMisc.Add(mdo);
		}
	}
	possibleModules = nonMisc;
}

UModuleDataObject* UModulePicker::RandomSelect(TArray<UModuleDataObject*> possibleModules, FGridTileStruct GridTileStruct)
{
	TArray<UModuleDataObject*> miscModules;
	SplittMiscsFromRest(possibleModules, miscModules);

	float miscChance = FMath::FRandRange(0.0f, 1.0f);
	float miscThreshold = 0.95f;
	if(GridTileStruct.moduleCategory == EModuleCategory::Wall)
	{
		miscThreshold -= 0.3f;
	}

	if(possibleModules.Num() > 0 && miscChance >= miscThreshold && miscModules.Num() > 0)
	{
		int randomIndex = FMath::RoundToInt(FMath::FRandRange(0.0f, miscModules.Num() - 1.0f));
		return miscModules[randomIndex];
	} else if(possibleModules.Num() > 0)
	{
		int randomIndex = FMath::RoundToInt(FMath::FRandRange(0.0f, possibleModules.Num() - 1.0f));
		return possibleModules[randomIndex];
	}

	return nullptr;
}

TArray<UModuleDataObject*> UModulePicker::GetPossibleModules(FGridTileStruct GridTileStruct)
{
	UModuleContainer* ModuleContainer = UModuleContainer::GetInstance();
	TArray<EModuleCategory> categorys;
	TArray<EModuleFacing> facings;

	categorys.Add(GridTileStruct.moduleCategory);
	facings.Add(GridTileStruct.moduleFacing);
	TArray<UModuleDataObject*> roughSelection;
	if (GridTileStruct.moduleEnvironment == EModuleEnvironment::Outdoor)
	{
		roughSelection = ModuleContainer->GetModulesByMultiOptions(OutdoorMaterial, categorys, facings);
	}
	else
	{
		roughSelection = ModuleContainer->GetModulesByMultiOptions(IndoorMaterial, categorys, facings);
	}
	
	TArray<UModuleDataObject*> accurateSelection;

	for(UModuleDataObject* mdo : roughSelection)
	{
		if(mdo->ModuleData.Environment == GridTileStruct.moduleEnvironment
		   || mdo->ModuleData.Environment == EModuleEnvironment::Both)
		{
			accurateSelection.Add(mdo);
		}
	}

	return accurateSelection;
}


UModuleDataObject* UModulePicker::PickModuleFor(FGridTileStruct GridTileStruct)
{
	UModuleContainer* ModuleContainer = UModuleContainer::GetInstance();
	TArray<UModuleDataObject*> fittingModules = GetPossibleModules(GridTileStruct);

	return RandomSelect(fittingModules, GridTileStruct);
}

TMap<FGridTileStruct, UModuleDataObject*> UModulePicker::PickModulesAt(int32 x, int32 y)
{
	std::list<FGridTileStruct> GridTileList = FloorGrid[x][y];

	TMap<FGridTileStruct, UModuleDataObject*> pickedModules;

	for(FGridTileStruct gridTile : GridTileList)
	{
		UModuleDataObject* moduleDataObject = PickModuleFor(gridTile);
		if(moduleDataObject != nullptr)
		{
			pickedModules.Add(gridTile, moduleDataObject);
		}
	}
	return pickedModules;
}


