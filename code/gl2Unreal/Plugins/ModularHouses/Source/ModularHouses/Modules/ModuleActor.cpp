// Copyright Maurice Langer und Jakob Seitz 2020

#include "ModuleActor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
AModuleActor::AModuleActor()
{
	SetMobility(EComponentMobility::Movable);
	SetRootComponent(GetStaticMeshComponent());
}

bool AModuleActor::LoadModuleData(FModuleData ModuleDataToLoad)
{
	this->ModuleData = ModuleDataToLoad;

	bool loadingSucceeded = false;
	//Loading the mesh of the module from path
	auto MeshAsset = LoadObject<UStaticMesh>(NULL, *ModuleDataToLoad.StaticMeshPath, NULL, LOAD_None, NULL);
	
	if(MeshAsset != nullptr)
	{
		GetStaticMeshComponent()->SetStaticMesh(MeshAsset);
		SetActorRelativeLocation(ModuleDataToLoad.Position);
		SetActorRelativeRotation(ModuleDataToLoad.Rotation);
		SetMobility(EComponentMobility::Static);
		loadingSucceeded = true;
	}
	return loadingSucceeded;
}
