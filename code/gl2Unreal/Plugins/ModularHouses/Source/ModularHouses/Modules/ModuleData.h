// Copyright Maurice Langer und Jakob Seitz 2020

#pragma once

#include "Json/Public/Serialization/JsonSerializer.h"
#include "ModuleData.generated.h"

UENUM(BlueprintType)
enum class EModuleDirection : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	North = 1 UMETA(DisplayName = "North"),
	NorthEast = 2 UMETA(DisplayName = "North East"),
	East = 3 UMETA(DisplayName = "East"),
	SouthEast = 4 UMETA(DisplayName = "South East"),
	South = 5 UMETA(DisplayName = "South"),
	SouthWest = 6 UMETA(DisplayName = "South West"),
	West = 7 UMETA(DisplayName = "West"),
	NorthWest = 8 UMETA(DisplayName = "North West")
};

UENUM(BlueprintType)
enum class EModuleFacing : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	Facing_Outside = 1 UMETA(DisplayName = "Facing Outside"),
	Facing_Inside = 2 UMETA(DisplayName = "Facing Inside"),
	Facing_Left = 3 UMETA(DisplayName = "Facing Left"),
	Facing_Right = 4 UMETA(DisplayName = "Facing Right")
};

UENUM(BlueprintType)
enum class EModuleEnvironment : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	Outdoor = 1 UMETA(DisplayName = "Outdoor"),
	Indoor = 2 UMETA(DisplayName = "Indoor"),
	Both = 3 UMETA(DisplayName = "Both")
};

/* Should be dynamic if possible too*/
UENUM(BlueprintType)
enum class EModuleCategory : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	Wall = 1 UMETA(DisplayName = "Wall"),
	Edge = 2 UMETA(DisplayName = "Edge"),
	Door = 3 UMETA(DisplayName = "Door"),
	Window = 4 UMETA(DisplayName = "Window"),
	Stairs = 5 UMETA(DisplayName = "Stairs"),
	Pillar = 6 UMETA(DisplayName = "Pillar"),
	Beam = 7 UMETA(DisplayName = "Beam"),
	Roof = 8 UMETA(DisplayName = "Roof"),
	Floor = 9 UMETA(DisplayName = "Floor"),
	Ceiling = 10 UMETA(DisplayName = "Ceiling"),
	Corner = 11 UMETA(DisplayName = "Corner"),
	R_Filler = 12 UMETA(DisplayName = "Roof Filler"),
	R_Corner = 13 UMETA(DisplayName = "Roof Corner"),
	R_Edge = 14 UMETA(DisplayName = "Roof Edge"),
	R_E_Corner = 15 UMETA(DisplayName = "Roof Edge Corner"),
	R_E_Gutter = 16 UMETA(DisplayName = "Roof Edge Gutter"),
	R_Wall = 17 UMETA(DisplayName = "Roof Wall"),
	R_Gutter = 18 UMETA(DisplayName = "Roof Gutter"),
	R_E_Filler = 19 UMETA(DisplayName = "Roof Edge Filler"),
	R_G_Corner = 20 UMETA(DisplayName = "Roof Edge Gutter Corner"),
	Flatroof = 21 UMETA(DisplayName = "Flat Roof"),
	Flatroof_Edge = 22 UMETA(DisplayName = "Flat Roof Edge"),
	Flatroof_Corner = 23 UMETA(DisplayName = "Flat Roof Corner")
};

/* Material need to be dynamic somehow to be editable for the users until then their is no use for it*/
UENUM(BlueprintType)
enum class EModuleMaterial : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	MaterialSlot1 = 1 UMETA(DisplayName = "Material Slot 1"),
	MaterialSlot2 = 2 UMETA(DisplayName = "Material Slot 2"),
	MaterialSlot3 = 3 UMETA(DisplayName = "Material Slot 3"),
	MaterialSlot4 = 4 UMETA(DisplayName = "Material Slot 4"),
	MaterialSlot5 = 5 UMETA(DisplayName = "Material Slot 5"),
	MaterialSlot6 = 6 UMETA(DisplayName = "Material Slot 6"),
	MaterialSlot7 = 7 UMETA(DisplayName = "Material Slot 7"),
	MaterialSlot8 = 8 UMETA(DisplayName = "Material Slot 8"),
	MaterialSlot9 = 9 UMETA(DisplayName = "Material Slot 9"),
	MaterialSlot310 = 10 UMETA(DisplayName = "Material Slot 10")
};


USTRUCT()
struct FDirectionManipulator
{
	GENERATED_USTRUCT_BODY()
	static EModuleDirection rotateDirection(EModuleDirection oldDirection, int degree);
	static int getAngleForDirection(EModuleDirection direction);
	static EModuleDirection getDirectionForAngle(int angle);
	static EModuleDirection mergeDirections(EModuleDirection d1, EModuleDirection d2);
	static TArray<EModuleDirection> splitDirection(EModuleDirection d1);
	static FIntPoint directionToVector(EModuleDirection direction);
};

USTRUCT(BlueprintType)
struct MODULARHOUSES_API FModuleData
{
	GENERATED_USTRUCT_BODY()

public:
	/*Name works as identifier*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	EModuleCategory Category = EModuleCategory::None;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	EModuleFacing Facing = EModuleFacing::None;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	EModuleMaterial Material = EModuleMaterial::None;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	EModuleEnvironment Environment = EModuleEnvironment::None;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	FString StaticMeshPath;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	FVector Position;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	FRotator Rotation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	FVector Scale;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Module Data")
	bool IsMisc;

public:
	FModuleData();

	static FModuleData BuildModuleData(
		EModuleFacing Facing,
		EModuleCategory Category,
		EModuleMaterial Material,
		EModuleEnvironment Environment,
		FString Name,
		FString StaticMeshPath,
		FVector Position,
		FRotator Rotation,
		FVector Scale,
		bool IsMisc);
	static FModuleData JsonToModuleData(TSharedPtr<FJsonObject> JsonObject);
	static TSharedPtr<FJsonObject> ModuleDataToJson(FModuleData ModularData);

private:
	static EModuleFacing ConvertToModuleFacing(uint8 ID);
	static EModuleCategory ConvertToModuleCategory(uint8 ID);
	static EModuleMaterial ConvertToModuleMaterial(uint8 ID);
	static EModuleEnvironment ConvertToModuleEnvironment(uint8 ID);
};

